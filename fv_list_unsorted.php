<?php
// Starta session.
require_once('startsession.php');
// Skriver ut en lista på alla anmälda till folkets val.
// Sorterar inte efter vem man vill stå bredvid.

// Inkludera konstanter och funktioner.
require_once('const.php');
require_once('funct.php');

// Kontrollera behörighet.
AccessChk(basename(__FILE__, ".php"));
$_SESSION["testsub"] = "testsub_set";
$dbg_msg = "";

// Anslut till databasen.
$dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if (!$dbc) {
  die("fv_list_unsorted.php: " . "Connection failed: " . mysqli_connect_error());
}
//blockera fler ändringar av FV-Nr 
//Detta är en säkerhetsfunktion för att hindra oavsiktlig uppdatering av FV-nr (manuell upplåsning i datbasen krävs)
if (isset($_POST['lockFVChanges'])) {
  $query = "UPDATE Events SET fv_nr_edit_locked = 1 WHERE event_id = " . $_SESSION['fv_event_id'];
  if (!mysqli_query($dbc, $query)) {
    die("fv_list_unsorted.php.Events " . mysqli_error($dbc) . $query);
  }
  $_SESSION['fv_nr_edit_locked'] = TRUE;
  echo '<p>FV-Nr är från och med nu låsta, av säkerhetsskäl kan återställnning enbart ske manuellt i databas.</p>';
}

// Ladda upp csv-fil med tävlingsnummer för FV
if (isset($_POST['uploadcsv'])) {
  if ($_FILES["fileToUpload"]["name"] == "") {
    echo ("Välj en först en fil att ladda upp.");
  } else {
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if (isset($_POST["uploadcsv"])) {
      $check = filesize($_FILES["fileToUpload"]["tmp_name"]);
      if ($check !== false) {
        //echo "File is an csv - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        echo "Filen är inte en csv.";
        $uploadOk = 0;
      }
    }

    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
      echo "Sorry, filen är för stor.";
      $uploadOk = 0;
    }
    // Allow certain file formats
    if ($imageFileType != "csv") {
      echo "Sorry, enbart csv är tillåten.";
      $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, det gick inte ladda upp filen.";
      // if everything is ok, try to upload file
    } else {
      // Check if file already exists
      if (file_exists($target_file)) {
        //delete existing file
        unlink($target_file);
      }
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "Filen " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " laddades upp.";
        $uploadOk = 1;
      } else {
        echo "Sorry, misslyckades med att ladda upp filen.";
        $uploadOk = 0;
      }
    }
    if ($uploadOk == 1) {
      // Läs in tävlingsnummer från csv-filen    
      $updateCount = 0;
      $beerCount = $_SESSION['no_fv_beers'];
      //förväntat format: 
      //Beer-Id;FV-Nr;Ölets namn;Öltyp....
      $csv = array_map(function ($v) {
        return str_getcsv($v, ";");
      }, file($target_file));
      //hitta positioner för fält Beer-Id och FV-Nr, eller sätt till -1 om de inte hittas
      $beerIdPos = array_search('Beer-Id', $csv[0]);
      $fvNrPos = array_search('FV-Nr', $csv[0]);

      if ($beerIdPos ===  false || $fvNrPos  ===  false) {
        die("fv_list_unsorted.php: Felaktig csv-fil, Kolumn Beer-Id eller FV-Nr saknas");
      }
      //rensa eventuella tidigare tävlingsnummer
      $query = "UPDATE Beers_in_event SET fv_competition_no = NULL WHERE event_id = " . $_SESSION['fv_event_id'];
      if (!mysqli_query($dbc, $query)) {
        die("fv_list_unsorted.php.Beers_in_event " . mysqli_error($dbc) . $query);
      }

      $beerCountCsv = count($csv);
      //loopa alla rader i csv-filen och uppdatera/addera tävlingsnummer till fv_competition_no i Beers_in_event, med ref mot beer_id och event_id
      for ($i = 1; $i < $beerCountCsv; $i++) {
        if ($csv[$i][$fvNrPos] != "" && $csv[$i][$beerIdPos] != "") {
          $query = "UPDATE Beers_in_event SET fv_competition_no = " . $csv[$i][$fvNrPos] . " WHERE beer_id = " . $csv[$i][$beerIdPos] . " AND event_id = " . $_SESSION['fv_event_id'];
          if (!mysqli_query($dbc, $query)) {
            die("fv_list_unsorted.php.Beers_in_event (itr=". i . ")" . mysqli_error($dbc) . $query);
          } else
            $updateCount++;
        }
      }


      echo '<p>' . $updateCount . 'st ölnummer inlästa</p>';
    }
  }
}


//skapa ölnummer (tävlingsnummer för FV) med start på 101, och spara till fv_competition_no i Beers_in_event
//(om ordningen på numren behöver justeras kan de senare ersättas med en csv-fil)
if (isset($_POST['createnumbers'])) {

  $updateCount = 0;
  $beerCount = $_SESSION['no_fv_beers'];
  
  //rensa evenuella tidigare tävlingsnummer
  $query = "UPDATE Beers_in_event SET fv_competition_no = NULL WHERE event_id = " . $_SESSION['fv_event_id'];
  if (!mysqli_query($dbc, $query)) {
    die("fv_list_unsorted.php.Beers_in_event " . mysqli_error($dbc) . $query);
  }
  //skapa nya tävlingsnummer
  for ($i = 0; $i < $beerCount; $i++) {
    $query = "UPDATE Beers_in_event SET fv_competition_no= " . ($i + 101) . " WHERE beer_id = " . $_SESSION['fv_beer_id'][$i] . " AND event_id = " . $_SESSION['fv_event_id'] . " AND deleted = 0 AND (fv_competition_no = 0 OR fv_competition_no IS NULL);";
    if (!mysqli_query($dbc, $query)) {
      die("fv_list_unsorted.php.Beers_in_event (itr=". $i . ")" . mysqli_error($dbc) + "/" . $query );
    } else
      $updateCount++;
  }
  echo '<p>' . $updateCount . 'st ölnummer skapade</p>';
}

// Spara data till Prices.
if (isset($_POST['save'])) {
  // Hämta värden från POST.
  for ($i = 0; $i < $_SESSION['no_fv_beers']; $i++) {
    $price[$i] = FilterPost($dbc, $_POST['price'][$i], 20);
  }

  for ($i = 0; $i < $_SESSION['no_fv_beers']; $i++) {
    // Kontrollera om något data ändrats
    // Hämta price från Prices.
    $query = "SELECT price FROM Prices " .
      "WHERE beer_id = " . $_SESSION['fv_beer_id'][$i] . " " .
      "AND event_id = " . $_SESSION['fv_event_id'] . " " .
      "AND deleted = 0";

    $result = mysqli_query($dbc, $query) or die(mysqli_error($dbc));
    $addPr = 0;
    $delPr = 0;
    if (mysqli_num_rows($result) == 0) {
      $addPr = 1;
    }
    if (mysqli_num_rows($result) == 1) {
      $row = mysqli_fetch_array($result);
      if ($price[$i] != $row['price']) {
        $addPr = 1;
        $delPr = 1;
      }
    }
    if (mysqli_num_rows($result) > 1) {
      die("fv_list_unsorted.php: No data or too many data found in Prices.");
    }
    if ($delPr) {
      // Markera nuvarande rad i Prices som raderad.
      $query = "UPDATE Prices SET deleted = 1 " .
        "WHERE beer_id = " . $_SESSION['fv_beer_id'][$i] . " " .
        "AND event_id = " . $_SESSION['fv_event_id'] . " " .
        "AND deleted = 0";
      if (!mysqli_query($dbc, $query)) {
        die("fv_list_unsorted.php.Prices " . mysqli_error($dbc) . $query);
      }
    }
    if ($addPr) {
      // Lägg till ny rad i Prices.
      $query = "INSERT INTO Prices (event_id, beer_id, price) " .
        "VALUES ('" . $_SESSION['fv_event_id'] . "', '" . $_SESSION['fv_beer_id'][$i] . "', '" . $price[$i] . "')";
      if (!mysqli_query($dbc, $query)) {
        die("fv_list_unsorted.php.Prices " . mysqli_error($dbc) . $query);
      }
    }
  }
}




// Sidhuvud.
$page_title = 'Anmälan till ' . $_SESSION['event_name'];
require_once('header_nav.php');

echo $dbg_msg;


// Hämta ölerna.
if (empty($_SESSION['fv_event_id'])) {
  die("fv_list_unsorted.php: No fv_event id defined.");
}
// $query = "SELECT Beers.beer_id, Beers.user_id, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class, ".
//          "Beer_data.low_alc, Beer_data.og, Beer_data.fg, Beer_data.bu, Beer_data.alc,User_data.name,Beers_in_event.fv_competition_no  FROM Beers ".
//          "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
//          "INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) ".

//          "WHERE Beers_in_event.event_id = ".$_SESSION['fv_event_id'].   " ".
//          "AND Beers_in_event.deleted = 0 ".
//          "AND Beer_data.deleted = 0 AND Beers.deleted = 0 ".
//          "AND Users.deleted = 0 AND User_data.deleted = 0 ".
//          "ORDER BY User_data.name ASC"; //,fv_event_reg.next_to_id  


//sortera grupperat på registrerat av, och sedan på stå bredvid
//DISTINCT pga php 5.6/shbf mysql
$query = "SELECT DISTINCT Beers.beer_id, Beers.user_id, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class,\n"
  . "           Beer_data.low_alc, Beer_data.og, Beer_data.fg, Beer_data.bu, Beer_data.alc,User_data.name,User_data.user_id,Beers_in_event.fv_competition_no,fer.next_to_id ,ferud.name as nextToName FROM Beers\n"
  . "           INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id)\n"
  . "           INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) \n"
  . "           LEFT JOIN Fv_event_reg fer \n"
  . "           				inner join User_data ferud on ferud.User_id=fer.Next_to_id  and ferud.deleted=0 \n"
  . "           			ON fer.User_id = Beers.user_id and fer.Fv_event_id=" . $_SESSION['fv_event_id'] . " and fer.Next_to_id > 0\n"
  . "           \n"
  . "           \n"
  . "           WHERE Beers_in_event.event_id = " . $_SESSION['fv_event_id'] . "\n"
  . "           AND Beers_in_event.deleted = 0\n"
  . "           AND Beer_data.deleted = 0 AND Beers.deleted = 0\n"
  . "           AND Users.deleted = 0 AND User_data.deleted = 0  \n"
  . "ORDER BY CASE \n"
  . "           WHEN `nextToName` IS NOT NULL THEN `nextToName` ELSE User_data.name \n"
  . "           END ASC";

mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
$result = mysqli_query($dbc, $query) or die('sql:' . mysqli_error($dbc));
//  $no_reg_beers = mysqli_num_rows($result);
$no_reg_beers = NoRegBeers($_SESSION['fv_event_id']);
// Rubrikerna i tabellen.
echo '<p class=head_2>Folkets val</p>';
echo '<p>' . $no_reg_beers . ' öl anmälda.</p>';
echo '<form method="post" action="' . $_SERVER['PHP_SELF'] . '" enctype="multipart/form-data"> ';
if ($_SESSION['adm_lev'] == 'FULL') {
  echo '<input type="submit" value="Spara ner priser" name="save" onclick="javascript:return confirm(' ."'Är du säker?'" . ');"/> ';
  if ($_SESSION['fv_nr_edit_locked'] !== TRUE) {
    echo '<input type="submit" value="Skapa FV tävlingsnummer" name="createnumbers" onclick="javascript:return confirm(' ."'Är du säker?'" . ');"/> ';
    echo '<input type="file" name="fileToUpload" id="fileToUpload">';
    echo '<input type="submit" value="Läs tävlingsnummer från csv" name="uploadcsv" onclick="javascript:return confirm(' ."'Är du säker?'" . ');"/> ';
    echo '<input type="submit" value="Lås FV-Nr ändringar" name="lockFVChanges" onclick="javascript:return confirm(' ."'Är du säker?'" . ');"/> ';
  }
}
echo '<table> ';
if ($_SESSION['fv_nr_edit_locked'] !== TRUE){
  echo '<p>Befintliga FV-Nr skrivs över vid varje knapptryck. Justerade FV-Nr kan importeras via csv-fil (innehållande minst kolumnerna Beer-Id;FV-Nr). </p>';
  echo '<p>FV-nr kan nollställas via csv-fil (med tomma FV-Nr i filen, eller genom att ta bort hela rader, de Beer-Id som inte förekommer i filen nollställs)  </p>';
}
echo '<p>Om FV-Nr finns lagrade när registreringen stängt har bryggarna möjlighet att logga in och ladda ner sina nummerskyltar och QR-koder </p>';
echo '<p>Ett datum när detta tidigast är åtkomligt för bryggarna kan sättas i databasen. Nuvarande Datuminställning: ' . $_SESSION['fv_nr_time_visible_to_brewers'] . '</p>'; 
echo '<tr> ';
echo '<td class=header> Beer-Id </td> ';
echo '<td class=header> FV-Nr </td> ';
echo '<td class=header> Ölets namn </td> ';
echo '<td class=header> Öltyp </td> ';
echo '<td class=header> FV-klass </td> ';
echo '<td class=header> Registrerad av </td> ';
echo '<td class=header> Bryggare </td> ';
echo '<td class=header> Stå bredvid </td> ';
echo '<td class=header> OG [g/l] </td> ';
echo '<td class=header> FG [g/l] </td> ';
echo '<td class=header> Beska [BU] </td> ';
echo '<td class=header> Alk [vol %] </td> ';
echo '<td class=header> Prisplats </td> ';
echo '<td class=header> Receptlänk </td> ';
echo '<td class=header> e-post </td> ';
echo '<td class=header> Adress </td> ';
echo '</tr>';
$line = 0;
while ($row = mysqli_fetch_array($result)) {
  $_SESSION['fv_beer_id'][$line] = $row['beer_id'];
  $fv_competition_no = $row['fv_competition_no'];
  $user_id = $row['user_id'];
  $beer_name = $row['beer_name'];
  $main_class = $row['main_class'];
  $sub_class = $row['sub_class'];
  $type_id = $row['main_class'] . ":" . $row['sub_class'];
  if (!empty($row['low_alc'])) {
    $low_alc = "F";
  } else {
    $low_alc = "";
  }
  // Hämta namn till user_id.
  $query2 = "SELECT name FROM User_data " .
    "WHERE user_id = " . $user_id .
    " AND deleted = 0";
  $result2 = mysqli_query($dbc, $query2);
  if (mysqli_num_rows($result2) == 1) {
    $row2 = mysqli_fetch_array($result2);
    $user_name = $row2['name'];
  } else {
    die("fv_list_unsorted.php: No data or too many data found in User_data.");
  }
  // Hämta bryggarnas namn och e-post.
  $query2 = "SELECT Brewers_of_beer.brewer_id, Brewers.brewer_name, Brewers.brewer_email FROM Brewers_of_beer " .
    "INNER JOIN Brewers USING (brewer_id) " .
    "WHERE Brewers_of_beer.beer_id = " . $_SESSION['fv_beer_id'][$line] . " " .
    "AND Brewers_of_beer.deleted = 0";
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $result2 = mysqli_query($dbc, $query2);
  $i = 0;
  $brewer_names = "";
  $brewer_emails = "";
  while ($row2 = mysqli_fetch_array($result2)) {
    if ($i > 0) {
      $brewer_names = $brewer_names . ", " . $row2['brewer_name'];
      $brewer_emails = $brewer_emails . " " . $row2['brewer_email'];
    } else {
      $brewer_names = $row2['brewer_name'];
      $brewer_emails = $row2['brewer_email'];
    }
    $i++;
  }
  // Översätt type_id till text.
  $type_name = "";
  for ($i = 0; $i < count($_SESSION['type_values']); $i++) {
    if ($_SESSION['type_values'][$i] == $type_id) {
      $type_name = $_SESSION['type_names'][$i];
    }
  }
  $og = $row['og'];
  $fg = $row['fg'];
  $bu = $row['bu'];
  $alc = $row['alc'];
  // Hämta next_to_id, beer_slots, bar_length från Fv_event_reg.
  $query2 = "SELECT next_to_id, beer_slots, bar_length FROM Fv_event_reg " .
    "WHERE fv_event_id = " . $_SESSION['fv_event_id'] . " " .
    "AND user_id = " . $user_id . " " .
    "AND deleted = 0";
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $result2 = mysqli_query($dbc, $query2);
  $num_rows = mysqli_num_rows($result2);
  if ($num_rows == 0) {
    $next_to_id = 0;
    $beer_slots = 0;
    $bar_length = 0;
  }
  if ($num_rows == 1) {
    $row2 = mysqli_fetch_array($result2);
    $next_to_id = $row2['next_to_id'];
    $beer_slots = $row2['beer_slots'];
    $bar_length = $row2['bar_length'];
  }
  if ($num_rows > 1) {
    die("fv_list_unsorted.php: Too many rows in Fv_event_reg.");
  }
  // Översätt next_to_id till namn.
  $query3 = "SELECT name FROM User_data " .
    "WHERE user_id = '$next_to_id' " .
    "AND deleted = 0";
  $result3 = mysqli_query($dbc, $query3);
  $row3 = mysqli_fetch_array($result3);
  $next_to_name = "";
  if (isset($row3['name']))
    $next_to_name = $row3['name'];
  // Hämta price från Prices.
  $query2 = "SELECT price FROM Prices " .
    "WHERE beer_id = " . $_SESSION['fv_beer_id'][$line] . " " .
    "AND event_id = " . $_SESSION['fv_event_id'] . " " .
    "AND deleted = 0";
   
  $result2 = mysqli_query($dbc, $query2);
  if (mysqli_num_rows($result2) == 0) {
    $price = "";
  }
  if (mysqli_num_rows($result2) == 1) {
    $row2 = mysqli_fetch_array($result2);
    $price = $row2['price'];
  }
  if (mysqli_num_rows($result2) > 1) {
    die("fv_list_unsorted.php: No data or too many data found in Prices.");
  }

  echo '<tr> ';
  echo '<td> ' . $row["beer_id"] . ' </td> ';
  echo '<td> ' . $fv_competition_no . ' </td> ';
  echo '<td> ' . $beer_name . ' </td> ';
  echo '<td> ' . $type_name . ' </td> ';
  echo '<td> ' . FvClass($low_alc, $main_class, $sub_class) . ' </td> ';
  echo '<td> ' . $user_name . "($beer_slots pl, $bar_length cm)" . ' </td> ';
  echo '<td> ' . $brewer_names . ' </td> ';
  echo '<td> ' . $next_to_name . ' </td> ';
  echo '<td> ' . ($og+1000) . ' </td> ';
  echo '<td> ' . ($fg+1000) . ' </td> ';
  echo '<td> ' . $bu . ' </td> ';
  echo '<td> ' . $alc . ' </td> ';
  if ($_SESSION['adm_lev'] == 'FULL') {
    echo '<td> <select name="price[]" id="price[]"> ';
    echo '<option value="" ';
    if ($price == "") {
      echo ("selected");
    };
    echo '></option>';
    echo '<option value="Guld" ';
    if ($price == "Guld") {
      echo ("selected");
    };
    echo '>Guld</option>';
    echo '<option value="Silver" ';
    if ($price == "Silver") {
      echo ("selected");
    };
    echo '>Silver</option>';
    echo '<option value="Brons" ';
    if ($price == "Brons") {
      echo ("selected");
    };
    echo '>Brons</option>';
    echo '<option value="SM-vinnare" ';
    if ($price == "SM-vinnare") {
      echo ("selected");
    };
    echo '>SM-vinnare</option>';
    echo '</select> </td> ';
  } else {
    echo '<td> ' . $price . ' </td> ';
  }
  echo '<td> <a href="recipe.php?beer_id=' . $_SESSION['fv_beer_id'][$line] . '">Recept</a> </td> ';
  echo '<td> ' . $brewer_emails . ' </td> ';
  echo '<td> <a href="user_addr.php?user_id=' . $user_id . '">Adress</a> </td> ';
  echo '</tr>';

  $line++;
}
$_SESSION['no_fv_beers'] = $line;
echo '</table>';
echo '</form> ';
mysqli_close($dbc)
?>

<?php
// Sidfot.
require_once('footer.php');
?>

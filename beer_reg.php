<?php

function GetBeerForm ($dbc) {
  // Hämta data från formuläret.
  if ($_SESSION['dt_event_open']) {
    if (empty($_POST['comp_dt'])) {
      $_SESSION['comp_dt'] = 0;
    } else {
      $_SESSION['comp_dt'] = 1;
    }
  }
  if ($_SESSION['fv_event_open']) {
    if (empty($_POST['comp_fv'])) {
      $_SESSION['comp_fv'] = 0;
    } else {
      $_SESSION['comp_fv'] = 1;
    }
  }
  if ($_SESSION['et_event_open']) {
    if (empty($_POST['comp_et'])) {
      $_SESSION['comp_et'] = 0;
    } else {
      $_SESSION['comp_et'] = 1;
    }
  }
  if ($_SESSION['fv_event_open']) {
    if (empty($_POST['low_alc'])) {
      $_SESSION['low_alc_sel'] = 0;
    } else {
      $_SESSION['low_alc_sel'] = 1;
    }
  }
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['beer_name'] = FilterPost ($dbc, $_POST['beer_name'], 40);
  }
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et'] && $_SESSION['fv_allow_user_votesys_category_selection']) {
      $_SESSION['votesys_category'] = FilterPost($dbc, $_POST['votesys_category'], 80);
  }
  $_SESSION['mashing'] = FilterPost ($dbc, $_POST['mashing'], 1000);
  $_SESSION['ferment'] = FilterPost ($dbc, $_POST['ferment'], 1000);
  $_SESSION['water'] = FilterPost ($dbc, $_POST['water'], 1000);
  $_SESSION['comment'] = FilterPost ($dbc, $_POST['comment'], 1000);
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['og'] = FilterPost ($dbc, $_POST['og'], 10) - 1000;
  } 
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['fg'] = FilterPost ($dbc, $_POST['fg'], 10) - 1000;
  }
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['bu'] = FilterPost ($dbc, $_POST['bu'], 10);
  }
  if (!$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['alc'] = FilterPost ($dbc, $_POST['alc'], 10);
  }
  $_SESSION['volume'] = FilterPost ($dbc, $_POST['volume'], 10);
  if (!$_SESSION['recipe_chg_blocked_dt'] && !$_SESSION['recipe_chg_blocked_fv'] && !$_SESSION['recipe_chg_blocked_et']) {
    $_SESSION['beer_type'] = FilterPost ($dbc, $_POST['beer_type'], 80);
  }
  // Sätt $_SESSION['main_class'] och $_SESSION['sub_class'].
  $i = 0;
  
  $_SESSION['main_class'] = NULL;
  $_SESSION['sub_class'] = NULL;
  
  foreach ($_SESSION['type_values'] as $type_value) {
    if ($type_value == $_SESSION['beer_type']) {
      $_SESSION['main_class'] = $_SESSION['type_main'][$i];
      $_SESSION['sub_class'] = $_SESSION['type_sub'][$i];
    }
    $i++;
  }
  for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
    $_SESSION['brewers_name'][$i] = FilterPost ($dbc, $_POST['brewers_name'][$i], 80);
    $_SESSION['brewers_email'][$i] = FilterPost ($dbc, $_POST['brewers_email'][$i], 80);
  }
  for ($i=0; $i<$_SESSION['no_hops']; $i++) {
    $_SESSION['hops_name'][$i] = FilterPost ($dbc, $_POST['hops_name'][$i], 50);
    $_SESSION['hops_form_id_sel'][$i] = FilterPost ($dbc, $_POST['hops_form_id_sel'][$i], 80);
    $_SESSION['hops_weight'][$i] = FilterPost ($dbc, $_POST['hops_weight'][$i], 10);
    $_SESSION['hops_alpha'][$i] = FilterPost ($dbc, $_POST['hops_alpha'][$i], 10);
    $_SESSION['hops_boil_time'][$i] = FilterPost ($dbc, $_POST['hops_boil_time'][$i], 10);
    $_SESSION['hops_comment'][$i] =FilterPost ($dbc, $_POST['hops_comment'][$i], 50);
  }
  for ($i=0; $i<$_SESSION['no_malts']; $i++) {
    $_SESSION['malts_name'][$i] = FilterPost ($dbc, $_POST['malts_name'][$i], 50);
    $_SESSION['malts_weight'][$i] = FilterPost ($dbc, $_POST['malts_weight'][$i], 10);
    $_SESSION['malts_comment'][$i] = FilterPost ($dbc, $_POST['malts_comment'][$i], 50);
  }
  for ($i=0; $i<$_SESSION['no_others']; $i++) {
    $_SESSION['others_name'][$i] = FilterPost ($dbc, $_POST['others_name'][$i], 50);
    $_SESSION['others_stage_id_sel'][$i] = FilterPost ($dbc, $_POST['others_stage_id_sel'][$i], 80);
    $_SESSION['others_weight'][$i] = FilterPost ($dbc, $_POST['others_weight'][$i], 10);
    $_SESSION['others_comment'][$i] = FilterPost ($dbc, $_POST['others_comment'][$i], 50);
  }
}


  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("beer_reg "."Connection failed: " . mysqli_connect_error());
  }
  
  // Sätt värden på variabler.
  $dbg_msg = "";
  $err_msg = "";
  $ok = 1;
  if ($_SESSION['comp_dt'] && !$_SESSION['dt_event_open']) {
    $_SESSION['recipe_chg_blocked_dt'] = 1;
  } else {
    $_SESSION['recipe_chg_blocked_dt'] = 0;
  }
  if ($_SESSION['comp_fv'] && !$_SESSION['fv_event_open']) {
    $_SESSION['recipe_chg_blocked_fv'] = 1;
  } else {
    $_SESSION['recipe_chg_blocked_fv'] = 0;
  }
  if ($_SESSION['comp_et'] && !$_SESSION['et_event_open']) {
    $_SESSION['recipe_chg_blocked_et'] = 1;
  } else {
    $_SESSION['recipe_chg_blocked_et'] = 0;
  }
  
  // Lägg till bryggare.
  if (isset($_POST['add_brewer'])) {
    $_SESSION['autofocus_sel'] = "brewers";
    // Hämta data från formuläret.
    GetBeerForm($dbc);
    // Lägg till en rad med bryggare.
    $_SESSION['brewers_name'][$_SESSION['no_brewers']] = "";
    $_SESSION['brewers_email'][$_SESSION['no_brewers']] = "";
    $_SESSION['no_brewers']++;
    // Ladda sidan igen.
    ReDirect ('beer_reg.php');
  }

  // Ta bort bryggare.
  // print_r($_POST);
  if (isset($_POST['del_brewer'])) {
    $_SESSION['autofocus_sel'] = "brewers";
    for ($j = 1; $j < $_SESSION['no_brewers']; $j++) {
      if (isset($_POST['del_brewer'][$j])) {
      $_SESSION['autofocus_sel'] = "brewer";
        // Hämta data från formuläret.
        GetBeerForm($dbc);
        // Ta bort en bryggarrad.
        $_SESSION['no_brewers']--;
        for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
          if ($i >= $j) {
            $_SESSION['brewers_name'][$i] = $_SESSION['brewers_name'][$i+1];
            $_SESSION['brewers_email'][$i] = $_SESSION['brewers_email'][$i+1];
          }
        }
        // Ladda sidan igen.
    ReDirect ('beer_reg.php');
      }
    }
  }

  // Lägg till maltrad.
  if (isset($_POST['add_malt'])) {
    $_SESSION['autofocus_sel'] = "malts";
    // Hämta data från formuläret.
    GetBeerForm($dbc);
    // Lägg till en maltrad.
    $_SESSION['malts_name'][$_SESSION['no_malts']] = "";
    $_SESSION['malts_weight'][$_SESSION['no_malts']] = "";
    $_SESSION['malts_comment'][$_SESSION['no_malts']] = "";
    $_SESSION['no_malts']++;
    // Ladda sidan igen.
    ReDirect ('beer_reg.php');
  }
  
  // Ta bort maltrad.
  // print_r($_POST);
    for ($j = 0; $j < $_SESSION['no_malts']; $j++) {
      if (isset($_POST['del_malt'][$j])) {
      $_SESSION['autofocus_sel'] = "malts";
        // Hämta data från formuläret.
        GetBeerForm($dbc);
        // Ta bort en maltrad.
        $_SESSION['no_malts']--;
        for ($i=0; $i<$_SESSION['no_malts']; $i++) {
          if ($i >= $j) {
            $_SESSION['malts_name'][$i] = $_SESSION['malts_name'][$i+1];
            $_SESSION['malts_weight'][$i] = $_SESSION['malts_weight'][$i+1];
            $_SESSION['malts_comment'][$i] = $_SESSION['malts_comment'][$i+1];
          }
        }
        // Ladda sidan igen.
    ReDirect ('beer_reg.php');
      }
    }

  // Lägg till humlerad.
  if (isset($_POST['add_hops'])) {
    $_SESSION['autofocus_sel'] = "hops";
    // Hämta data från formuläret.
    GetBeerForm($dbc);
    // Lägg till en humlerad.
    $_SESSION['hops_name'][$_SESSION['no_hops']] = "";
    $_SESSION['hops_form_id_sel'][$_SESSION['no_hops']] = NULL;
    $_SESSION['hops_alpha'][$_SESSION['no_hops']] = NULL;
    $_SESSION['hops_weight'][$_SESSION['no_hops']] = NULL;
    $_SESSION['hops_boil_time'][$_SESSION['no_hops']] = NULL;
    $_SESSION['hops_comment'][$_SESSION['no_hops']] = "";
    $_SESSION['no_hops']++;
    // Ladda sidan igen.
    ReDirect ('beer_reg.php');
  }
  
  // Ta bort humlerad.
    for ($j = 0; $j < $_SESSION['no_hops']; $j++) {
      if (isset($_POST['del_hops'][$j])) {
        $_SESSION['autofocus_sel'] = "hops";
        // Hämta data från formuläret.
        GetBeerForm($dbc);
        // Ta bort en humlerad.
        $_SESSION['no_hops']--;
        for ($i=0; $i<$_SESSION['no_hops']; $i++) {
          if ($i >= $j) {
            $_SESSION['hops_name'][$i] = $_SESSION['hops_name'][$i+1];
            $_SESSION['hops_form_id_sel'][$i] = $_SESSION['hops_form_id_sel'][$i+1];
            $_SESSION['hops_alpha'][$i] = $_SESSION['hops_alpha'][$i+1];
            $_SESSION['hops_weight'][$i] = $_SESSION['hops_weight'][$i+1];
            $_SESSION['hops_boil_time'][$i] = $_SESSION['hops_boil_time'][$i+1];
            $_SESSION['hops_comment'][$i] = $_SESSION['hops_comment'][$i+1];
          }
        }
        // Ladda sidan igen.
    ReDirect ('beer_reg.php');
      }
    }
  
  // Lägg till övrigtrad.
  if (isset($_POST['add_others'])) {
    $_SESSION['autofocus_sel'] = "others";
    // Hämta data från formuläret.
    GetBeerForm($dbc);
    // Lägg till en humlerad.
    $_SESSION['others_name'][$_SESSION['no_others']] = "";
    $_SESSION['others_stage_id_sel'][$_SESSION['no_others']] = NULL;
    $_SESSION['others_weight'][$_SESSION['no_others']] = NULL;
    $_SESSION['others_comment'][$_SESSION['no_others']] = "";
    $_SESSION['no_others']++;
    // Ladda sidan igen.
    ReDirect ('beer_reg.php');
  }
  
  // Ta bort övrigtrad.
    for ($j = 0; $j < $_SESSION['no_others']; $j++) {
      if (isset($_POST['del_others'][$j])) {
        $_SESSION['autofocus_sel'] = "others";
        // Hämta data från formuläret.
        GetBeerForm($dbc);
        // Ta bort en humlerad.
        $_SESSION['no_others']--;
        for ($i=0; $i<$_SESSION['no_others']; $i++) {
          if ($i >= $j) {
            $_SESSION['others_name'][$i] = $_SESSION['others_name'][$i+1];
            $_SESSION['others_stage_id_sel'][$i] = $_SESSION['others_stage_id_sel'][$i+1];
            $_SESSION['others_weight'][$i] = $_SESSION['others_weight'][$i+1];
            $_SESSION['others_comment'][$i] = $_SESSION['others_comment'][$i+1];
          }
        }
        // Ladda sidan igen.
    ReDirect ('beer_reg.php');
      }
    }
  
  if (isset($_POST['add_beer'])) {
    $_SESSION['autofocus_sel'] = "";
    // Hämta data från formuläret.
    GetBeerForm($dbc);

    // Kontrollerna nedan av lediga bord och max antal öl kan inte göras direkt när sidan laddas. Vi måste 
    // först veta till vilka tävlingar bryggaren vill anmäla sitt öl. Om max antal öl för tävlingen är satt 
    // till noll så görs ingen kontroll av antal anmälda öl.
    // Kontrollera att det finns lediga bord.
    if ( $_SESSION['comp_fv'] && (FreeTables ($dbc) == 0) ) {
      $err_msg = $err_msg."Det finns inga lediga bord till folkets val. \r\n";
      $ok = 0;
    }
    // Kontrollera om max antal öl till DT har nåtts.
    if ( $_SESSION['comp_dt'] && ($_SESSION['dt_max_no_beers'] != 0) && 
       (NoRegBeers ($_SESSION['dt_event_id']) >= $_SESSION['dt_max_no_beers']) ) {
      $err_msg = $err_msg."Inga fler öl kan anmälas till domartävlingen. \r\n";
      $ok = 0;
    }
    // Kontrollera om max antal öl till FV har nåtts.
    if ( $_SESSION['comp_fv'] && ($_SESSION['fv_max_no_beers'] != 0) && 
       (NoRegBeers ($_SESSION['fv_event_id']) >= $_SESSION['fv_max_no_beers']) ) {
      $err_msg = $err_msg."Inga fler öl kan anmälas till folkets val. \r\n";
      $ok = 0;
    }
    // Kontrollera om max antal öl till ET har nåtts.
    if ( $_SESSION['comp_et'] && ($_SESSION['et_max_no_beers'] != 0) && 
       (NoRegBeers ($_SESSION['et_event_id']) >= $_SESSION['et_max_no_beers']) ) {
      $err_msg = $err_msg."Inga fler öl kan anmälas till etikettävlingen. \r\n";
      $ok = 0;
    }
//$err_msg = $err_msg."comp_fv=".$_SESSION['comp_fv']."\r\n".
//"fv_max_no_beers=".$_SESSION['fv_max_no_beers']."\r\n".
//"fv_event_id=".$_SESSION['fv_event_id']."\r\n".
//"NoRegBeers=".NoRegBeers ($_SESSION['fv_event_id'])."\r\n";
//$ok = 0;
//$err_msg = $err_msg."comp_dt=".$_SESSION['comp_dt']."\r\n".
//"dt_max_no_beers=".$_SESSION['dt_max_no_beers']."\r\n".
//"dt_event_id=".$_SESSION['dt_event_id']."\r\n".
//"NoRegBeers=".NoRegBeers ($_SESSION['dt_event_id'])."\r\n";
//$ok = 0;

    // Kontrollera att alla fält är ifyllda.
    if ( empty($_SESSION['beer_type']) ) {
      $err_msg = $err_msg."Öltyp måste fyllas i. \r\n";
      $ok = 0;
    }
    if ( empty($_SESSION['beer_name']) ) {
      $err_msg = $err_msg."Ölnamn måste fyllas i. \r\n";
      $ok = 0;
    }
    //ratingssystemet
    if ($_SESSION['fv_allow_user_votesys_category_selection'] == 1
        && $_SESSION['comp_fv'] == 1) {
      if ( empty($_SESSION['votesys_category']) ) {
        $err_msg = $err_msg."Tävlingsklass Folkets val måste fyllas i. \r\n";
        $ok = 0;
      }
    }
    if ( empty($_SESSION['brewers_name'][0]) ) {
      $err_msg = $err_msg."Minst en bryggare måste fyllas i. \r\n";
      $ok = 0;
    }
    if ( !is_numeric($_SESSION['og']) ) {
      $err_msg = $err_msg."OG måste fyllas i. \r\n";
      $ok = 0;
    } elseif ($_SESSION['og'] < -700) {
      $err_msg = $err_msg."OG ska anges i gram/liter. \r\n";
      $ok = 0;
    }
    if ( !is_numeric($_SESSION['fg']) ) {
      $err_msg = $err_msg."FG måste fyllas i. \r\n";
      $ok = 0;
    } elseif ($_SESSION['fg'] < -700) {
      $err_msg = $err_msg."FG ska anges i gram/liter. \r\n";
      $ok = 0;
    }
    if ( !is_numeric($_SESSION['bu']) ) {
      $err_msg = $err_msg."BU måste fyllas i. \r\n";
      $ok = 0;
    }
    if ( !is_numeric($_SESSION['alc']) ) {
      $err_msg = $err_msg."Alkoholhalt måste fyllas i. \r\n";
      $ok = 0;
    }

    // Kontrollera att någon tävlan är ikryssad.
    if (!$_SESSION['comp_dt'] && !$_SESSION['comp_fv'] && !$_SESSION['comp_et']) {
        $err_msg = $err_msg."Du måste kryssa i nån av tävlingarna. \r\n";
        $ok = 0;
    }

    // Man kan inte anmäla ett öl endast till ET.
    if ( (!$_SESSION['comp_dt'] && !$_SESSION['comp_fv']) && $_SESSION['comp_et']) {
        $err_msg = $err_msg."Man kan inte anmäla ett öl endast till etikettävlingen. \r\n";
        $ok = 0;
    }

    // Kontrollera OG-max om ölet ska till DT.
    if ($_SESSION['comp_dt']) {
      $ogmax = NULL;
      for ($i=0; $i<count($_SESSION['type_values']); $i++) {
        if (($_SESSION['type_values'][$i] == $_SESSION['beer_type']) && !empty($_SESSION['type_ogmax'][$i])) {
          $ogmax = $_SESSION['type_ogmax'][$i];
        }
      }
      if ( ($ogmax != NULL) && ($_SESSION['og'] > $ogmax) ) {      
        $err_msg = $err_msg."OG får inte vara högre än ".($ogmax+1000).". \r\n";
        $ok = 0;
      }
    }

    // Kontrollera alc-max om ölet ska till DT.
    if ($_SESSION['comp_dt']) {
      $alcmax = NULL;
      for ($i=0; $i<count($_SESSION['type_values']); $i++) {
        if (($_SESSION['type_values'][$i] == $_SESSION['beer_type']) && !empty($_SESSION['type_alcmax'][$i])) {
          $alcmax = $_SESSION['type_alcmax'][$i];
        }
      }
      if ( ($alcmax != NULL) && ($_SESSION['alc'] > $alcmax) ) {
        $err_msg = $err_msg."Alkoholhalten får inte vara högre än ".$alcmax.". \r\n";
        $ok = 0;
      }
    }

    // Kontrollera alc om ölet ska till klassen Folköl i FV.
    if ( ($_SESSION['low_alc_sel']) && ($_SESSION['alc'] > 3.5) ) {
      $err_msg = $err_msg."Alkoholhalten får inte vara högre än 3,5 % för folköl. \r\n";
      $ok = 0;
    }

    // Kontrollera att FV är kryssat om ölet ska till klassen Folköl i FV.
    if ( $_SESSION['low_alc_sel'] && !$_SESSION['comp_fv'] ) {
      $err_msg = $err_msg."Du måste välja folkets val om du vill att ölet ska räknas som folköl i FV. \r\n";
      $ok = 0;
    }

    // Mjöd och cider kan inte anmälas till domartävlingen.
    if ($_SESSION['comp_dt'] && ( ($_SESSION['beer_type'] == "12:A") || ($_SESSION['beer_type'] == "12:B") ) ) {
        $err_msg = $err_msg."Mjöd och cider kan inte anmälas till domartävlan. \r\n";
        $ok = 0;
    }

    // Det får inte finnas dubletter i bryggarlistan.
    $brewer_dup = 0;
    for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
      $brewer_hit = 0;
      for ($j=0; $j<$_SESSION['no_brewers']; $j++) {
        if ( ($_SESSION['brewers_name'][$i] == $_SESSION['brewers_name'][$j]) AND 
             ($_SESSION['brewers_email'][$i] == $_SESSION['brewers_email'][$j]) ) {
          $brewer_hit++;
        }
      }
      if ($brewer_hit > 1) {
        $brewer_dup = 1;
      }
    }
    if ($brewer_dup) {
        $err_msg = $err_msg."Det får inte finnas dubletter i bryggarlistan. \r\n";
        $ok = 0;
    }

   // Kontrollera bryggarnas e-postformat.
    for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
      // Kontrollera formatet om e-postadress fyllts i.
      if (!empty($_SESSION['brewers_email'][$i])) {
		    if (!filter_var($_SESSION['brewers_email'][$i], FILTER_VALIDATE_EMAIL)) {
		      $err_msg = $err_msg.$_SESSION['brewers_email'][$i]." är ogiltigt e-postformat. \r\n";
		      $ok = 0;
		    }
      }
    }

    // Sätt $_SESSION['sel_type_name'].
    $i = 0;
    $_SESSION['sel_type_name'] = NULL;
    foreach ($_SESSION['type_names'] as $type_name) {
      if ($_SESSION['type_values'][$i] == $_SESSION['beer_type']) {
        $_SESSION['sel_type_name'] = $type_name;
      }
      $i++;
    }

    if ($ok) {
      // Lägg till ölet om det inte uppdateras.
      if (!$_SESSION['update']) {
        // Lägg till ölet i Beers så vi får ett beer_id.
        $query = "INSERT INTO Beers (user_id) ".
                 "VALUES ('".$_SESSION['user_id']."')";
        if (mysqli_query($dbc, $query)) {
          $_SESSION['beer_id'] = mysqli_insert_id($dbc);
        } else { die("beer_reg.Beers 1 ".mysqli_error($dbc).$query); }
      }
      // Öldata
      if ($_SESSION['update']) {
        // Kontrollera om något data ändrats
        $query = "SELECT * FROM Beer_data ".
                 "WHERE beer_id = ".$_SESSION['beer_id']." AND ".
                 "deleted = 0";
        $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
        $row = mysqli_fetch_array($result);
        if (mysqli_num_rows($result) != 1) {
          die("beer_reg.Beer_data "."Incorrect number of rows in Beer_data.");
        }
        if ($_SESSION['main_class'] != $row['main_class'] ||
            $_SESSION['sub_class'] != $row['sub_class'] ||
            $_SESSION['votesys_category'] != $row['votesys_category'] ||
            $_SESSION['beer_name'] != $row['beer_name'] ||
            $_SESSION['volume'] != $row['volume'] ||
            $_SESSION['low_alc_sel'] != $row['low_alc'] ||
            $_SESSION['og'] != $row['og']-1000 ||
            $_SESSION['fg'] != $row['fg']-1000 ||
            $_SESSION['bu'] != $row['bu'] ||
            $_SESSION['alc'] != $row['alc'] ||
            $_SESSION['mashing'] != $row['mashing'] ||
            $_SESSION['ferment'] != $row['ferment'] ||
            $_SESSION['water'] != $row['water'] ||
            $_SESSION['comment'] != $row['comment']) {
          $row = mysqli_fetch_array($result);       
          // Markera nuvarande rad i Beer_data som raderad.
          $query = "UPDATE Beer_data SET deleted = 1 ".
                   "WHERE beer_id = ".$_SESSION['beer_id']." AND ".
                   "deleted = 0";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Beer_data ".mysqli_error($dbc).$query);
          }
          // Lägg till ny rad i Beer_data om vi uppdaterar.
          $query = "INSERT INTO Beer_data (beer_id, main_class, sub_class,votesys_category, type_name, low_alc, type_def, ".
                                          "beer_name, volume, og, fg, bu, alc, mashing, ferment, water, comment) ".
                   "VALUES ('".$_SESSION['beer_id']."', '".$_SESSION['main_class']."', '".$_SESSION['sub_class']."', '".$_SESSION['votesys_category'].
                   "', '".$_SESSION['sel_type_name']."', '".$_SESSION['low_alc_sel']."', '".$_SESSION['type_def']."', '".$_SESSION['beer_name'].
                   "', '".$_SESSION['volume']."', '".$_SESSION['og']."', '".$_SESSION['fg']."', '".$_SESSION['bu'].
                   "', '".$_SESSION['alc']."', '".$_SESSION['mashing'].
                   "', '".$_SESSION['ferment']."', '".$_SESSION['water']."', '".$_SESSION['comment']."')";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Beer_data ".mysqli_error($dbc).$query);
          }
        }
      } else {
        // Lägg till ny rad i Beer_data om vi lägger till ett öl.
          $query = "INSERT INTO Beer_data (beer_id, main_class, sub_class, votesys_category, type_name, low_alc, type_def, ".
                                          "beer_name, volume, og, fg, bu, alc, mashing, ferment, water, comment) ".
                   "VALUES ('".$_SESSION['beer_id']."', '".$_SESSION['main_class']."', '".$_SESSION['sub_class']."', '".$_SESSION['votesys_category'].
                   "', '".$_SESSION['sel_type_name']."', '".$_SESSION['low_alc_sel']."', '".$_SESSION['type_def']."', '".$_SESSION['beer_name'].
                   "', '".$_SESSION['volume']."', '".$_SESSION['og']."', '".$_SESSION['fg']."', '".$_SESSION['bu'].
                   "', '".$_SESSION['alc']."', '".$_SESSION['mashing'].
                   "', '".$_SESSION['ferment']."', '".$_SESSION['water']."', '".$_SESSION['comment']."')";
        if (!mysqli_query($dbc, $query)) {
          die("beer_reg.Beer_data ".mysqli_error($dbc).$query);
        }
      }
      
      // Bryggarna
      // Ta bort eventuella bryggare i Brewers_of_beer som inte finns med i formuläret.
      // Görs ifall användaren raderat några.
      for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
        $in_db[$i] = 0;
      }
      // Tar fram lista på dom som finns i Brewers_of_beer.
      $query = "SELECT Brewers_of_beer.brewers_of_beer_id, Brewers_of_beer.brewer_id, ".
               "Brewers.brewer_name, Brewers.brewer_email FROM Brewers_of_beer ".
               "INNER JOIN Brewers USING (brewer_id) ".
               "WHERE Brewers_of_beer.beer_id = ".$_SESSION['beer_id'].
               "  AND Brewers_of_beer.deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        $brewers_of_beer_id = $row['brewers_of_beer_id'];
        $brewer_name = $row['brewer_name'];
        $brewer_email = $row['brewer_email'];
        $in_form = 0;
        // Kolla varje namn i listan om namnet finns i formuläret.
        for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
          if ( $_SESSION['brewers_name'][$i] == $row['brewer_name'] &&
               $_SESSION['brewers_email'][$i] == $row['brewer_email'] ) {
            $in_form = 1;
            $in_db[$i] = 1;
          }
        }
        // Den som inte finns i formuläret markeras som raderad i Brewers_of_beer.
        if (!$in_form) {
          $query = "UPDATE Brewers_of_beer ".
                   "SET deleted=1 ".
                   "WHERE brewers_of_beer_id=".$row['brewers_of_beer_id'];
          if (!mysqli_query($dbc, $query)) { die("beer_reg.Brewers_of_beer ".mysqli_error($dbc).$query); }
        }
      }
      for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
        // Kolla att namn är ifyllt och att bryggaren inte redan finns i databasen.
        if (!empty($_SESSION['brewers_name'][$i]) && $in_db[$i] == 0) {
          // Hämta brewer_id från tabellen Brewers. 
          $query = "SELECT brewer_id FROM Brewers ".
                   "WHERE brewer_name = '".$_SESSION['brewers_name'][$i]."' AND ".
                   "user_id = ".$_SESSION['user_id']." ".
                   "AND brewer_email = '".$_SESSION['brewers_email'][$i]."' ".
                   "AND deleted = 0";
          $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
          // Lägg till bryggaren i Brewers om bryggaren inte fanns där.
          if (mysqli_num_rows($result) == 0) {
            $query = "INSERT INTO Brewers (user_id, brewer_name, brewer_email) ".
                     "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['brewers_name'][$i].
                     "', '".$_SESSION['brewers_email'][$i]."')";
            if (mysqli_query($dbc, $query)) {
                $brewer_id = mysqli_insert_id($dbc);
            } else { die("beer_reg.Brewers ".mysqli_error($dbc).$query); }
          } else {
            $row = mysqli_fetch_array($result);
            $brewer_id = $row['brewer_id'];
          }
          // Lägg till bryggaren i Brewers_of_beer.
          $query = "INSERT INTO Brewers_of_beer (beer_id, brewer_id) ".
                   "VALUES (".$_SESSION['beer_id'].", ".
                    $brewer_id.")";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Brewers_of_beer ".mysqli_error($dbc).$query);
          }          
        }       
      }

      // Humle
      // Ta bort humle i listan som inte finns med i formuläret.
      for ($i=0; $i<$_SESSION['no_hops']; $i++) {
        $in_db[$i] = 0;
      }
      $query =  "SELECT Hops_in_beer.hops_weight, Hops.hops_name, ".
			          "Hops.hops_form_id, Hops.hops_alpha, Hops_in_beer.hops_boil_time, ".
			          "Hops_in_beer.hops_comment, Hops_in_beer.hops_in_beer_id ".
			          "FROM Hops_in_beer ".
			          "INNER JOIN Hops USING (hops_id) ".
			          "INNER JOIN Hops_forms USING (hops_form_id) ".
			          "WHERE Hops_in_beer.beer_id = ".$_SESSION['beer_id'].
			          "  AND Hops_in_beer.deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        $in_form = 0;
        for ($i=0; $i<$_SESSION['no_hops']; $i++) {
          if ( $_SESSION['hops_name'][$i] == $row['hops_name'] &&
               $_SESSION['hops_form_id_sel'][$i] == $row['hops_form_id'] &&
               $_SESSION['hops_weight'][$i] ==$row['hops_weight'] &&
               $_SESSION['hops_alpha'][$i] == $row['hops_alpha'] &&
               $_SESSION['hops_boil_time'][$i] == $row['hops_boil_time'] &&
               $_SESSION['hops_comment'][$i] == $row['hops_comment'] ) {
            $in_form = 1;
            $in_db[$i] = 1;
          }
        }
        if (!$in_form) {
          $query =  "UPDATE Hops_in_beer ".
                   "SET deleted=1 ".
                   "WHERE hops_in_beer_id=".$row['hops_in_beer_id'];
          if (!mysqli_query($dbc, $query)) { die("beer_reg.Hops_in_beer ".mysqli_error($dbc).$query); }
        }
      }
      for ($i=0; $i<$_SESSION['no_hops']; $i++) {
        // Kolla att namn och vikt är ifyllt och att humlen inte redan finns i listan.
        if (!empty($_SESSION['hops_name'][$i])  && 
            !empty($_SESSION['hops_weight'][$i]) && $in_db[$i] == 0) {
          // Hämta hops_id från tabellen Hops. Lägg till humlen i Hops om den inte finns där.
          $query = "SELECT hops_id FROM Hops WHERE  ".
                   "user_id = ".$_SESSION['user_id']." AND ".
                   "hops_name = '".$_SESSION['hops_name'][$i]."' AND ".
                   "hops_form_id = '".$_SESSION['hops_form_id_sel'][$i]."' AND ".
                   "hops_alpha = '".$_SESSION['hops_alpha'][$i]."' AND ".
                   "deleted = 0";
          $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
          if (mysqli_num_rows($result) == 0) {
            $hops_form_id_sel = $_SESSION['hops_form_id_sel'][$i];
            if (empty($_SESSION['hops_form_id_sel'][$i])) {$hops_form_id_sel = 0;}
            $hops_alpha = $_SESSION['hops_alpha'][$i];
            if (empty($_SESSION['hops_alpha'][$i])) {$hops_alpha = 0.0;}
            $query = "INSERT INTO Hops (user_id, hops_name, hops_form_id, hops_alpha) ".
                     "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['hops_name'][$i]."', '".
                     $hops_form_id_sel."', '".$hops_alpha."')";
            if (mysqli_query($dbc, $query)) {
                $hops_id = mysqli_insert_id($dbc);
            } else { die("beer_reg.Hops ".mysqli_error($dbc).$query); }
          } else {
            $row = mysqli_fetch_array($result);
            $hops_id = $row['hops_id'];
          }
          // Lägg till humlen i Hops_in_beer.
          $hops_boil_time = $_SESSION['hops_boil_time'][$i];
          if (empty($_SESSION['hops_boil_time'][$i])) {$hops_boil_time = 0;}
          $query = "INSERT INTO Hops_in_beer (beer_id, hops_id, hops_weight, hops_boil_time, hops_comment) ".
                   "VALUES ('".$_SESSION['beer_id']."', '".
                   $hops_id."', '".$_SESSION['hops_weight'][$i]."', '".$hops_boil_time."', '".
                   $_SESSION['hops_comment'][$i]."')";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Hops_in_beer ".mysqli_error($dbc).$query);
          }          
        }       
      }
      
      // Malt
      // Ta bort malter i listan som inte finns med i formuläret
      // om vi uppdaterar ett öl.
      for ($i=0; $i<$_SESSION['no_malts']; $i++) {
        $in_db[$i] = 0;
      }
      $query = "SELECT Malts_in_beer.malts_in_beer_id, Malts_in_beer.malt_id, ".
               "Malts.malt_name, Malts_in_beer.malt_weight, Malts_in_beer.malt_comment FROM Malts_in_beer ".
               "INNER JOIN Malts USING (malt_id) ".
               "WHERE Malts_in_beer.beer_id = ".$_SESSION['beer_id'].
               " AND Malts_in_beer.deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        $in_form = 0;
        for ($i=0; $i<$_SESSION['no_malts']; $i++) {
          if ( $_SESSION['malts_name'][$i] == $row['malt_name'] &&
               $_SESSION['malts_weight'][$i] == $row['malt_weight'] &&
               $_SESSION['malts_comment'][$i] == $row['malt_comment'] ) {
            $in_form = 1;
            $in_db[$i] = 1;
          }
        }
        if (!$in_form) {
          $query = "UPDATE Malts_in_beer ".
                   "SET deleted=1 ".
                   "WHERE malts_in_beer_id=".$row['malts_in_beer_id'];
          if (!mysqli_query($dbc, $query)) { die("beer_reg.Malts_in_beer ".mysqli_error($dbc).$query); }
        }
      }
      for ($i=0; $i<$_SESSION['no_malts']; $i++) {
        // Kolla att namn och vikt är ifyllt och att malten inte redan finns i listan.
        if (!empty($_SESSION['malts_name'][$i]) && 
            !empty($_SESSION['malts_weight'][$i]) && $in_db[$i] == 0) {
          // Hämta malt_id från tabellen Malts. Lägg till malten i Malts om den inte finns där.
          $query = "SELECT malt_id FROM Malts ".
                   "WHERE malt_name = '".$_SESSION['malts_name'][$i]."' AND ".   
                   "user_id = ".$_SESSION['user_id']." ".
                   "AND Malts.user_id = ".$_SESSION['user_id']." ".
                   "AND deleted = 0";
          $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
          if (mysqli_num_rows($result) == 0) {
            $query = "INSERT INTO Malts (user_id, malt_name) ".
                     "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['malts_name'][$i]."')";
            if (mysqli_query($dbc, $query)) {
                $malt_id = mysqli_insert_id($dbc);
            } else { die("beer_reg.Malts ".mysqli_error($dbc).$query); }
          } else {
            $row = mysqli_fetch_array($result);
            $malt_id = $row['malt_id'];
          }
          // Lägg till malten i Malts_in_beer.
          $query = "INSERT INTO Malts_in_beer (beer_id, malt_id, malt_weight, malt_comment) ".
                   "VALUES ('".$_SESSION['beer_id']."', '".
                    $malt_id."', '".$_SESSION['malts_weight'][$i]."', '".
                    $_SESSION['malts_comment'][$i]."')";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Malts_in_beer ".mysqli_error($dbc).$query);
          }          
        }
      }

      // Övrigt
      // Ta bort övrigt i listan som inte finns med i formuläret.
      for ($i=0; $i<$_SESSION['no_others']; $i++) {
        $in_db[$i] = 0;
      }
      $query =  "SELECT Others_in_beer.others_weight, Others.others_name, ".
			          "Others.others_stage_id, ".
			          "Others_in_beer.others_comment, Others_in_beer.others_in_beer_id ".
			          "FROM Others_in_beer ".
			          "INNER JOIN Others USING (others_id) ".
			          "INNER JOIN Others_stages USING (others_stage_id) ".
			          "WHERE Others_in_beer.beer_id = ".$_SESSION['beer_id'].
			          "  AND Others_in_beer.deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        $in_stage = 0;
        for ($i=0; $i<$_SESSION['no_others']; $i++) {
          if ( $_SESSION['others_name'][$i] == $row['others_name'] && 
               $_SESSION['others_stage_id_sel'][$i] == $row['others_stage_id'] && 
               $_SESSION['others_weight'][$i] ==$row['others_weight'] && 
               $_SESSION['others_comment'][$i] == $row['others_comment'] ) {
            $in_stage = 1;
            $in_db[$i] = 1;
          }
        }
        if (!$in_stage) {
          $query =  "UPDATE Others_in_beer ".
                   "SET deleted=1 ".
                   "WHERE others_in_beer_id=".$row['others_in_beer_id'];
          if (!mysqli_query($dbc, $query)) { die("beer_reg.Others_in_beer ".mysqli_error($dbc).$query); }
        }
      }
      for ($i=0; $i<$_SESSION['no_others']; $i++) {
        // Kolla att namn och vikt är ifyllt och att övrigt inte redan finns i listan.
        if (!empty($_SESSION['others_name'][$i]) && 
            !empty($_SESSION['others_weight'][$i]) && $in_db[$i] == 0) {
          // Hämta others_id från tabellen Others. Lägg till övrigt i Others om den inte finns där.
          $query = "SELECT others_id FROM Others WHERE  ".
                   "user_id = ".$_SESSION['user_id']." AND ".
                   "others_name = '".$_SESSION['others_name'][$i]."' AND ".
                   "others_stage_id = '".$_SESSION['others_stage_id_sel'][$i]."' AND ".
                   "deleted = 0";
          $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
          if (mysqli_num_rows($result) == 0) {
            $others_stage_id_sel = $_SESSION['others_stage_id_sel'][$i];
            if (empty($_SESSION['others_stage_id_sel'][$i])) {$others_stage_id_sel = 0;}
            $query = "INSERT INTO Others (user_id, others_name, others_stage_id) ".
                     "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['others_name'][$i]."', '".
                     $others_stage_id_sel."')";
            if (mysqli_query($dbc, $query)) {
                $others_id = mysqli_insert_id($dbc);
            } else { die("beer_reg.Others ".mysqli_error($dbc).$query); }
          } else {
            $row = mysqli_fetch_array($result);
            $others_id = $row['others_id'];
          }
          // Lägg till övrig ingrediens i Others_in_beer.
          $query = "INSERT INTO Others_in_beer (beer_id, others_id, others_weight, others_comment) ".
                   "VALUES ('".$_SESSION['beer_id']."', '".
                   $others_id."', '".$_SESSION['others_weight'][$i]."', '".
                   $_SESSION['others_comment'][$i]."')";
          if (!mysqli_query($dbc, $query)) {
            die("beer_reg.Others_in_beer ".mysqli_error($dbc).$query);
          }          
        }       
      }
      
      // Uppdatera Beers_in_event.
      if (!empty($_SESSION['dt_event_id'])) {
        ModBeersEvent($dbc, $_SESSION['dt_event_id'], $_SESSION['comp_dt']);
      }
      if (!empty($_SESSION['fv_event_id'])) {
        ModBeersEvent($dbc, $_SESSION['fv_event_id'], $_SESSION['comp_fv']);
      }
      if (!empty($_SESSION['et_event_id'])) {
        ModBeersEvent($dbc, $_SESSION['et_event_id'], $_SESSION['comp_et']);
      }
      
      // Gå till event_reg, men varna om bryggaren kan ha missat att kryssa för Folköl.
      if ( !$_SESSION['low_alc_sel'] && !($_SESSION['alc'] > 3.5) && $_SESSION['comp_fv'] && $_SESSION['low_alc_in_fv'] && !($_SESSION['fv_allow_user_votesys_category_selection'] == 1)){
        ReDirect ('warn_lowalc.php');        
      } else {
        ReDirect ('event_reg.php');
      }
    }
  }

  // Fyll i användarens namn i fältet bryggare om fältet är tomt.
  if (empty($_SESSION['brewers_name'][0])) {  
    $query = "SELECT name, email FROM User_data WHERE user_id = ".$_SESSION['user_id']." AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($result);
    $_SESSION['brewers_name'][0] = $row['name'];
    $_SESSION['brewers_email'][0] = $row['email'];
  }
  
  mysqli_close($dbc);
  
?>




<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  //hämta kategorier från ratingsystmets databas
  require_once('funct_ratingsys.php');
  if (($_SESSION['fv_votesys_competition_id'] > 0 &&  $_SESSION['fv_allow_user_votesys_category_selection'] == 1)){
      $rating_competitionId = intval($_SESSION['fv_votesys_competition_id']);
      $dbAccess = new DbAccess();
      $rating_categories = $dbAccess->getCategories($rating_competitionId, true);
  } else {
      $rating_categories = null;
  }

?>
  <pre> <?php echo $dbg_msg;?> </pre>
  <pre class="error"><?php echo $err_msg;?></pre>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <fieldset> <legend><?php if ($_SESSION['update']) {echo 'Ändra öl';} else {echo 'Lägg till öl';} ?></legend>
      <table>
        <tr>
          <td class=header>Tävling</td>
        </tr>
        <tr>
          <td>Markera vad du vill <br>anmäla ditt öl till.</td>
          <td>
          <?php
            if (!empty($_SESSION['dt_event_id'])) {
              echo '<input type="checkbox" name="comp_dt" value="1" '; 
              if ($_SESSION['comp_dt']) {echo 'checked ';} 
              if (!$_SESSION['dt_event_open']) {echo 'disabled ';}
              echo '> '.$_SESSION['dt_event_msg'].' <br>';
            } else {
              $_SESSION['comp_dt'] = 0;
            }
            //see event_reg.js för click-event som visar/döljer votesys_category
            if (!empty($_SESSION['fv_event_id'])) {
              echo '<input type="checkbox" name="comp_fv" value="1" '; 
              if ($_SESSION['comp_fv']) {echo 'checked ';} 
              if (!$_SESSION['fv_event_open']) {echo 'disabled ';}
              echo '> '.$_SESSION['fv_event_msg'].' <br>';
            } else {
              $_SESSION['comp_fv'] = 0;
            }
            if (!empty($_SESSION['et_event_id'])) {
              echo '<input type="checkbox" name="comp_et" value="1" '; 
              if ($_SESSION['comp_et']) {echo 'checked ';} 
              if (!$_SESSION['et_event_open']) {echo 'disabled ';}
              echo '> '.$_SESSION['et_event_msg'].' <br>';
            } else {
              $_SESSION['comp_et'] = 0;
            }
          ?>
          </td>
        </tr>      
      </table>

      <?php 
          if ($_SESSION['low_alc_in_fv'] == 1) {
            echo '<input type="checkbox" name="low_alc" value="1" ';
            if ($_SESSION['low_alc_sel']) {echo 'checked ';}
            if (!$_SESSION['fv_event_open']) {echo 'disabled ';}
            echo '> Ölet ska vara med i FV-klassen Folköl (< 3,5 %). <br>';
          }
      ?>

      <table>
        <tr>
          <td class=header>Öluppgifter</td>
        </tr>
        <tr>
          <td class=col_1>Öltyp:</td>
          <td>
            <select name="beer_type" <?php if ($_SESSION['recipe_chg_blocked_dt'] || $_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> >
              <option <?php if ($_SESSION['beer_type'] == ""){echo 'selected ';} ?> value=""></option>
              <?php 
              $i = 0;
              foreach ($_SESSION['type_names'] as $type_name) {
                echo '<option ';
                if ($_SESSION['type_values'][$i] == $_SESSION['beer_type']) {
                  echo 'selected ';
                }
                echo 'value="';
                echo $_SESSION['type_values'][$i];
                echo '">';
                echo $type_name;
                echo '</option> ';
                $i++;
              } ?>
            </select>
          </td>
        </tr>
        
        <?php if ($rating_categories != null) { ?>
          <tr>
            <td class=col_1>Tävlingsklass Folkets Val:</td>
            <td>
              <select name="votesys_category" <?php if (!($_SESSION['comp_et'] || $_SESSION['comp_fv'])){echo ' disabled ';} ?> >
                <option <?php if ($_SESSION['votesys_category'] == ""){echo 'selected ';} ?> value=""></option>
                <?php 
                foreach ($rating_categories as $category) {
                  echo '<option ';
                  if ($category['id'] == $_SESSION['votesys_category']) {
                    echo 'selected ';
                  }
                  echo 'value="';
                  echo $category['id'];
                  echo '">';
                  echo $category['description'];
                  echo '</option> ';
                } ?>
              </select>
            </td>
          </tr>
        <?php } ?>
        <tr>
          <td class=col_1>Ölets namn:</td>
          <td>
            <input type="text" name="beer_name" id="beer_name" value="<?php echo $_SESSION['beer_name'] ?>" maxlength="40" <?php if ($_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> />
          </td>
        </tr>
        <tr>
          <td class=col_1>OG [g/l]:</td>
          <td>
            <input type="number" name="og" id="og" min=0 max=10000 step=1 value="<?php if (isset($_SESSION['og'])) echo $_SESSION['og']+1000; else echo NULL ?>" <?php if ($_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> />
          </td>
        </tr>
        <tr>
          <td class=col_1>FG [g/l]:</td>
          <td>
            <input type="number" name="fg" id="fg" min=0 max=10000 step=1 value="<?php if (isset($_SESSION['fg'])) echo $_SESSION['fg']+1000; else echo NULL ?>" <?php if ($_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> />
          </td>
        </tr>
        <tr>
          <td class=col_1>Beska [BU]:</td>
          <td>
            <input type="number" name="bu" id="bu" min=0 value="<?php echo $_SESSION['bu'] ?>" <?php if ($_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> />
          </td>
        </tr>
        <tr>
          <td class=col_1>Alkoholhalt [vol%]:</td>
          <td>
            <input type="number" name="alc" id="alc" min=0 step=0.1 value="<?php echo $_SESSION['alc'] ?>" <?php if ($_SESSION['recipe_chg_blocked_fv'] || $_SESSION['recipe_chg_blocked_et']){echo ' disabled ';} ?> />
          </td>
        </tr>
        <tr>
          <td class=col_1>Volym [l]:</td>
          <td>
            <input type="number" name="volume" id="volume" min=0 value="<?php echo $_SESSION['volume'] ?>" /> Efter kok.
          </td>
        </tr>
      </table>

      <table>
        <tr>
          <td class=header>Bryggare</td>
        </tr>
        <tr>
          <td class=col_2>Namn:</td>
          <td class=col_2>e-post:</td>
        </tr>
        <?php for ($i=0; $i<$_SESSION['no_brewers']; $i++) {
          echo '<tr>';
            echo '<td>';
              echo '<input type="text" name="brewers_name[]" id="brewers_name[] " value="' . $_SESSION['brewers_name'][$i] . '" maxlength="50" ';
              if ( ($_SESSION['autofocus_sel'] == "brewers") && ($i == ($_SESSION['no_brewers']-1)) ) {echo 'autofocus';} 
              echo '/>';
            echo '</td>';
            echo '<td>';
              echo '<input type="text" name="brewers_email[]" id="brewers_email[] " value="' . $_SESSION['brewers_email'][$i] . '" maxlength="50" />';
            echo '</td>';
            echo ' <td> <input type="submit" value="Ta bort" name="del_brewer['.$i.']" /> </td> ';
          echo '</tr>';
        } ?>
        <tr>
          <td> <input type="submit" value="Lägg till rad" name="add_brewer" /> </td>
          <td></td>
          <td></td>
        </tr>
      </table>

      <table>
        <tr>
          <td class=header>Kok</td>
        </tr>
        <tr>
          <td class=col_2>Humle / krydda</td>
          <td class=col_2>Form</td>
          <td class=col_2>Alfasyra[%]</td>
          <td class=col_2>Mängd[g]</td>
          <td class=col_2>Koktid[min]</td>
          <td class=col_2>Kommentar</td>
        </tr>
        <?php for ($i=0; $i<$_SESSION['no_hops']; $i++) {
          echo '<tr>';
            echo '<td>';
              echo '<input type="text" name="hops_name[]" id="hops_name[] " value="' . $_SESSION['hops_name'][$i] . '" maxlength="50" ';
              if ( ($_SESSION['autofocus_sel'] == "hops") && ($i == ($_SESSION['no_hops']-1)) ) {echo 'autofocus';} 
              echo '/>';
            echo '</td>';
            echo '<td>';
              echo '<select name="hops_form_id_sel[]" >';
                echo '<option '; if($_SESSION['hops_form_id_sel'][$i] == 0){echo("selected");}; echo ' value=""></option>';
                $j = 0;
                foreach ($_SESSION['hops_form_name'] as $hops_form_name) {
                  echo '<option ';
                  if ($_SESSION['hops_form_id'][$j] == $_SESSION['hops_form_id_sel'][$i]) {
                    echo 'selected ';
                  }
                  echo 'value="';
                  echo $_SESSION['hops_form_id'][$j];
                  echo '">';
                  echo $hops_form_name;
                  echo '</option> ';
                  $j++;
                }
              echo '</select>';
            echo '</td>';
            echo '<td>';
              echo '<input type="number" name="hops_alpha[]" id="hops_alpha[] " min=0 step=0.1 value="' . $_SESSION['hops_alpha'][$i] . '" />';
            echo '</td>';
            echo '<td>';
              echo '<input type="number" name="hops_weight[]" id="hops_weight[] " min=0 value="' . $_SESSION['hops_weight'][$i] . '" />';
            echo '</td>';
            echo '<td>';
              echo '<input type="number" name="hops_boil_time[]" id="hops_boil_time[] " min=0 value="' . $_SESSION['hops_boil_time'][$i] . '" />';
            echo '</td>';
            echo '<td>';
              echo '<input type="text" name="hops_comment[]" id="hops_comment[] " value="' . $_SESSION['hops_comment'][$i] . '" maxlength="50" />';
            echo '</td>';
            echo ' <td> <input type="submit" name="del_hops['.$i.']" value="Ta bort" /> </td> '; 
          echo '</tr>';
        } ?>
        <tr>
          <td> <input type="submit" value="Lägg till rad" name="add_hops" /> </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>

      <table>
        <tr>
          <td class=header>Mäsk</td>
        </tr>
        <tr>
          <td class=col_2>Malt / extraktgivare:</td>
          <td class=col_2>Mängd[g]:</td>
          <td class=col_2>Kommentar:</td>
        </tr>
        <?php for ($i=0; $i<$_SESSION['no_malts']; $i++) {
          echo '<tr>';
            echo '<td>';
              echo '<input type="text" name="malts_name[]" id="malts_name[] " value="' . $_SESSION['malts_name'][$i] . '" maxlength="50" ';
              if ( ($_SESSION['autofocus_sel'] == "malts") && ($i == ($_SESSION['no_malts']-1)) ) {echo 'autofocus';} 
              echo '/>';
            echo '</td>';
            echo '<td>';
              echo '<input type="number" name="malts_weight[]" id="malts_weight[] " min=0 value="' . $_SESSION['malts_weight'][$i] . '" />';
            echo '</td>';
            echo '<td>';
              echo '<input type="text" name="malts_comment[]" id="malts_comment[] " value="' . $_SESSION['malts_comment'][$i] . '" maxlength="50" />';
            echo '</td>';
            echo ' <td> <input type="submit" name="del_malt['.$i.']" value="Ta bort" /> </td> '; 
          echo '</tr>';
        } ?>
        <tr>
          <td> <input type="submit" value="Lägg till rad" name="add_malt" /> </td>
          <td></td>
          <td></td>
        </tr>
      </table>

      <table>
        <tr>
          <td class=header>Mäskschema</td>
        </tr>
        <tr>
          <td> <textarea name="mashing" rows="5" cols="40" maxlength="1000"><?php echo $_SESSION['mashing'];?></textarea> </td>
        </tr>
      </table>
        
      <table>
        <tr>
          <td class=header>Övriga ingredienser</td>
        </tr>
        <tr>
          <td class=col_2>Ingrediens</td>
          <td class=col_2>Tillsatt vid</td>
          <td class=col_2>Mängd[g]</td>
          <td class=col_2>Kommentar</td>
        </tr>

        <?php for ($i=0; $i<$_SESSION['no_others']; $i++) {
          echo '<tr>';
            echo '<td>';
              echo '<input type="text" name="others_name[]" id="others_name[] " value="' . $_SESSION['others_name'][$i] . '" maxlength="50" ';
              if ( ($_SESSION['autofocus_sel'] == "others") && ($i == ($_SESSION['no_others']-1)) ) {echo 'autofocus';} 
              echo '/>';
            echo '</td>';
            echo '<td>';
              echo '<select name="others_stage_id_sel[]" >';
                echo '<option '; if($_SESSION['others_stage_id_sel'][$i] == 0){echo("selected");}; echo ' value=""></option>';
                $j = 0;
                foreach ($_SESSION['others_stage_name'] as $others_stage_name) {
                  echo '<option ';
                  if ($_SESSION['others_stage_id'][$j] == $_SESSION['others_stage_id_sel'][$i]) {
                    echo 'selected ';
                  }
                  echo 'value="';
                  echo $_SESSION['others_stage_id'][$j];
                  echo '">';
                  echo $others_stage_name;
                  echo '</option> ';
                  $j++;
                }
              echo '</select>';
            echo '</td>';
            echo '<td>';
              echo '<input type="number" name="others_weight[]" id="others_weight[] " min=0 value="' . $_SESSION['others_weight'][$i] . '" />';
            echo '</td>';
            echo '<td>';
              echo '<input type="text" name="others_comment[]" id="others_comment[] " value="' . $_SESSION['others_comment'][$i] . '" maxlength="50" />';
            echo '</td>';
            echo ' <td> <input type="submit" name="del_others['.$i.']" value="Ta bort" /> </td> '; 
          echo '</tr>';
        } ?>
        <tr>
          <td> <input type="submit" value="Lägg till rad" name="add_others" /> </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>

      <table>
        <tr>
          <td class=header>Jäsning</td>
        </tr>
        <tr>
          <td> <textarea name="ferment" rows="5" cols="40" maxlength="1000"><?php echo $_SESSION['ferment'];?></textarea> </td>
        </tr>
      </table>
        
      <table>
        <tr>
          <td class=header>Vatten</td>
        </tr>
        <tr>
          <td> <textarea name="water" rows="5" cols="40" maxlength="1000"><?php echo $_SESSION['water'];?></textarea> </td>
        </tr>
      </table>
        
      <table>
        <tr>
          <td class=header>Övriga kommentarer</td>
        </tr>
        <tr>
          <td> <textarea name="comment" rows="5" cols="40" maxlength="1000"><?php echo $_SESSION['comment'];?></textarea> </td>
        </tr>
      </table>
        
      <table>
        <tr>
          <td></td>
          <td>
            <input type="submit" value="Spara" name="add_beer" />
          </td>
        </tr>
      </table>
    </fieldset>
  </form>

<?php

  // Sidfot.
  require_once('footer.php');
?>


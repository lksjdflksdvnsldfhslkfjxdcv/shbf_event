<?php
// Skriver ut en lista på alla mottagna och godkända öl till domartävlingen.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("dt_list.php: "."Connection failed: " . mysqli_connect_error());
  }

  // Hämta ölerna.
  if (empty($_SESSION['dt_event_id'])) {
    die("dt_list_rec.php: No dt_event id defined.");
  }
  $query = "SELECT Beers.user_id, Beer_data.main_class, Beer_data.sub_class, Beers_in_event.label_no, ".
           "Dt_event_reg.received, Dt_event_reg.disq, Beer_data.comment FROM Beers ".
           "INNER JOIN Beer_data USING (beer_id) ".
           "INNER JOIN Beers_in_event USING (beer_id) ".
           "INNER JOIN Dt_event_reg USING (beer_id) ".
           "WHERE Beers_in_event.event_id = ".$_SESSION['dt_event_id']." ".
           "AND Dt_event_reg.event_id = ".$_SESSION['dt_event_id']." ".
           "AND Dt_event_reg.received = 1 AND Dt_event_reg.disq = 0 ".
           "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
           "AND Dt_event_reg.deleted = 0 AND Beers.deleted = 0 ".
           "ORDER BY Beer_data.main_class, Beer_data.sub_class, Beers_in_event.label_no ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $line = 0;
  while ($row = mysqli_fetch_array($result)) {
    $_SESSION['dt_beer_id'][$line] = $row['beer_id'];
    $user_id[$line] = $row['user_id'];
    $beer_name[$line] = $row['beer_name'];
    $type_id[$line] = $row['main_class'].":".$row['sub_class'];
    $label_no[$line] = $row['label_no'];
    $beer_comment[$line] = $row['comment'];
    // Översätt type_id till text.
    $type_name[$line] = "";
    for ($i=0; $i < count($_SESSION['type_values']); $i++) {
      if ($_SESSION['type_values'][$i] == $type_id[$line]) {
        $type_name[$line] = $_SESSION['type_names'][$i];
      }
    }
    $line++;
  }
  $_SESSION['no_dt_beers'] = $line;
  mysqli_close($dbc);



  // Sidhuvud.
  $page_title = 'Mottagna öl till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  echo '<table> ';
  echo '<tr> ';
  echo '<td class=header> Öltyp </td> ';
  echo '<td class=header> Bryggarens kommentar </td> ';
  echo '<td class=header> Flasknr </td> ';
  echo '<td class=header> ID </td> ';
  echo '</tr>';

  for ($i=0; $i<$_SESSION['no_dt_beers']; $i++) {
    // Skapa HTML-kod för listan.
    echo '<tr> ';
    echo '<td> '.$type_name[$i]. ' </td> ';
    echo '<td> '.$beer_comment[$i]. ' </td> ';
    echo '<td> '.$type_id[$i].'-'.$label_no[$i]. ' </td> ';
    echo '<td> '.$user_id[$i]. ' </td> ';
    echo '</tr>';
  }
  echo '</table>';
  echo '</form> ';
?>

<?php
  // Sidfot.
  require_once('footer.php');
?>

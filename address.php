<?php


// Skapar pdf med adresser med fpdf.
function address($event_id, $filter_main) {
  require_once('fpdf.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  // Skapa pdf-sida.
  $pdf = new FPDF('P','mm','A4');
  $pdf->AddPage();
  $pdf->SetFont('Courier','B',12);

  $border = 0;
  $cell_width = 95;
  $line_height = 5;
  $cell_hight = 40;
  $start_xpos = 10;
  $start_ypos = 10;
  $xpos = $start_xpos;
  $ypos = $start_ypos;

  $query = "SELECT User_data.name, User_data.care_of, User_data.street, User_data.post_no, ".
           "User_data.city, Beers.user_id FROM Beers_in_event ".
           "INNER JOIN Beers USING (beer_id) INNER JOIN Beer_data USING (beer_id) ".
           "INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$event_id." AND Beers_in_event.deleted = 0 ".
           "AND Users.deleted = 0 AND User_data.deleted = 0 ".
           "AND Beer_data.deleted = 0 AND Beers.deleted = 0 ".
           $filter_main." ".
           "ORDER BY Beers.user_id ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $prev_user_id = "";
  while ($row = mysqli_fetch_array($result)) {
    $name = $row['name'];
    $care_of = $row['care_of'];
    $street = $row['street'];
    $post_no = $row['post_no'];
    $city = $row['city'];
    $user_id = $row['user_id'];

    $text = $user_id.chr(10);
    $text = $text.$name.chr(10);
    if ($user_id != $prev_user_id) {
      $prev_user_id = $user_id;
      if (!empty ($care_of) ) {
        $text = $text."c/o ".$care_of.chr(10);
      }
      $text = $text.$street.chr(10);
      $text = $text.$post_no." ".$city.chr(10);

      if ($ypos > $cell_hight * 7) {
        $xpos = $xpos + $cell_width;
        $ypos = $start_ypos;
      }
      if ($xpos > $cell_width * 2) {
        $pdf->AddPage();
        $xpos = $start_xpos;
        $ypos = $start_ypos;
      }
      $pdf->SetXY($xpos, $ypos);
      $pdf->MultiCell($cell_width,$line_height,utf8_decode($text), $border, 'L', 0);
      $ypos = $ypos + $cell_hight;
    }
  }
  ob_start();
  $pdf->Output();
  ob_end_flush();
}

  // Skapar pdf med adressetiketter till deltagare i domartävlingen.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sidhuvud.
  $page_title = 'Adressetiketter';

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Hämta filtrering på huvudklass från Get.
  if (isset($_GET['filter_main'])) {
    $text = $_GET['filter_main'];
    $filter_main = mb_strimwidth (filter_var(trim($_GET['filter_main']),FILTER_SANITIZE_SPECIAL_CHARS),0,100);
    if ($text != "Alla") {
      if ( preg_match_all('/(\d+)/', $filter_main, $matches) ) {
        $filter_main = "AND (Beer_data.main_class=".$matches[1][0];
        $classes = $matches[1][0];
        for ($i=1; $i<sizeof($matches[1]); $i++) {
          $filter_main = $filter_main." OR Beer_data.main_class=".$matches[1][$i];
          $classes = $classes.", ".$matches[1][$i];
        };
        $filter_main = $filter_main.") ";
      } else {
        $filter_main = "";
      }
    } else {
      $filter_main = "";
    }
  }

  // Skapa adressetiketter till deltagare i domartävlingen.
  address($_SESSION['dt_event_id'], $filter_main);

?>


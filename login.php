<?php
  

  // Starta session.
  require_once('startsession.php');
  require_once('const.php');
  require_once('funct.php');
  require_once("Mail.php");

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Rensa felmeddelande
  $error_msg = "";

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  // Sätt startvärden.
  $msg = "";
  $_SESSION['adm_lev'] = 'NONE';
  $new_passwd_disabled ="";
  $passwd_msg = "Om du glömt ditt lösenord så fyll i din e-postadress och tryck på Glömt lösen.";

  // Logga in om user_id inte är satt.
  if (!isset($_SESSION['user_id'])) {
    if (isset($_POST['submit'])) {
      // Hämta POST-data.
      $user_name = FilterPost ($dbc, $_POST['user_name'], 50);
      $user_passwd = FilterPost ($dbc, $_POST['passwd'], 1000);

      if (!empty($user_name) && !empty($user_passwd)) {
        // Kontrollera användarnam och lösen mot databasen.
        $query = "SELECT Users.user_id, Users.user_name, User_data.name, User_data.adm_lev, User_data.passwd, User_data.code ".
                 "FROM Users INNER JOIN User_data USING (user_id) ".
                 "WHERE user_name = '$user_name' AND passwd = SHA('$user_passwd') AND Users.deleted = 0 AND User_data.deleted = 0";
        $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
        $row = mysqli_fetch_array($data);
        $code = isset($row['code']) ? $row['code'] : '';

        if ( (mysqli_num_rows($data) == 1) && ($code == 0) ) {
          // Inloggning ok. Gå till registreringssidan.
          // Egentligen vill jag gå tillbaka till föregående sida,
          // men det funkade inte.
          $_SESSION['user_id'] = $row['user_id'];
          $_SESSION['user_name'] = $row['user_name'];
          $_SESSION['name'] = $row['name'];
          $_SESSION['adm_lev'] = $row['adm_lev'];
          ReDirect ('beer_reg_pre.php');
          // Två försök att gå tillbaka till föregående sida.
          //header("Location: {$_SERVER['HTTP_REFERER']}");
          //header("location:javascript://history.go(-1)");
        }
        else {
          // Fel användarnamn eller lösen.
          $error_msg = $error_msg.' Inloggningen misslyckades';
        }
      }
      else {
        // Användarnamn eller lösen skrevs inte in.
        $error_msg = $error_msg.' Du måste ange användarnamn och lösenord.';
      }
    }
    if (isset($_POST['new_passwd'])) {
      $ok = 1;

      // Hämta e-postadress från POST
      $email = FilterPost ($dbc, $_POST['email'], 50);
      if (empty($email)) {
        $ok = 0;
      }

      // Hämta user_id från databasen.
      $query = "SELECT user_id FROM User_data WHERE email = '".$email."' AND code = 0 AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      if (mysqli_num_rows($result) == 0) {
        $ok = 0;
      }
      if ($ok) {
        $row = mysqli_fetch_array($result);
        $user_id = $row['user_id'];
        // Skapa nytt lösenord.
        $passwd = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz"), 0, 8);
        $new_passwd_disabled = "disabled"; // För att hindra användaren från att trycka fler gånger på Glömt lösen.
        // Hämta uppgifter från Users.
        $query = "SELECT * FROM User_data WHERE user_id = ".$user_id." AND code = 0 AND deleted = 0";
        $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
        if (mysqli_num_rows($result) == 0) {
          die("login.php: No data found in database.");
        }
        $row = mysqli_fetch_array($result);
        $name = $row['name'];
        $member_no = $row['member_no'];
        $email = $row['email'];
        $care_of = $row['care_of'];
        $street = $row['street'];
        $post_no = $row['post_no'];
        $city = $row['city'];
        $adm_lev = $row['adm_lev'];
        // Markera nuvarande rad i User_data som raderad.
        $query = "UPDATE User_data SET deleted = 1 ".
                 "WHERE user_id = ".$user_id." AND ".
                 "deleted = 0";
        if (!mysqli_query($dbc, $query)) {
          die("update.php: ".mysqli_error($dbc).$query);
        }
        // Lägg in lösenordet i User_data.
        $query = "INSERT INTO User_data (user_id, name, member_no, email, care_of, street, post_no, city, adm_lev, passwd, code) ".
                 "VALUES ('".$user_id."', '".$name."', '".$member_no."', '".$email."', '".$care_of."', '".
                 $street."', '".$post_no."', '".$city."', '$adm_lev', SHA('".$passwd."'), '0')";
        if (!mysqli_query($dbc, $query)) {
          die("login.php: ".mysqli_error($dbc).$query);
        }
        $msg = $msg."Ett nytt lösenord har skickats till dig. ";
        // Hämta användarnamn från Users.
        $query = "SELECT user_name FROM Users WHERE user_id = ".$user_id." AND deleted = 0";
        $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
        if (mysqli_num_rows($result) == 0) {
          die("login.php: No data found in database.");
        }
        $row = mysqli_fetch_array($result);
        $user_name = $row['user_name'];
        // Skicka lösenord och användarnamn till användaren.
        $to = $email;
        $subject = "SHBF registrering";
        $mail_msg = "Din nya inloggning är: \n";
        $mail_msg = $mail_msg."Användarnamn: ".$user_name." \n";
        $mail_msg = $mail_msg."Lösenord: ".$passwd." \n";
        SendMail($to, $subject, $mail_msg);
        mysqli_close($dbc);
        $passwd_msg = "Nytt lösenord har skickats till dig.";
      }
      if (!$ok) {
        $passwd_msg = "e-postadressen kunde inte hittas.";
      }
    }
  }

?>



<?php
  // Sidhuvud.
  $page_title = 'Inloggning';
  require_once('header_nav.php');

  // Skriv ut ev. felmeddelande.
  echo '<p class="error">' . $error_msg . '</p>';

  // Visa formuläret om user_id inte är satt.
  if (!isset($_SESSION['user_id'])) {
?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
      <fieldset>
        <legend>Logga in</legend>
        <label for="user_name">Användarnamn:</label>
        <input type="text" name="user_name" value="<?php if (!empty($user_name)) echo $user_name; ?>" maxlength="50" autofocus /><br />
        <label for="passwd">Lösenord:</label>
        <input type="password" name="passwd" maxlength="1000" /><br />
        <input type="submit" value="Logga in" name="submit" /><br />
      </fieldset>
      <fieldset>
      <p> <?php echo $passwd_msg ?> </p>
        <legend>Glömt lösen</legend>
        <label for="email">e-post:</label>
        <input type="text" id="email" name="email" value="<?php if (!empty($email)) echo $email; ?>" maxlength="50" /><br />
        <input type="submit" value="Glömt lösen" name="new_passwd" <?php echo $new_passwd_disabled; ?> />
      </fieldset>
    </form>
<?php
  }
  else {
    // Annars, bekräfta inloggningen.
    echo('<p class="login">Du är redan inloggad som ' . $_SESSION['name'] . '.</p>');
  }

  // Sidfot
  require_once('footer.php');
?>

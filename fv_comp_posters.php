<?php
//skapar pdf med nummerskyltar till FV

// Starta session.
require_once('startsession.php');
// Inkludera konstanter och funktioner.
require_once('const.php');
require_once('funct.php');

// Kontrollera behörighet.
AccessChk(basename(__FILE__, ".php"));

$event_id = $_SESSION['fv_event_id'];
//if url param et_event_id, override
if (isset($_GET['event_id']))
    $event_id = $_GET['event_id'];


// Sidhuvud.
$page_title = 'Nummerskyltar';

// Skapa nummerskyltar för aktuell user_id och tävling., eller för alla users om admin.

if ($_SESSION['adm_lev'] == 'FULL') {
    comp_posters($event_id, null /*all users*/ );

} else if (isset($_SESSION['user_id'])){
    //för vanliga users enbart vid stängd tävlingsregistrering
    if ($_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE)
        comp_posters($event_id,  $_SESSION['user_id']);
    else
        die('Tävlingsregistreringen är inte stängd än.');
}


function comp_posters($event_id, $user_id = null)
{
    require_once('fpdf.php');
    
    // Skapa pdf med nummerskyltar till FV.

    $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if (!$dbc) {
        die("Connection failed: " . mysqli_connect_error());
    }

    //get my host for qr-code
    $qrhost = $_SERVER['HTTP_HOST'];
    // //get path from $_Server[PHP_SELF]
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    //if my host is https
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
        $qrhost = 'https://' . $qrhost;
    } else {
        $qrhost = 'http://' . $qrhost;
    }
    $qrhost = $qrhost . $uri;    
    $votesys_competition_id = 0;
    //kontrolelra att votsys_competition_id är satt, annars inga nummerskyltar
    $query = "SELECT votesys_competition_id FROM Events WHERE event_id = " . $event_id . " AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die(mysqli_error($dbc));
    $row = mysqli_fetch_array($data);
    if (empty($row['votesys_competition_id'])) {
        die("event_id = " . $event_id . " saknar koppling till tävlings-id't i röstningssystem, <b>fyll i Events.votesys_competition_id </b> och försök igen");
    }
    else
        $votesys_competition_id = $row['votesys_competition_id'];



    // Skapa pdf
    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->SetTopMargin(10);
    $pdf->SetLeftMargin(10);
    $pdf->SetRightMargin(10);    
  
      //hämta kategorier från ratingsystmets databas
    require_once('funct_ratingsys.php');
    if (($_SESSION['fv_votesys_competition_id'] > 0 &&  $_SESSION['fv_allow_user_votesys_category_selection'] == 1)){
        $rating_competitionId = intval($_SESSION['fv_votesys_competition_id']);
        $dbAccess = new DbAccess();
        $rating_categories = $dbAccess->getCategories($rating_competitionId, true);
    } else {
      $rating_categories = null;
    }


    
    //hämta öl som är registrerade för user_id och i aktuell tävlingsklass, eller alla om user är admin
    //DISTINCT pga php 5.6
    $query = "SELECT DISTINCT Beers.beer_id, Beers.user_id, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class, Beer_data.votesys_category,Beer_data.type_name, \n"
    . "           Beer_data.low_alc, Beer_data.og, Beer_data.fg, Beer_data.bu, Beer_data.alc,User_data.name,User_data.user_id,Beers_in_event.fv_competition_no,fer.next_to_id ,ferud.name as nextToName FROM Beers\n"
    . "           INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id)\n"
    . "           INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) \n"
    . "           LEFT JOIN Fv_event_reg fer \n"
    . "           				inner join User_data ferud on ferud.User_id=fer.Next_to_id   and ferud.deleted=0 \n"
    . "           			ON fer.User_id = Beers.User_id and fer.Fv_event_id=" . $event_id . " and fer.Next_to_id > 0\n"
    . "           \n"
    . "           \n"
    . ($user_id !== null ? (" WHERE Beers.user_id = " . $user_id . " AND ") : ' WHERE ' )  
    .              "Beers.deleted = 0 " 
    .              "AND Beers_in_event.event_id = " . $event_id . " AND Beers_in_event.deleted = 0 " 
    .              "AND Beer_data.deleted = 0 " 
    . "           AND Users.deleted = 0 AND User_data.deleted = 0  \n"
    . "ORDER BY CASE \n"
    . "           WHEN `nextToName` IS NOT NULL THEN `nextToName` ELSE User_data.name \n"
    . "           END ASC";        
    mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
    $result = mysqli_query($dbc, $query) or die(mysqli_error($dbc));
    $beer_no = 0;
    while ($row = mysqli_fetch_array($result)) {
        $beer_name = $row['beer_name'];
        $beer_type = $row['type_name'];
        
        $fv_competition_no = $row['fv_competition_no'];
        if ($fv_competition_no == '') { //ej skapat tävlingsnummer än
            $fv_competition_no = '---';
        }
        $main_class = $row['main_class'];
        $sub_class = $row['sub_class'];        
        $beer_fg = $row['fg'];
        $beer_og = $row['og'];
        $beer_bu = $row['bu'];
        $beer_alc = $row['alc'];
        
        if (!empty($row['low_alc'])) {
            $low_alc = "F";
          }
          else {
            $low_alc = "";
          }
        $fv_classnumber = mb_convert_encoding(FvClass($low_alc, $main_class, $sub_class,true), 'ISO-8859-1', 'UTF-8');
        $fv_classname = mb_convert_encoding(FvClass($low_alc, $main_class, $sub_class), 'ISO-8859-1', 'UTF-8');
        $beer_type = mb_convert_encoding($beer_type, 'ISO-8859-1', 'UTF-8');

        

        $pdf->AddPage();
            

        /*Klass */
        $pdf->SetXY(10, 17);
        $pdf->SetFont('Times', 'B', 60);
        $beer_fv_cat = '';
        if ($rating_categories != null) {
            //hämta kategorinamn från ratingsystemet
            $votesys_category = $row['votesys_category'];
            foreach ($rating_categories as $category) {
                if ($category['id'] == $votesys_category) {
                    $beer_fv_cat = mb_convert_encoding($category['name'], 'ISO-8859-1', 'UTF-8');
                    break;
                }
            }
            
         }
        if ($beer_fv_cat != '')        
            $pdf->Cell(0, 10,   $beer_fv_cat, 0, 1, 'C', false);
         else        
            $pdf->Cell(0, 10, 'Klass ' . $fv_classnumber, 0, 1, 'C', false);    

        /*Klassnamn*/
        $pdf->SetXY(10, 40);
        $pdf->SetFontSize(40);
        $pdf->Cell(0, 10, $fv_classname, 0, 1, 'C', false);
        $qrfile = $qrhost . '/fv_qrgen.php?votesys_competition_id=' . $votesys_competition_id . '&fv_beer_id=' . $fv_competition_no;
        
        //generate & insert QR code
        //fungerar INTE i xdebug! (file stream error), släng upp dummy text istället för Imgage i debug mode
        if ((function_exists("xdebug_is_debugger_active") && xdebug_is_debugger_active())) {
            // $pdf->SetXY(0, 71);
            // $pdf->Image($qrfile, 20, 60, 100, 100, "png");        
            $pdf->SetXY(10, 100);
            $pdf->SetFontSize(32);
            $pdf->Cell(0, 10, 'QR code', 0, 1, 'L', false);            
            
        }
        else { 
            $pdf->SetXY(0, 71);
            $pdf->Image($qrfile, 20, 60, 100, 100, "png");        
        }        
        

        /*FV-NR */
        $pdf->SetXY(120, 78);
        $pdf->SetFontSize(282);
        $pdf->Cell(0, 71, $fv_competition_no, 0, 1, 'L', false);
        //die('-'.$fv_competition_no.'-');

        /*Beerstyle*/
        $pdf->SetXY(10, 158);
        $pdf->SetFontSize(32);
        $pdf->Cell(0, 14, $beer_type, 0, 1, 'L', false);
        /*BeerData*/
        $pdf->SetXY(28, 173);
        $pdf->Cell(0, 14, $beer_alc .' %   '.$beer_bu. ' BU' , 0, 1, 'R', false);            

    }


    ob_start();
    $pdf->Output();
    ob_end_flush();
}


?>
<?php
// Skriver ut en lista på alla anmälda till domartävlingen.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("dt_list.php: "."Connection failed: " . mysqli_connect_error());
  }

  // Adress-etiketter.
  if (isset($_POST['addr'])) {
    $addr_filter_main = FilterPost ($dbc, $_POST['addr_filter_main'], 100);
    ReDirect ('address.php?filter_main='.$addr_filter_main);
  }

  // ID-etiketter.
  if (isset($_POST['id'])) {
    $id_filter_main = FilterPost ($dbc, $_POST['id_filter_main'], 100);
    ReDirect ('id.php?filter_main='.$id_filter_main);
  }

  // Mottagna öl.
  if (isset($_POST['dt_rec'])) {
    ReDirect ('dt_list_rec.php');
  }

  // Spara data till Dt_event_reg.
  if (isset($_POST['save'])) {
    // Hämta värden från POST.
    for ($i=0; $i<$_SESSION['no_dt_beers']; $i++) {
      if (isset($_POST['dt_received'][$i]) && $_POST['dt_received'][$i] == '1') {
        $dt_received[$i] = 1;
      } else {
        $dt_received[$i] = 0;
      }
      if (isset($_POST['dt_disq'][$i]) && $_POST['dt_disq'][$i] == '1') {
        $dt_disq[$i] = 1;
      } else {
        $dt_disq[$i] = 0;
      }
      $dt_comment[$i] = FilterPost ($dbc, $_POST['dt_comment'][$i], 200);
      $price[$i] = FilterPost ($dbc, $_POST['price'][$i], 20);
    }
    for ($i=0; $i<$_SESSION['no_dt_beers']; $i++) {
      // Kontrollera om något data ändrats
      // Hämta received, disq och comment från Dt_event_reg.
      $query = "SELECT received, disq, comment FROM Dt_event_reg ".
                "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
                "AND event_id = ".$_SESSION['dt_event_id']." ".
                "AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      $addDt = 0;
      $delDt = 0;
      if (mysqli_num_rows($result) == 0) {
        $addDt = 1;
      }
      if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        if ($dt_received[$i] != $row['received'] ||
            $dt_disq[$i] != $row['disq'] ||
            $dt_comment[$i] != $row['comment']) {
          $addDt = 1;
          $delDt = 1;
        }
      }
      if (mysqli_num_rows($result) > 1) {
        die ("dt_list.php: No data or too many data found in Dt_event_reg.");
      }
      if ($delDt) {
        // Markera nuvarande rad i Dt_event_reg som raderad.
        $query = "UPDATE Dt_event_reg SET deleted = 1 ".
                 "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
                 "AND event_id = ".$_SESSION['dt_event_id']." ".
                 "AND deleted = 0";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
      if ($addDt) {
        // Lägg till ny rad i Dt_event_reg.
        $query = "INSERT INTO Dt_event_reg (beer_id, event_id, received, disq, comment) ".
                 "VALUES ('".$_SESSION['dt_beer_id'][$i]."', '".$_SESSION['dt_event_id']."', '".$dt_received[$i].
                 "', '".$dt_disq[$i]."', '".$dt_comment[$i]."')";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
      // Hämta price från Prices.
      $query = "SELECT price FROM Prices ".
               "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
               "AND event_id = ".$_SESSION['dt_event_id']." ".
               "AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      $addPr = 0;
      $delPr = 0;
      if (mysqli_num_rows($result) == 0) {
        $addPr = 1;
      }
      if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        if ($price[$i] != $row['price']) {
          $addPr = 1;
          $delPr = 1;
        }
      }
      if (mysqli_num_rows($result) > 1) {
        die ("dt_list.php: No data or too many data found in Prices.");
      }
      if ($delPr) {
        // Markera nuvarande rad i Prices som raderad.
        $query = "UPDATE Prices SET deleted = 1 ".
                 "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
                 "AND event_id = ".$_SESSION['dt_event_id']." ".
                 "AND deleted = 0";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
      if ($addPr) {
        // Lägg till ny rad i Prices.
        $query = "INSERT INTO Prices (event_id, beer_id, price) ".
                 "VALUES ('".$_SESSION['dt_event_id']."', '".$_SESSION['dt_beer_id'][$i]."', '".$price[$i]."')";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
    }
  }



  // Hämta ölerna.
  if (empty($_SESSION['dt_event_id'])) {
    die("dt_list.php: No dt_event id defined.");
  }
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $query = "SELECT Beers.beer_id, Beers.user_id, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class, ".
           "Beers_in_event.label_no, Beer_data.og, Beer_data.fg, Beer_data.bu, Beer_data.alc, Beer_data.comment FROM Beers ".
           "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
           "INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$_SESSION['dt_event_id']." ".
           "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
           "AND Users.deleted = 0 AND User_data.deleted = 0 ".
           "AND Beers.deleted = 0 ".
           "ORDER BY Beer_data.main_class, Beer_data.sub_class, Beers_in_event.label_no ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $line = 0;
  while ($row = mysqli_fetch_array($result)) {
    $_SESSION['dt_beer_id'][$line] = $row['beer_id'];
    $user_id[$line] = $row['user_id'];
    $beer_name[$line] = $row['beer_name'];
    $type_id[$line] = $row['main_class'].":".$row['sub_class'];
    $label_no[$line] = $row['label_no'];
    $beer_comment[$line] = $row['comment'];
    // Hämta namn till user_id.
    $query2 = "SELECT name FROM User_data ".
             "WHERE user_id = ".$user_id[$line].
             " AND deleted = 0";
    $result2 = mysqli_query($dbc, $query2);
    if (mysqli_num_rows($result2) == 1) {
      $row2 = mysqli_fetch_array($result2);
      $user_name[$line] = $row2['name'];
    } else {
      die ("dt_list.php: No data or too many data found in User_data.");
    }
    // Hämta bryggarnas namn och e-post.
    $query2 = "SELECT Brewers_of_beer.brewer_id, Brewers.brewer_name, Brewers.brewer_email FROM Brewers_of_beer ".
              "INNER JOIN Brewers USING (brewer_id) ".
              "WHERE Brewers_of_beer.beer_id = ".$_SESSION['dt_beer_id'][$line]." ".
              "AND Brewers_of_beer.deleted = 0";
    $result2 = mysqli_query($dbc, $query2);
    $i=0;
    $brewer_names[$line] = "";
    $brewer_emails[$line] = "";
    while ($row2 = mysqli_fetch_array($result2)) {
      if ($i > 0) {
        $brewer_names[$line] = $brewer_names[$line] . ", " . $row2['brewer_name'];
        $brewer_emails[$line] = $brewer_emails[$line] . " " . $row2['brewer_email'];
      } else {
        $brewer_names[$line] = $row2['brewer_name'];
        $brewer_emails[$line] = $row2['brewer_email'];
      }
      $i++;
    }
    // Översätt type_id till text.
    $type_name[$line] = "";
    for ($i=0; $i < count($_SESSION['type_values']); $i++) {
      if ($_SESSION['type_values'][$i] == $type_id[$line]) {
        $type_name[$line] = $_SESSION['type_names'][$i];
      }
    }
    $og[$line] = $row['og'];
    $fg[$line] = $row['fg'];
    $bu[$line] = $row['bu'];
    $alc[$line] = $row['alc'];
    // Hämta received, disq och comment från Dt_event_reg.
    $query2 = "SELECT received, disq, comment FROM Dt_event_reg ".
              "WHERE beer_id = ".$_SESSION['dt_beer_id'][$line]." ".
              "AND event_id = ".$_SESSION['dt_event_id']." ".
              "AND deleted = 0";
    $result2 = mysqli_query($dbc, $query2);
    if (mysqli_num_rows($result2) == 0) {
      $dt_received[$line] = 0;
      $dt_disq[$line] = 0;
      $dt_comment[$line] = "";
    }
    if (mysqli_num_rows($result2) == 1) {
      $row2 = mysqli_fetch_array($result2);
      $dt_received[$line] = $row2['received'];
      $dt_disq[$line] = $row2['disq'];
      $dt_comment[$line] = $row2['comment'];
    }
    if (mysqli_num_rows($result2) > 1) {
      die ("dt_list.php: No data or too many data found in Dt_event_reg.");
    }
    // Hämta price från Prices.
    $query2 = "SELECT price FROM Prices ".
              "WHERE beer_id = ".$_SESSION['dt_beer_id'][$line]." ".
              "AND event_id = ".$_SESSION['dt_event_id']." ".
              "AND deleted = 0";
    $result2 = mysqli_query($dbc, $query2);
    if (mysqli_num_rows($result2) == 0) {
      $price[$line] = "";
    }
    if (mysqli_num_rows($result2) == 1) {
      $row2 = mysqli_fetch_array($result2);
      $price[$line] = $row2['price'];
    }
    if (mysqli_num_rows($result2) > 1) {
      die ("dt_list.php: No data or too many data found in Prices.");
    }
    $line++;
  }
  $_SESSION['no_dt_beers'] = $line;
  mysqli_close($dbc);



  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'"> ';
  echo '<p> <input type="submit" value="Adress-etiketter" name="addr" /> ';
  echo 'Adress-etiketter för huvudklasserna: ';
  echo '<input type="text" name="addr_filter_main" id="addr_filter_main" value="Alla" maxlength="100" /> ';
  echo 'T.ex. 1,4,6 ';
  echo '</p>';
  echo '<p> <input type="submit" value="ID-etiketter" name="id" /> ';
  echo 'ID-etiketter för huvudklasserna: ';
  echo '<input type="text" name="id_filter_main" id="id_filter_main" value="Alla" maxlength="100" /> ';
  echo 'T.ex. 1,4,6 ';
  echo '</p>';
  echo '<p> <input type="submit" value="Mottagna" name="dt_rec" /> ';
  echo 'Lista med endast mottagna och godkända öl. </p>';
  echo '<p class=head_2>Domartävlingen</p>';
  echo '<p>'.$_SESSION['no_dt_beers'].' öl anmälda.</p>';
  if ($_SESSION['adm_lev'] == 'FULL') {
    echo '<input type="submit" value="Spara" name="save" /> '; 
  }
  
  echo '<table> ';
  echo '<tr> ';
  echo '<td class=header> Öltyp </td> ';
  echo '<td class=header> Bryggarens kommentar </td> ';
  echo '<td class=header> Flasknr </td> ';
  echo '<td class=header> Ölets namn </td> ';
  echo '<td class=header> ID </td> ';
  echo '<td class=header> OG [g/l] </td> ';
  echo '<td class=header> FG [g/l] </td> ';
  echo '<td class=header> Beska [BU] </td> ';
  echo '<td class=header> Alk [vol %] </td> ';
  echo '<td class=header> Mottaget </td> ';
  echo '<td class=header> Diskad </td> ';
  echo '<td class=header> Kommentar </td> ';
  echo '<td class=header> Prisplats </td> ';
  echo '<td class=header> Registrerad av </td> ';
  echo '<td class=header> Bryggare </td> ';
  echo '<td class=header> e-post </td> ';
  echo '<td class=header> Adress </td> ';
  echo '<td class=header> Receptlänk </td> ';
  echo '</tr>';

  for ($i=0; $i<$_SESSION['no_dt_beers']; $i++) {
    // Skapa HTML-kod för listan.
    echo '<tr> ';
    echo '<td> '.$type_name[$i]. ' </td> ';
    echo '<td> '.$beer_comment[$i]. ' </td> ';
    echo '<td> '.$type_id[$i].'-'.$label_no[$i]. ' </td> ';
    echo '<td> '.$beer_name[$i]. ' </td> ';
    echo '<td> '.$user_id[$i]. ' </td> ';
    echo '<td> '.($og[$i]+1000). ' </td> ';
    echo '<td> '.($fg[$i]+1000). ' </td> ';
    echo '<td> '.$bu[$i]. ' </td> ';
    echo '<td> '.$alc[$i]. ' </td> ';
    echo '<td> <input type="checkbox" name="dt_received['.$i.']" value="1" '; 
    if ($_SESSION['adm_lev'] != 'FULL') { echo("disabled "); } 
    if($dt_received[$i]) { echo("checked"); } 
    echo '> </td>';
    echo '<td> <input type="checkbox" name="dt_disq['.$i.']" value="1" '; 
    if ($_SESSION['adm_lev'] != 'FULL') { echo("disabled "); } 
    if($dt_disq[$i]) { echo("checked"); } 
    echo '> </td>';
    echo '<td> <input type="text" name="dt_comment['.$i.']" id="dt_comment['.$i.'] " value="' . $dt_comment[$i] . '" maxlength="200" readonly';
    echo '/> </td>';
    if ($_SESSION['adm_lev'] == 'FULL') {
      echo '<td> <select name="price['.$i.']" id="price['.$i.']"> ';
      echo '<option value="" '; if($price[$i] == ""){echo("selected");}; echo '></option>';
      echo '<option value="Guld" '; if($price[$i] == "Guld"){echo("selected");}; echo '>Guld</option>';
      echo '<option value="Silver" '; if($price[$i] == "Silver"){echo("selected");}; echo '>Silver</option>';
      echo '<option value="Brons" '; if($price[$i] == "Brons"){echo("selected");}; echo '>Brons</option>';
      echo '<option value="SM-vinnare" '; if($price[$i] == "SM-vinnare"){echo("selected");}; echo '>SM-vinnare</option>';
      echo '</select> </td> ';
    } else {
      echo '<td> '.$price[$i]. ' </td> ';
    }
    echo '<td> '.$user_name[$i]. ' </td> ';
    echo '<td> '.$brewer_names[$i]. ' </td> ';
    echo '<td> '.$brewer_emails[$i]. ' </td> ';
    echo '<td> <a href="user_addr.php?user_id='.$user_id[$i].'">Adress</a> </td> ';
    echo '<td> <a href="recipe.php?beer_id='.$_SESSION['dt_beer_id'][$i].'">Recept</a> </td> ';
    echo '</tr>';
  }
  echo '</table>';
  echo '</form> ';
?>

<?php
  // Sidfot.
  require_once('footer.php');
?>

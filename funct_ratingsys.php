<?php
//funktioner för att hämta data från ratingsystemet
//i stort sett kopierat från ratingsystemets common.inc
class DbAccess
{
    private $dbConn = null;

    
    function __construct()
    {
    }

    function __destruct()
    {
        if ($this->dbConn != null) {
            mysqli_close($this->dbConn);
        }
        $this->dbConn = 0;

    }
    
    private function getConnection()
    {
        if ($this->dbConn === null) {

            $this->dbConn =  mysqli_connect(DB_RATE_HOST, DB_RATE_USER, DB_RATE_PASSWORD)
                or die('Unable to connect to database: ' . DB_RATE_HOST . " ," . mysqli_connect_error());
            $this->dbConn->select_db(DB_RATE_NAME);
            $this->dbConn->set_charset('utf8');
        }
        return $this->dbConn;
    }
    private function escape($s)
    {
        return $this->getConnection()->real_escape_string($s);
    }

    public function getCompetition($id)
    {
        $sql = 'SELECT count(*) as c FROM voteCodes WHERE competitionId=' . $this->escape($id);
        $result = $this->getConnection()->query($sql) or die("getCompetition: Query failed: $sql");

        $voteCodeCount = $result->fetch_assoc()['c'];

        $sql = 'SELECT name, openTime, closeTime, testingOpenUntilTime,lastEventRegCache, brewerLoginOpenFrom '
            . 'FROM competitions '
            . 'WHERE competitions.id=' . $this->escape($id);
        $result = $this->getConnection()->query($sql) or die("getCompetition: Query failed: $sql");

        if ($result->num_rows != 1)
            die("Invalid competition id." . $id);
        $row = $result->fetch_assoc();

        $openTime = new DateTime($row['openTime']);
        $closeTime = new DateTime($row['closeTime']);

        if (is_null($row['testingOpenUntilTime'])) {
            $testingOpenUntilTime = null;
        } else {
            $testingOpenUntilTime = new DateTime($row['testingOpenUntilTime']);
        }
        if (is_null($row['lastEventRegCache'])) {
            $lastEventRegCache = null;
        } else {
            $lastEventRegCache = new DateTime($row['lastEventRegCache']);
        }

        if (is_null($row['brewerLoginOpenFrom'])) {
            $brewerLoginOpenFrom = $closeTime; //fallback to closeTime
        } else {
            $brewerLoginOpenFrom = new DateTime($row['brewerLoginOpenFrom']);
        }
        //get evetreg event_id's corresponding to our $compentionId
        // $evenreg_event_ids = $this->getEventReg_EventId($id);
        // $ids = array();
        // $eventInfo = array();
        // foreach ($evenreg_event_ids as $event) {
        //     array_push($ids, $event['event_id']);
        //     //store event info in array, by id
        //     $eventInfo[$event['event_id']] = array('event_name' => $event['event_name'], 'competition' => $event['competition']);
        // }

        return array(
            'id' => $id,
            'name' => $row['name'],
            'openTime' => $openTime,
            'closeTime' => $closeTime,
            'brewerLoginOpenFrom' => $brewerLoginOpenFrom,  //when users can login to the brewer page at /rate/user for own beer statistics
            'testingOpenUntilTime' => $testingOpenUntilTime,
            'lastEventRegCache' => $lastEventRegCache,
            'voteCodeCount' => $voteCodeCount,
            //'eventReg_ids' => $ids,
            //'eventReg_Info' => $eventInfo,


        );
    }
    public function getCategories($competitionId, $NoEntries = false)
    {
        $sql = 'select id, name, description from categories ' .
            'where competitionId="' . $this->escape($competitionId) . '" order by id';
        $result = $this->getConnection()->query($sql) or die("getCategories: Query failed: $sql");

        $categories = array();
        $useOffset = false; //CONNECT_EVENTREG_DB > 1 ? true : false;
        $idStartPos = 0;

        while ($row = $result->fetch_assoc()) {
            $id = $row['id'];
            $name = $row['name'];
            $description = $row['description'];

            //when we don't want to get entries, we can skip the entries query
            //(ie rating-mode where entries are fetched separately fram the eventreg database)
            if ($NoEntries) {
                $entries = array();
            } else {
                $sql = "select entryCode from entries where categoryId=$id order by entryCode";
                $result2 = $this->getConnection()->query($sql) or die("getCategories: Query failed: $sql");
                $entries = array_map(function ($a) {
                    return $a[0];
                }, $result2->fetch_all());
            }
            $idStartPos = $useOffset ? $id : $idStartPos;
            $categories[$idStartPos] = array(
                'id' => $id,
                'name' => $name,
                'description' => $description,
                'entries' => $entries
            );
            $idStartPos++;
            // Notice there is also color field that could be filled in if added to the database.
        }

        return $categories;
    }


  }
// -*- coding: utf-8 -*-
"use strict";


$(function (event) {
	event_reg.init();
});

var event_reg = function () {
    

    


    function init_html() {
        //find input named comp_fv
        var comp_fv = $('input[name="comp_fv"]');
        //find option list named rating_category
        var rating_category = $('select[name="votesys_category"]');
        
        comp_fv.change(function(){
            rating_category.prop('disabled', !comp_fv.prop('checked'));
        });

    }

    function init() {
        init_html();   
    }

    return {
        init: init
    }
}();




<?php
  //header('Content-type: text/html; charset=utf-8');

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("set_sessions.php: Connection failed: " . mysqli_connect_error());
  }
 
  // Nollställ sessionsvariabler.
  $_SESSION['beer_slots_sel'] = 0;
  $_SESSION['next_to_sel'] = 0;
  $_SESSION['bar_length'] = 0;
  $_SESSION['no_hops_forms'] = 0;
  $_SESSION['no_others_stages'] = 0;
  $_SESSION['fv_votesys_competition_id'] = 0;
  $_SESSION['et_votesys_competition_id'] = 0;
  $_SESSION['fv_allow_user_votesys_category_selection'] = 0;
  $_SESSION['fv_event_closed_brewerinfo_ready'] =0;
  $_SESSION['rating_category'] = 0;
  $_SESSION['votesys_category'] = 0;
  unset ($_SESSION['dt_event_id']);
  unset ($_SESSION['fv_event_id']);
  unset ($_SESSION['et_event_id']);
  unset ($_SESSION['type_main']);
  unset ($_SESSION['type_sub']);
  unset ($_SESSION['type_values']);
  unset ($_SESSION['type_names']);
  unset ($_SESSION['type_ogmax']);
  unset ($_SESSION['type_alcmax']);
  unset ($_SESSION['hops_form_id']);
  unset ($_SESSION['hops_form_name']);
  unset ($_SESSION['others_stage_id']);
  unset ($_SESSION['others_stage_name']);
  unset ($_SESSION['dt_event_msg']);
  unset ($_SESSION['fv_event_msg']);
  unset ($_SESSION['et_event_msg']);
  unset ($_SESSION['low_alc_in_fv']);

  // Sätt event_id. Används för att sätta event_name och type_def.
  $_SESSION['dt_event_id'] = FilterPost ($dbc, $_GET['dt_event_id'], 100);
  $_SESSION['fv_event_id'] = FilterPost ($dbc, $_GET['fv_event_id'], 100);
  $_SESSION['et_event_id'] = FilterPost ($dbc, $_GET['et_event_id'], 100);
  if (!empty($_SESSION['et_event_id'])) {
    $event_id = $_SESSION['et_event_id'];
  } elseif (!empty($_SESSION['fv_event_id'])) {
    $event_id = $_SESSION['fv_event_id'];
  } elseif (!empty($_SESSION['dt_event_id'])) {
    $event_id = $_SESSION['dt_event_id'];
  }

  // Sätt extra_fv, beers_per_table, table_length, max_no_tables och low_alc_in_fv.
  if (!empty($_SESSION['fv_event_id'])) {
    $query = "SELECT extra_fv, low_alc, beers_per_table, table_length, max_no_tables,votesys_competition_id, fv_allow_user_votesys_category_selection FROM Events WHERE event_id = '".$_SESSION['fv_event_id']."' AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($data);
    if (mysqli_num_rows($data) == 0) {
      die("set_sessions.php: event_id = ".$_SESSION['fv_event_id']." not found in table Events.");
    }
    $_SESSION['beers_per_table'] = $row['beers_per_table'];
    $_SESSION['table_length'] = $row['table_length'];
    $_SESSION['max_no_tables'] = $row['max_no_tables'];
    // Sätt low_alc_in_fv om det inte är tillåtet att välja kategori själv (isf sätt lågaklohol upp som kategori i votesys)
    if (!($row['fv_allow_user_votesys_category_selection'] == 1))
      $_SESSION['low_alc_in_fv'] = $row['low_alc'];
    else
      $_SESSION['low_alc_in_fv'] = 0;

    $_SESSION['extra_fv'] = $row['extra_fv'];
    $_SESSION['fv_votesys_competition_id'] = $row['votesys_competition_id'];
    $_SESSION['fv_allow_user_votesys_category_selection'] = $row['fv_allow_user_votesys_category_selection'];
  }

  // Sätt dt_max_no_beers.
  if (!empty($_SESSION['dt_event_id'])) {
    $query = "SELECT max_no_beers FROM Events WHERE event_id = '".$_SESSION['dt_event_id']."' AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($data);
    if (mysqli_num_rows($data) == 0) {
      die("set_sessions.php: event_id = ".$_SESSION['dt_event_id']." not found in table Events.");
    }
    $_SESSION['dt_max_no_beers'] = $row['max_no_beers'];
  }

  // Sätt fv_max_no_beers.
  if (!empty($_SESSION['fv_event_id'])) {
    $query = "SELECT max_no_beers FROM Events WHERE event_id = '".$_SESSION['fv_event_id']."' AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($data);
    if (mysqli_num_rows($data) == 0) {
      die("set_sessions.php: event_id = ".$_SESSION['fv_event_id']." not found in table Events.");
    }
    $_SESSION['fv_max_no_beers'] = $row['max_no_beers'];
  }

  // Sätt et_max_no_beers.
  if (!empty($_SESSION['et_event_id'])) {
    $query = "SELECT max_no_beers,votesys_competition_id  FROM Events WHERE event_id = '".$_SESSION['et_event_id']."' AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($data);
    if (mysqli_num_rows($data) == 0) {
      die("set_sessions.php: event_id = ".$_SESSION['et_event_id']." not found in table Events.");
    }
    $_SESSION['et_max_no_beers'] = $row['max_no_beers'];
    $_SESSION['et_votesys_competition_id'] = $row['votesys_competition_id'];
  }


  // Sätt event_name, type_def.
  $query = "SELECT event_name, type_def, extra_fv FROM Events WHERE event_id = '$event_id' AND deleted = 0";
  $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $row = mysqli_fetch_array($data);
  if (mysqli_num_rows($data) == 0) {
    die("set_sessions.php: event_id = ".$event_id." not found in table Events.");
  }
  $_SESSION['event_name'] = $row['event_name'];
  $_SESSION['type_def'] = $row['type_def'];

  // Hämta öltypsdefinitionerna.
//  SHBF: http://styles.shbf.se/json/2018/styles
// Lokalt: http://localhost/hostol-2018.json
// Lokalt: http://localhost/extra_fv_classes.json
// Nordlil: http://www.nordlil.se/extra_fv_classes.json
// Nordlil: http://www.nordlil.se/hostol-2018.json
// LHBF: redirects (http://styles.lhbf.net) fungerar ej (fil läses tillsynes OK, men json_decode fallerar ), så $url = "./json/styles.json";
  $url = $_SESSION['type_def'];
  
  
  
  try {$json = file_get_contents($url);  //FILE_TEXT deprecated
  } catch (Exception $e) {
    die("set_sessions.php: No type def found at ".$url);
  }  
  if (empty($json) || $json=="") {
    die("set_sessions.php: No type def found at ".$url);
  }
  
 // $json = mb_convert_encoding($json, 'HTML-ENTITIES', "UTF-8");
  $allStyles = json_decode($json ,true);
  //echo 'Last error: ', json_last_error_msg(), PHP_EOL, PHP_EOL;
  //var_dump($allStyles);
  $i=0;
  foreach ($allStyles as $class) {
    if (isset($class['styles'])) {
      foreach ($class['styles'] as $style) {

        $_SESSION['type_main'][$i] = ($class['number']);
        echo $_SESSION['type_main'][$i];
        $_SESSION['type_sub'][$i] = ($style['letter']);
        $_SESSION['type_values'][$i] = $_SESSION['type_main'][$i].":".$_SESSION['type_sub'][$i];
        $type_names = $class['number'].":".$style['letter']." ".$style['name'];
        $_SESSION['type_names'][$i] = ($type_names);
        if (!empty($style['ogMax'])) {
          $_SESSION['type_ogmax'][$i] = $style['ogMax']*1000-1000;
        } else {
          $_SESSION['type_ogmax'][$i] = NULL;
        }
        if (!empty($style['abvMax'])) {
          $_SESSION['type_alcmax'][$i] = $style['abvMax'];
        } else {
          $_SESSION['type_alcmax'][$i] = NULL;
        }
        $i++;
      }
    }
  }
  if (!isset($_SESSION['type_values']) || $_SESSION['type_values'] == "" || !isset($_SESSION['type_names'])|| $_SESSION['type_names'] == "") {
    die("set_sessions.php: Invalid type def at " . $url);
  }

  if (!empty($_SESSION['extra_fv'])) {
    // Lägg till extra FV-klasser.
    $url = $_SESSION['extra_fv'];
    try {$json = file_get_contents($url, FILE_TEXT);
    } catch (Exception $e) {
      die("set_sessions.php: No type def found at ".$url);
    }  
    if (empty($json) || $json=="") {
      die("set_sessions.php: No type def found at ".$url);
    }
  //  $json = mb_convert_encoding($json, 'HTML-ENTITIES', "UTF-8");
    $allStyles = json_decode($json, true);
    foreach ($allStyles as $class) {
      foreach ($class['styles'] as $style) {
        $_SESSION['type_main'][$i] = utf8_decode ($class['number']);
        $_SESSION['type_sub'][$i] = utf8_decode ($style['letter']);
        $_SESSION['type_values'][$i] = $_SESSION['type_main'][$i].":".$_SESSION['type_sub'][$i];
        $type_names = $class['number'].":".$style['letter']." ".$style['name'];
        $_SESSION['type_names'][$i] = utf8_decode ($type_names);
        if (!empty($style['ogMax'])) {
          $_SESSION['type_ogmax'][$i] = $style['ogMax']*1000-1000;
        } else {
          $_SESSION['type_ogmax'][$i] = NULL;
        }
        if (!empty($style['abvMax'])) {
          $_SESSION['type_alcmax'][$i] = $style['abvMax'];
        } else {
          $_SESSION['type_alcmax'][$i] = NULL;
        }
        $i++;
      }
    }
    if (!isset($_SESSION['type_values']) || $_SESSION['type_values'] == "" || !isset($_SESSION['type_names'])|| $_SESSION['type_names'] == "") {
      die("set_sessions.php: Invalid type def");
    }
  }

  // Hämta humleformer (kottar, pellets, m.m.)
  $query = "SELECT * FROM Hops_forms ".
           "WHERE deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $_SESSION['hops_form_id'][$i] = $row['hops_form_id'];
      $_SESSION['hops_form_name'][$i] = $row['hops_form_name'];
      $i++;
    }
    $_SESSION['no_hops_forms'] = mysqli_num_rows($result);
  } else {
    die("set_sessions.php: Hops_forms ".mysqli_error($dbc)." ".$query);
  }

  // Hämta tillsatt vid för övrigt (Kokning, mäskning, m.m.)
  $query = "SELECT * FROM Others_stages ".
           "WHERE deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $_SESSION['others_stage_id'][$i] = $row['others_stage_id'];
      $_SESSION['others_stage_name'][$i] = $row['others_stage_name'];
      $i++;
    }
    $_SESSION['no_others_stages'] = mysqli_num_rows($result);
  } else {
    die("set_sessions.php: Other_stages ".mysqli_error($dbc)." ".$query);
  }

  mysqli_close($dbc);

  // Gå till login-sidan om användaren inte är inloggad.
  if (!isset($_SESSION['user_id'])) {
    ReDirect ('login.php');
  } else {
    // Gå vidare till registreringssidan.
    ReDirect ('beer_reg_pre.php');
  }
?>

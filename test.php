<?php

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("et_list.php: "."Connection failed: " . mysqli_connect_error());
  }
  
  // Sidhuvud.
  $page_title = 'Test';
  require_once('header.php');

  // Visa navigeringslisten.
  require_once('header_nav.php');


  // Kod att testa
//date_timezone_set(NULL, timezone_open('Europe/Stockholm'));
// echo date_format($date, 'Y-m-d H:i:sP') . "\n";
// echo "<p>".date_timezone_set(NULL, timezone_open('Europe/London'))."</p>";
// echo "<p>".date('Y-m-d H:i:s')."</p>";
// $date = date_create(NULL, timezone_open('Europe/Stockholm'));
// echo "<p>".date_format($date, 'Y-m-d H:i:s')."</p>";

// echo "<p>".phpinfo()."</p>";
// echo "<p>".mb_list_encodings()."</p>";

//  echo "<p>1:".basename(__FILE__, ".php")."</p>";
//  echo "<p>1:".CurrUrl()."</p>";


  // Skapa lista med alla användare som hittills anmält öl till det här evenemanget.
  // Används om man vill ange någon att stå bredvid.
  $query = "SELECT Beers.user_id, User_data.name FROM Beers_in_event ".
           "INNER JOIN Beers USING (beer_id) INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$_SESSION['fv_event_id']." AND Beers.user_id != ".$_SESSION['user_id'].
           " AND Beers.deleted = 0 AND Beers_in_event.deleted = 0 "."ORDER BY User_data.name ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $i = 0;
  while ($row = mysqli_fetch_array($result)) {
    if ($i > 0) {
      if ($reg_users_id[$i-1] != $row['user_id']) {
        $reg_users_id[$i] = $row['user_id'];
        $reg_users_name[$i] = $row['name'];
      $i++;
      }
    } else {
      $reg_users_id[$i] = $row['user_id'];
      $reg_users_name[$i] = $row['name'];
      $i++;
    }
  }

foreach ($reg_users_name as $name) {
  echo "<p> $name </p>";
}
foreach ($reg_users_id as $id) {
  echo "<p> $id </p>";
}


  // Sidfot
  require_once('footer.php');
?>

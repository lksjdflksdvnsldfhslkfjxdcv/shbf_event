<?php
  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');
  require_once("Mail.php");

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Nollställ ok och err_msg.
  $ok = 1;
  $msg = "";
  $err_msg = " ";

  // Anslut till databasen
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("update.php. Connection failed: " . mysqli_connect_error());
  }

  if (isset($_POST['submit'])) {
    // Hämta värden från POST
    $_SESSION['email'] = FilterPost ($dbc, $_POST['email'], 50);
    $_SESSION['member_no'] = FilterPost ($dbc, $_POST['member_no'], 50);
    $_SESSION['care_of'] = FilterPost ($dbc, $_POST['care_of'], 35);
    $_SESSION['street'] = FilterPost ($dbc, $_POST['street'], 35);
    $_SESSION['post_no'] = FilterPost ($dbc, $_POST['post_no'], 10);
    $_SESSION['city'] = FilterPost ($dbc, $_POST['city'], 35);
    $_SESSION['passwd'] = FilterPost ($dbc, $_POST['passwd'], 1000);
    $_SESSION['name'] = FilterPost ($dbc, $_POST['name'], 35);

    // Hämta användaruppgifterna från databasen.
    $query = "SELECT * FROM User_data WHERE user_id = ".$_SESSION['user_id']." AND code = 0 AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) == 0) {
      die("update.php: No data found in database.");
    }
    $row = mysqli_fetch_array($result);
    $old_email = $row['email'];
    $adm_lev = $row['adm_lev'];  // Kan inte ändras av användaren.
    $passwd_sha = $row['passwd']; // Krypterat lösenord.

    // Kontrollera att alla fält är ifyllda.
    if ( empty($_SESSION['name']) || empty($_SESSION['email']) ) {
      $err_msg = $err_msg."Alla fält måste fyllas i. ";
      $ok = 0;
    }

    // Kontrollera att e-postadressen inte finns om den har ändrats.
    if ($_SESSION['email'] != $old_email) {
      $query = "SELECT * FROM User_data WHERE email = '".$_SESSION['email']."'"." AND code = 0 AND deleted = 0";
      $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      if (mysqli_num_rows($data) != 0) {
        $err_msg = $err_msg."e-postadressen finns redan registrerad. ";
        $ok = 0;
      }
    }

    // Kontrollera att e-postformatet är giltigt.
    if ( ($_SESSION['email'] != $old_email) && !filter_var($_SESSION['email'], FILTER_VALIDATE_EMAIL)) {
      $err_msg = $err_msg."Ogiltigt e-postformat ";
      $ok = 0;
    }

    // Kolla att lösenordet är tillräckligt långt om ett nytt har matats in.
    if ( ($_SESSION['passwd'] != "") && (strlen($_SESSION['passwd']) < MIN_CHARS_PASSWD) ) {
      $err_msg = $err_msg."Lösenordet måste ha minst ".MIN_CHARS_PASSWD." tecken. ";
      $ok = 0;
    }



    if ($ok) {
      // Markera nuvarande rad i User_data som raderad.
      $query = "UPDATE User_data SET deleted = 1 ".
               "WHERE user_id = ".$_SESSION['user_id']." AND ".
               "deleted = 0";
      if (!mysqli_query($dbc, $query)) {
        die("update.php: ".mysqli_error($dbc).$query);
      }

      // Lägg in uppgifterna i User_data. Görs olika beroende på om lösenordet ändrats eller inte.
      if ($_SESSION['passwd'] != "") {
        $query = "INSERT INTO User_data (user_id, name, member_no, email, care_of, street, post_no, city, adm_lev, passwd, code) ".
                 "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['name']."', '".$_SESSION['member_no']."', '".$_SESSION['email']."', '".$_SESSION['care_of']."', '".
                 $_SESSION['street']."', '".$_SESSION['post_no']."', '".$_SESSION['city']."', '$adm_lev', SHA('".$_SESSION['passwd']."'), '0')";
      } else {
        $query = "INSERT INTO User_data (user_id, name, member_no, email, care_of, street, post_no, city, adm_lev, passwd, code) ".
                 "VALUES ('".$_SESSION['user_id']."', '".$_SESSION['name']."', '".$_SESSION['member_no']."', '".$_SESSION['email']."', '".$_SESSION['care_of']."', '".
                 $_SESSION['street']."', '".$_SESSION['post_no']."', '".$_SESSION['city']."', '$adm_lev', '$passwd_sha', '0')";
      }
      if (!mysqli_query($dbc, $query)) {
        die("update.php: ".mysqli_error($dbc).$query);
      }
      $msg = $msg."Dina användaruppgifter har uppdaterats. ";
      // Skicka e-post till användarens gamla e-postadress.
      $to = $old_email;
      $subject = "SHBF registrering";
      $mail_msg = "Dina användaruppgifter har uppdaterats. \n";
      $mail_msg = wordwrap($mail_msg, 70, "\n");
      SendMail($to, $subject, $mail_msg);
    }
  }

  mysqli_close($dbc);
?>




<?php
  // Sidhuvud.
  $page_title = 'Updatera användarkonto';
  require_once('header_nav.php');

?>

  <p class="error"> <?php echo $err_msg ?> </p>
  <p> <?php echo $msg ?> </p>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <fieldset>
      <legend>Ändra användaruppgifter</legend>
      <label for="name">Namn:</label>
      <input type="text" name="name" id="name" value="<?php echo $_SESSION['name'] ?>" maxlength="35" /><br />
      <label for="member_no">Medlemsnummer:</label>
      <input type="number" name="member_no" id="member_no" min=0 value="<?php echo $_SESSION['member_no'] ?>" /><br>
      <label for="email">e-post:</label>
      <input type="text" name="email" id="email" value="<?php echo $_SESSION['email'] ?>" maxlength="80" /><br />
      <label for="care_of">Ev. C/O:</label>
      <input type="text" name="care_of" id="care_of" value="<?php echo $_SESSION['care_of'] ?>" maxlength="35" /><br />
      <label for="street">Gata:</label>
      <input type="text" name="street" id="street" value="<?php echo $_SESSION['street'] ?>" maxlength="35" /><br />
      <label for="post_no">Postnummer:</label>
      <input type="text" name="post_no" id="post_no" value="<?php echo $_SESSION['post_no'] ?>" maxlength="10" /><br />
      <label for="city">Ort:</label>
      <input type="text" name="city" id="city" value="<?php echo $_SESSION['city'] ?>" maxlength="35" /><br />
      <label for="passwd">Lösenord:</label>
      <input type="password" name="passwd" id="passwd" value="" maxlength="1000" /><br />
    </fieldset>
    <input type="submit" value="Uppdatera" name="submit" />
  </form>

<?php
  // Sidfot.
  require_once('footer.php');
?>

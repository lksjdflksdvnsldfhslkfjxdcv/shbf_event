<?php
  // Insert the page header
  $page_title = 'Aktivering';
  require_once('header.php');

  require_once('const.php');
  require_once('funct.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("set_sessions.php: Connection failed: " . mysqli_connect_error());
  }

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Läs id och kod.
  $user_id_get = FilterPost ($dbc, $_GET['id'], 100);
  $code_get = FilterPost ($dbc, $_GET['code'], 100);

  $query = "SELECT code FROM User_data WHERE user_id = '$user_id_get' AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $row = mysqli_fetch_row($result);
  $code = $row[0];
  if ( ($code != 0) && ($code == $code_get) ) {
    $query = "UPDATE User_data SET code = 0 WHERE user_id = '$user_id_get' AND deleted = 0";
    if (!mysqli_query($dbc, $query)) {
      die("activate ".mysqli_error($dbc).$query);
    }
    echo '<p>Ditt konto är nu aktiverat.</p>';
  } elseif ($code == 0) {
    echo '<p>Kontot är redan aktiverat.</p>';
  } elseif ($code != $code_get) {
    echo '<p>Aktivering av ditt konto misslyckades. Fel kod.</p>';
  }

  mysqli_close($dbc);

  // Insert the page footer
  require_once('footer.php');
?>

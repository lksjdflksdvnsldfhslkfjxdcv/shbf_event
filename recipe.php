<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (isset($_GET['beer_id']) && is_numeric($_GET['beer_id'])) {
    $beer_id = FilterPost ($dbc, $_GET['beer_id'], 100);
  } else {
    die ("recipe.php: Missing beer_id.");
  }


  // Hämta öldata.
  $query = "SELECT * FROM Beer_data WHERE beer_id = ".$beer_id." AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $row = mysqli_fetch_array($result);
  $beer_name = $row['beer_name'];
  $type_name = $row['type_name'];
  $volume = $row['volume'];
  $og = $row['og'];
  $fg = $row['fg'];
  $bu = $row['bu'];
  $alc = $row['alc'];
  $mashing = $row['mashing'];
  $ferment = $row['ferment'];
  $water = $row['water'];
  $comment = $row['comment'];

  // Hämta bryggarnas namn.
  $query = "SELECT Brewers_of_beer.brewer_id, Brewers.brewer_name FROM Brewers_of_beer ".
           "INNER JOIN Brewers USING (brewer_id) ".
           "WHERE Brewers_of_beer.beer_id = '$beer_id' ".
           "AND Brewers_of_beer.deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $i=0;
  $brewer_names = "";
  while ($row = mysqli_fetch_array($result)) {
    if ($i > 0) {
      $brewer_names = $brewer_names . ", " . $row['brewer_name'];
    } else {
      $brewer_names = $row['brewer_name'];
    }
    $i++;
  }


  // Hämta eventinfo.
  $query = "SELECT Beers_in_event.event_id, Events.event_name, ".
           "Events.comp FROM Beers_in_event ".
           "INNER JOIN Events USING (event_id) ".
           "WHERE Beers_in_event.beer_id = ".$beer_id." ".
           "AND Beers_in_event.deleted = 0 ".
           "ORDER BY Events.event_name ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $in_events = "";
  $first = 1;
  if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
      $event_id = $row['event_id'];
      $event_name = $row['event_name'];
      $comp = $row['comp'];
      $price = "";
      // Hämta prisinfo.
      $query2 = "SELECT price FROM Prices ".
               "WHERE beer_id = ".$beer_id." AND event_id = ".$event_id." ".
               "AND deleted = 0 ";
      $result2 = mysqli_query($dbc, $query2) or die (mysqli_error($dbc));
      if (mysqli_num_rows($result2) > 0) {
        while ($row2 = mysqli_fetch_array($result2)) {
          $price = $row2['price'];
        }
      }
      if (!empty($price)) {
      $event_str = $price . " i " . $event_name . " ". $comp;
      } else {
      $event_str = "Anmäld till " . $event_name . " ". $comp;
      }
	    if ($first) {
	      $in_events = $event_str;
	      $first = 0;
	    } else {
	      $in_events = $in_events . " \n". $event_str;
	    }
    }
  }


  // Hämta maltinfo.
  $query = "SELECT Malts_in_beer.malts_in_beer_id, Malts_in_beer.malt_id, ".
           "Malts_in_beer.malt_weight, Malts_in_beer.malt_comment, Malts.malt_name FROM Malts_in_beer ".
           "INNER JOIN Malts USING (malt_id) ".
           "WHERE Malts_in_beer.beer_id = ".$beer_id.
           " AND Malts_in_beer.deleted = 0".
           " ORDER BY Malts_in_beer.malt_weight DESC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $malts_name[$i] = $row['malt_name'];
      $malts_weight[$i] = $row['malt_weight'];
      $malts_comment[$i] = $row['malt_comment'];
      $i++;
    }
    $no_malts = mysqli_num_rows($result);
  } else {
    $no_malts = 0;
  }

  // Hämta humleinfo.
  $query = "SELECT Hops_in_beer.hops_weight, Hops.hops_name, ".
            "Hops.hops_form_id, Hops.hops_alpha, Hops_in_beer.hops_boil_time, ".
            "Hops_in_beer.hops_comment, Hops_forms.hops_form_name ".
            "FROM Hops_in_beer ".
            "INNER JOIN Hops USING (hops_id) ".
            "INNER JOIN Hops_forms USING (hops_form_id) ".
            "WHERE Hops_in_beer.beer_id = ".$beer_id.
            "  AND Hops_in_beer.deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $hops_name[$i] = $row['hops_name'];
      $hops_form_name[$i] = $row['hops_form_name'];
      $hops_weight[$i] = $row['hops_weight'];
      $hops_alpha[$i] = $row['hops_alpha'];
      $hops_boil_time[$i] = $row['hops_boil_time'];
      $hops_comment[$i] = $row['hops_comment'];
      $i++;
    }
    $no_hops = mysqli_num_rows($result);
  } else {
    $no_hops = 0;
  }

  // Hämta övrigtinfo.
  $query = "SELECT Others_in_beer.others_weight, Others.others_name, ".
           "Others.others_stage_id, Others_in_beer.others_comment, ".
           "Others_stages.others_stage_name ".
           "FROM Others_in_beer ".
           "INNER JOIN Others USING (others_id) ".
           "INNER JOIN Others_stages USING (others_stage_id) ".
           "WHERE Others_in_beer.beer_id = ".$beer_id.
           "  AND Others_in_beer.deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $others_name[$i] = $row['others_name'];
      $others_stage_name[$i] = $row['others_stage_name'];
      $others_weight[$i] = $row['others_weight'];
      $others_comment[$i] = $row['others_comment'];
      $i++;
    }
    $no_others = mysqli_num_rows($result);
  } else {
    $no_others = 0;
  }
	
?>



<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  echo '<table>';
  echo '<tr>';
  echo '<td class=head_1 colspan="5">'.$beer_name.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td colspan="5"><pre>'.$in_events.'</pre></td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td class=head_2 colspan="5">'.$type_name.'</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td> Volym: '.$volume.' l </td>';
  echo '<td> OG: '.$og.' &#176Ö </td>';
  echo '<td> FG: '.$fg.' &#176Ö </td>';
  echo '<td> Beska: '.$bu.' BU </td>';
  echo '<td> Alkoholhalt: '.$alc.' % </td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td class=head_3 colspan="5">Bryggare</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td colspan="5">'.$brewer_names.'</td>';
  echo '</tr>';
  echo '</table>';
  echo '<table>';
  echo '<tr>';
  echo '<td class=head_2 colspan="5">Kokning</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td class=head_3>Humle / krydda</td>';
  echo '<td class=head_3>Form</td>';
  echo '<td class=head_3_r>Vikt [g]</td>';
  echo '<td class=head_3_r>Alfasyra [%]</td>';
  echo '<td class=head_3_r>Koktid [min]</td>';
  echo '<td class=head_3>Kommentar</td>';
  echo '</tr>';
  for ($i=0; $i<$no_hops; $i++) {
    echo '<tr>';
    echo '<td>'.$hops_name[$i].'</td>';
    echo '<td>'.$hops_form_name[$i].'</td>';
    echo '<td class=norm_r>'.$hops_weight[$i].'</td>';
    echo '<td class=norm_r>'.$hops_alpha[$i].'</td>';
    echo '<td class=norm_r>'.$hops_boil_time[$i].'</td>';
    echo '<td>'.$hops_comment[$i].'</td>';
    echo '</tr>';
  }
  echo '</table>';
  echo '<table>';
  echo '<tr>';
  echo '<td class=head_2 colspan="5">Mäskning</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td class=head_3>Malt / extraktgivare</td>';
  echo '<td class=head_3_r>Vikt [g]</td>';
  echo '<td class=head_3>Kommentar</td>';
  echo '</tr>';
  for ($i=0; $i<$no_malts; $i++) {
    echo '<tr>';
    echo '<td>'.$malts_name[$i].'</td>';
    echo '<td class=norm_r>'.$malts_weight[$i].'</td>';
    echo '<td>'.$malts_comment[$i].'</td>';
    echo '</tr>';
  }
  echo '<tr>';
  echo '<td colspan="3"> <pre>'.$mashing.'</pre> </td>';
  echo '</tr>';
  echo '</table>';

  echo '<table>';
  echo '<tr>';
  echo '<td class=head_2 colspan="4">Övriga ingredienser</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td class=head_3>Ingrediens</td>';
  echo '<td class=head_3>Tillsatt vid</td>';
  echo '<td class=head_3_r>Vikt [g]</td>';
  echo '<td class=head_3>Kommentar</td>';
  echo '</tr>';
  for ($i=0; $i<$no_others; $i++) {
    echo '<tr>';
    echo '<td>'.$others_name[$i].'</td>';
    echo '<td>'.$others_stage_name[$i].'</td>';
    echo '<td class=norm_r>'.$others_weight[$i].'</td>';
    echo '<td>'.$others_comment[$i].'</td>';
    echo '</tr>';
  }
  echo '</table>';

  echo '<table>';
  echo '<tr>';
  echo '<td class=head_2>Jäsning</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td><pre>'.$ferment.'</pre></td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class=head_2>Vattenbehandling</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td><pre>'.$water.'</pre></td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class=head_2>Kommentar</td>';
  echo '</tr>';
  echo '<tr>';
  echo '<td><pre>'.$comment.'</pre></td>';
  echo '</tr>';
  echo '</table>';

  // Sidfot.
  require_once('footer.php');
?>


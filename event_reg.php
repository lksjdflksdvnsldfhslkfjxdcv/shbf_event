<?php

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');
  require_once("Mail.php");

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sätt värden på variabler.
  $dbg_msg = "";
  $err_msg = "";
  $_SESSION['update'] = 0;
  $_SESSION['beer_id'] = NULL;
  $_SESSION['beer_slots_sel'] = 0;
  $reg_users_id = NULL;

  $dbg_msg = "";

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
  // Skapa lista med alla användare som hittills anmält öl till det här evenemanget.
  // Används om man vill ange någon att stå bredvid.
  if (!empty($_SESSION['fv_event_id'])) {
    $query = "SELECT Beers.user_id, User_data.name FROM Beers_in_event ".
             "INNER JOIN Beers USING (beer_id) INNER JOIN User_data USING (user_id) ".
             "INNER JOIN Users USING (user_id) ".
             "WHERE Beers_in_event.event_id = ".$_SESSION['fv_event_id']." AND Beers.user_id != ".$_SESSION['user_id']." ".
             "AND Beers.deleted = 0 AND Beers_in_event.deleted = 0 ".
             "AND Users.deleted = 0 AND User_data.deleted = 0 ".
             "ORDER BY User_data.name ASC";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $i = 0;
    while ($row = mysqli_fetch_array($result)) {
      if ($i > 0) {
        if ($reg_users_id[$i-1] != $row['user_id']) {
          $reg_users_id[$i] = $row['user_id'];
          $reg_users_name[$i] = $row['name'];
          $i++;
        }
      } else {
        $reg_users_id[$i] = $row['user_id'];
        $reg_users_name[$i] = $row['name'];
        $i++;
      }
    }
  }
  
  // Ta reda på antalet användare som finns registrerade.
  $no_users = 0;
	$query = "SELECT user_id FROM Users ".
	         "WHERE Users.deleted = 0";
	$result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
	$no_users = mysqli_num_rows($result);
  // Ta reda på antalet öl som användaren har anmält till domartävlan.
  $dt_beers = 0;
  if (!empty($_SESSION['dt_event_id'])) {
    $query = "SELECT Beers_in_event.beer_id FROM Beers_in_event ".
             "INNER JOIN Beers USING (beer_id) ".
             "WHERE Beers.user_id = ".$_SESSION['user_id'].
             " AND Beers_in_event.event_id = ".$_SESSION['dt_event_id'].
             " AND Beers_in_event.deleted = 0 AND Beers.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $dt_beers = mysqli_num_rows($result);
  }
  // Ta reda på antalet öl som användaren har anmält till folkets val.
  $fv_beers = 0;
  if (!empty($_SESSION['fv_event_id'])) {
    $query = "SELECT Beers_in_event.beer_id FROM Beers_in_event ".
             "INNER JOIN Beers USING (beer_id) ".
             "WHERE Beers.user_id = ".$_SESSION['user_id'].
             " AND Beers_in_event.event_id = ".$_SESSION['fv_event_id'].
             " AND Beers_in_event.deleted = 0 AND Beers.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $fv_beers = mysqli_num_rows($result);
  }
  // Ta reda på antalet öl som användaren har anmält till etikettävlingen.
  $et_beers = 0;
  if (!empty($_SESSION['et_event_id'])) {
    $query = "SELECT Beers_in_event.beer_id FROM Beers_in_event ".
	     "INNER JOIN Beers USING (beer_id) ".
	     "WHERE Beers.user_id = ".$_SESSION['user_id'].
	     " AND Beers_in_event.event_id = ".$_SESSION['et_event_id'].
	     " AND Beers_in_event.deleted = 0 AND Beers.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $et_beers = mysqli_num_rows($result);
  }
  // Hämta data från Fv_event_reg.
  if (!empty($_SESSION['fv_event_id'])) {
    $query = "SELECT next_to_id, beer_slots, bar_length FROM Fv_event_reg ".
	     "WHERE user_id = ".$_SESSION['user_id'].
	     " AND fv_event_id = ".$_SESSION['fv_event_id'].
	     " AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_array($result);
      $_SESSION['beer_slots_sel'] = $row['beer_slots'];
      $_SESSION['next_to_sel'] = $row['next_to_id'];
      $_SESSION['bar_length'] = $row['bar_length'];
    }
  }
   
  if (isset($_POST['add_beer'])) {
    // Gå till Beer_reg.
    ReDirect ('beer_reg_pre.php');
  }

  if (isset($_POST['register'])) {
    $err_msg = "";
    $ok = 1;

    if (!empty($_SESSION['fv_event_id'])) {
      // Hämta data från formuläret.
      $_SESSION['beer_slots_sel'] = FilterPost ($dbc, $_POST['beer_slots'], 10);
      $_SESSION['next_to_sel'] = FilterPost ($dbc, $_POST['next_to'], 50);
      $_SESSION['bar_length'] = FilterPost ($dbc, $_POST['bar_length'], 10);

      // Kontrollera att antal bord eller barlängd är ifyllt om bryggaren anmält öl till FV.
      if ($fv_beers > 0) {
        if ( (($_SESSION['beer_slots_sel'] == 0) && ($_SESSION['bar_length'] == 0))  ||
	         (($_SESSION['beer_slots_sel'] != 0) && ($_SESSION['bar_length'] != 0)) ) {
	      $ok = 0;
	      $err_msg = $err_msg."Antal bord eller barlängd måste fyllas i. <".$_SESSION['beer_slots_sel']. "> <".$_SESSION['bar_length'].">";
        }
      }

      // Kontrollera att antal bord eller barlängd inte är för stort.
      if ( $_SESSION['beer_slots_sel'] > $fv_beers) {
        $ok = 0;
        $_SESSION['beer_slots_sel'] = $fv_beers;
        $err_msg = $err_msg."Du får inte mer än 1/".$_SESSION['beers_per_table']." bord per anmält öl.";
      }
      if ( $_SESSION['bar_length'] > (($_SESSION['table_length']/$_SESSION['beers_per_table']) * $fv_beers) ) {
        $ok = 0;
        $_SESSION['bar_length'] = strval(round($_SESSION['table_length']/$_SESSION['beers_per_table'],0)) * $fv_beers;
        $err_msg = $err_msg."Du får inte mer än ".strval(round($_SESSION['table_length']/$_SESSION['beers_per_table'],0))." cm per anmält öl.";
      }
    }
    if ($ok) {
      // Kontrollera om något har ändrats som ska lagras i fv_event_reg.
      // Om inget har ändrats så behöver inte fv_event_reg ändras.
      if (!empty($_SESSION['fv_event_id'])) {
        $query = "SELECT next_to_id, beer_slots, bar_length FROM Fv_event_reg ".
                 "WHERE user_id = ".$_SESSION['user_id'].
                 " AND fv_event_id = ".$_SESSION['fv_event_id'].
                 " AND deleted = 0";
        $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
        $row = mysqli_fetch_array($result);
        if (empty ($row['next_to_id']) ) {
          $next_to_id = 0;
        } else {
          $next_to_id = $row['next_to_id'];
        }
        
        $beer_slots = $row['beer_slots'] !== null ? $row['beer_slots'] : 0;
        $bar_length = $row['bar_length'] !== null ? $row['bar_length']: 0;
        if ($_SESSION['beer_slots_sel'] != $beer_slots ||
            $_SESSION['next_to_sel'] != $next_to_id ||
            $_SESSION['bar_length'] != $bar_length) {
          // Markera nuvarande rad i Fv_event_reg som raderad.
          $query = "UPDATE Fv_event_reg SET deleted = 1 ".
                 "WHERE user_id = ".$_SESSION['user_id'].
                 " AND fv_event_id = ".$_SESSION['fv_event_id'].
                 " AND deleted = 0";
          if (!mysqli_query($dbc, $query)) {
            die("event_reg.php: ".mysqli_error($dbc).$query);
          }
          // Lägg in användaren i Fv_event_reg
          $query = "INSERT INTO Fv_event_reg (user_id, fv_event_id, next_to_id, beer_slots, bar_length) ".
                   "VALUES (".$_SESSION['user_id'].", ".$_SESSION['fv_event_id'].", '".$_SESSION['next_to_sel'].
                   "', '".$_SESSION['beer_slots_sel']."', '".$_SESSION['bar_length']."')";
          if (!mysqli_query($dbc, $query)) {
            die("event_reg.php: ".mysqli_error($dbc).$query);
          }
        }
      }
      // Hämta användarens e-postadress.
      $query = "SELECT email FROM User_data ".
               "WHERE user_id = ".$_SESSION['user_id'].
               " AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      $row = mysqli_fetch_array($result);
      $to = $row['email'];
      // Skicka e-post till användaren.
      $msg = "";
      $subject = "Registrering ".$_SESSION['event_name'];
      if ($dt_beers > 0) {
        $msg = $msg."Du har anmält $dt_beers öl till domartävlan. \n";
      }
      if ($fv_beers > 0) {
        $msg = $msg."Du har anmält $fv_beers öl till folkets val. \n";
      }
      if ($et_beers > 0) {
        $msg = $msg."Du har anmält $et_beers öl till etikettävlingen. \n";
      }
      if ( ($dt_beers == 0) && ($fv_beers == 0) && ($et_beers == 0) ) {
        $msg = "Du har inte anmält något öl till ".$_SESSION['event_name'].". \n";
      }
      $msg = wordwrap($msg, 70, "\n");
      SendMail($to, $subject, $msg);
      mysqli_close($dbc);
      // Gå till event_reg_msg.php.
      $_SESSION['msg'] = $msg;
      ReDirect ('event_reg_msg.php');
    }
  }

  if (!empty($_SESSION['dt_event_id'])) {
    // Ta reda på hur många öl bryggaren har anmält till DT.
    mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
    $query = "SELECT Beers.beer_id FROM Beers ".
             "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
             "WHERE Beers.user_id = ".$_SESSION['user_id']." AND "."Beers_in_event.event_id = ".$_SESSION['dt_event_id']." ".
             "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
             "AND Beers.deleted = 0 ";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $dt_no_beers = mysqli_num_rows($result);

    // Hämta dt_cost_memb, dt_cost_nonmemb och dt_pay_before.
    $query = "SELECT dt_cost_memb, dt_cost_nonmemb, dt_pay_before FROM Events ".
             "WHERE event_id = ".$_SESSION['dt_event_id']." ".
             "AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    while ($row = mysqli_fetch_array($result)) {
      $dt_cost_memb = $row['dt_cost_memb'];
      $dt_cost_nonmemb = $row['dt_cost_nonmemb'];
      $dt_pay_before = $row['dt_pay_before'];
    }
  }

  mysqli_close($dbc);
?>




<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');
?>

  <p> <?php echo $dbg_msg;?> </p>
  <p class=error> <?php echo $err_msg;?> </p>
  
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <?php
      $msg = "";
      if ($dt_beers > 0) {
        $msg = $msg."Du har anmält $dt_beers öl till domartävlan. \n";
      }
      if ($fv_beers > 0) {
        $msg = $msg."Du har anmält $fv_beers öl till folkets val. \n";
      }
      if ($et_beers > 0) {
        $msg = $msg."Du har anmält $et_beers öl till etikettävlingen. \n";
      }
      if ( ($dt_beers == 0) && ($fv_beers == 0) && ($et_beers == 0) ) {
        $msg = "Du har inte anmält något öl till ".$_SESSION['event_name'].". \n";
      }
    ?>

  <?php
    echo '<table>';
      echo '<tr>';
        echo '<td>';
          echo '<input type="submit" value="Registrera fler öl" name="add_beer" >';
        echo '</td>';
        echo '<td></td>';
      echo '</tr>';
      if ( ($_SESSION['dt_event_open']) || ($_SESSION['fv_event_open']) || ($_SESSION['et_event_open'])) {
		    echo '<tr>';
		      echo '<td>';
		        echo '<input type="submit" value="Anmäl dig till '.$_SESSION['event_name'].'" name="register" >';
		      echo '</td>';
		      echo '<td></td>';
		    echo '</tr>';
      }
    echo '</table>';
  
    if (!empty($_SESSION['fv_event_id'])) {
      echo '<p class=head_2>Folkets val</p>';
      if ($fv_beers == 0) {
        echo '<p>Du har inga öl anmälda till folkets val.</p>';
        } else {
        echo '<p>Du har anmält '.$fv_beers.' öl till folkets val.</p>';
        if ($_SESSION['fv_event_open']) {
		      echo '<table>';
		        echo '<tr>';
		          echo '<td class=col_1>Antal bord:</td>';
		          echo '<td>';
		            echo '<select name="beer_slots" >';
		              for ($i = 0; $i <= $fv_beers; $i=$i+1) {
		                echo '<option ';
		                if ($_SESSION['beer_slots_sel'] == $i) {
		                  echo 'selected ';
		                }
		                echo 'value="';
		                echo $i;
		                echo '">';
		                if ($i == 0) {
		                  echo " ";
		                } else {
		                  echo strval(round($i/$_SESSION['beers_per_table'],1));
		                }
		                echo '</option> ';
		              }
		            echo '</select>';
		          echo '</td>';
		          echo '<td>Ange antal bord du behöver. Ett bord är '.$_SESSION['table_length'].' cm långt. Du får inte mer än 1/'.$_SESSION['beers_per_table'].' bord per anmält öl. Det finns '.FreeTables ($dbc).' lediga bord.</td>';
		        echo '</tr>';
		        echo '<tr>';
		          echo '<td class=col_1>Egen bar [cm]:</td>';
		          echo '<td>';
		            echo '<input type="number" name="bar_length" id="bar_length " min=0 value="' . $_SESSION['bar_length'] . '" />';
		          echo '</td>';
		          echo '<td>Om du inte vill ha något bord utan bygger en egen bar, så fyll i hur lång baren är. Du får inte mer än '.strval(round($_SESSION['table_length']/$_SESSION['beers_per_table'],0)).' cm per anmält öl. Det finns '.( FreeTables ($dbc) * $_SESSION['table_length']/100).' m ledigt för egna barer.</td>';
		        echo '</tr>';
		        if ($no_users > 1) {
				      echo '<tr>';
				        echo '<td class=col_1>Stå bredvid</td>';
						      echo '<td>';
						        echo '<select name="next_to" >';
					          echo '<option '; if(empty($_SESSION['next_to_sel'])){echo("selected");} echo ' value="0"></option>';
					          if ($reg_users_id != NULL)
                      for ($i = 0; $i < count($reg_users_id); $i++) {
                        echo '<option ';
                        if ($_SESSION['next_to_sel'] == $reg_users_id[$i]) {
                          echo 'selected ';
                        }
                        echo 'value="';
                        echo $reg_users_id[$i];
                        echo '">';
                        echo $reg_users_name[$i];
                        echo '</option> ';
                      }
						        echo '</select>';
						      echo '</td>';
				        echo '<td>Du kan bara välja en person att stå bredvid. Om den du måste stå bredvid inte finns med i listan så be hen att välja dig istället eller återkom när hen har anmält sina öl.</td>';
				      echo '</tr>';
		        }
		      echo '</table>';
				}
        else {
          echo '<p>Tävlingsregistreringen till folkets val är stängd.</p>';
        }
      }
    }
  ?>


<?php
  if (!empty($_SESSION['dt_event_id'])) {
    echo '<p class=head_2>Domartävlingen</p>';
    if ($dt_no_beers == 0) {
      echo '<p>Du har inga öl anmälda till domartävlingen.</p>';
      } else {
      echo '<p>Du har anmält '.$dt_no_beers.' öl till domartävlingen.</p>';
      if (($dt_cost_memb != 0) && ($dt_cost_nonmemb != 0)) {
        echo '<p>Du ska betala avgiften för dina anmälda öl till BG 5834-5489 senast '.mb_strimwidth ($dt_pay_before,0,10).'. Ange ditt medlemsnummer eller namn på inbetalningen.</p>';
        echo '<p>Om du är medlem och lämnar in dina öl på ett inlämningsställe ska du betala '.$dt_cost_memb*$dt_no_beers.' kr .</p>';
        echo '<p>Om du är medlem och postar dina öl ska du betala '.(25+$dt_cost_memb*($dt_no_beers-1)).' kr.</p>';
        echo '<p>Om du inte är medlem ska du betala '.$dt_cost_nonmemb*$dt_no_beers.' kr.</p>';
        echo '<p>Flaskorna ska märkas med etiketterna som du hittar under länken Flasketiketter högst upp.</p>';
      }
    }
  }
?>
      
<?php
  if (!empty($_SESSION['et_event_id'])) {
    echo '<p class=head_2>Etikettävlingen</p>';
    if ($et_beers == 0) {
      echo '<p>Du har inga öl anmälda till etikettävlingen.</p>';
    } else {
      echo '<p>Du har anmält '.$et_beers.' öl till etikettävlingen.</p>';
    }
  }
?>

  </form>

  <p class=head_2>Dina registrerade öl</p>
  <?php
      if ((isset($_SESSION['fv_event_closed_brewerinfo_ready']) && $_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE) || 
          (isset($_SESSION['et_event_closed_brewerinfo_ready'])  && $_SESSION['et_event_closed_brewerinfo_ready'] === TRUE) && ($fv_beers > 0 || $et_beers > 0)) {
          
          echo '<p> <a href="fv_comp_posters.php">Visa tävlingsskyltar med tävlingsnummer för dina bidrag i folkets val</a> </p> ';
          echo '<p><b> OBS: du behöver inte skriva ut tävlingskyltarna själv, arrangören ordnar med skyltar för varje tävligsbidrag. De finns här för din information.  </b></p> ';
          echo '<p>En <b>QR-kod som identifierar ditt öl</b>  i röstsystemet på tävlingsdagen finns att hämta hem för respektive tävlingsbidrag nedan (exempelvis att använda ihop med egen öletikett)</p>';
          echo '<p>Frågor om anmälningssystemet skickas till smreg&ltat&gtshbf.se.</p>';
        } 
      //Link to brewer competition statistics and comments (link is protected until datetime is past, in rating database)
      if ($_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE && $fv_beers > 0) {
        echo '<p> <a href="' .VOTE_HOST_STAT . '"><strong>Visa statistik och kommentarer för dina bidrag i folkets val</strong></a> </p> ';
      }  
  ?>

  <?php ListBeers ($_SESSION['user_id']); ?>
  


<?php
  // Sidfot.
  require_once('footer.php');
?>


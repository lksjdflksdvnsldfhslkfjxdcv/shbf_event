<?php
  // Starta session.
  require_once('startsession.php');

  require_once('funct.php');

  // Hämta meddelande från Get.
  $message = mb_strimwidth (filter_var(trim($_GET['message']),FILTER_SANITIZE_SPECIAL_CHARS),0,1000)
?>


<?php
  // Sidhuvud.
  $page_title = 'Anmälan till ';
  if (isset($_SESSION['event_name'])) $page_title .= $_SESSION['event_name'];
  
  require_once('header_nav.php');
  //Link to brewer competition statistics and comments (link is protected until datetime is past, in rating database)
  //Länk finns ochså i event_reg.php. 
  //Lade som snabbfix här för sm 2024, för att folk sak hitta enkeöt. borde flyttas till en egen sida med kontroll om bryggare har FV öl anvämld etc.
  if ($_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE) {
    require_once('const.php');
    echo '<p> <a href="' .VOTE_HOST_STAT . '"><strong>Visa statistik och kommentarer för dina bidrag i folkets val</strong></a> </p> ';
  }  

  // Skriv ut meddelande.
  echo '<p class="message">' . $message . '</p>';

  // Sidfot
  require_once('footer.php');
?>

<?php
  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');
  require_once("Mail.php");

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Nollställ ok och err_msg.
  $ok = 1;
  $err_msg = " ";

  // Anslut till databasen
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  // Hämta captcha fågor.
  $query = "SELECT * FROM Captcha ";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $no_captchas = mysqli_num_rows($result);
  if ($no_captchas > 0) {
    $i=1;
    while ($row = mysqli_fetch_array($result)) {
      $captcha_question[$i] = $row['captcha_question'];
      $captcha_answer[$i] = $row['captcha_answer'];
      $i++;
    }
  } else {
    die("set_sessions.php: No captcha questions found. ".mysqli_error($dbc)." ".$query);
  }

  if (!isset($_POST['submit'])) {
  $_SESSION['$captcha_no'] = mt_rand(1,$no_captchas);
  }

  if (isset($_POST['submit'])) {
    // Hämta värden från POST
    $user_name = FilterPost ($dbc, $_POST['user_name'], 35);
    $member_no = FilterPost ($dbc, $_POST['member_no'], 50);
    $email = FilterPost ($dbc, $_POST['email'], 50);
    $care_of = FilterPost ($dbc, $_POST['care_of'], 35);
    $street = FilterPost ($dbc, $_POST['street'], 35);
    $post_no = FilterPost ($dbc, $_POST['post_no'], 10);
    $city = FilterPost ($dbc, $_POST['city'], 35);
    $passwd = FilterPost ($dbc, $_POST['passwd'], 1000);
    $passwd2 = FilterPost ($dbc, $_POST['passwd2'], 1000);
    $name = FilterPost ($dbc, $_POST['name'], 35);
    $captcha_usr_ans = FilterPost ($dbc, $_POST['captcha_usr_ans'], 50);

    // Kontrollera att alla obligatoriska fält är ifyllda.
    if (empty($user_name)) {
      $err_msg = $err_msg."Användarnamn måste fyllas i. ";
      $ok = 0;
    }
    if (empty($name)) {
      $err_msg = $err_msg."Namn måste fyllas i. ";
      $ok = 0;
    }
    if (empty($email)) {
      $err_msg = $err_msg."e-postadress måste fyllas i. ";
      $ok = 0;
    }
    if (empty($passwd)) {
      $err_msg = $err_msg."Lösenord måste fyllas i. ";
      $ok = 0;
    }

    // Sätt member_no till noll om det inte är ifyllt.
    if ( empty($member_no) ) {
      $member_no = 0;
    }

    // Kontrollera att member_no är ett tal.
    if ( !is_numeric($member_no) ) {
      $err_msg = $err_msg." Medlemsnummret ska vara siffror. ";
      $ok = 0;
    }

    // Kontrollera att e-postadressen inte finns.
    $query = "SELECT * FROM User_data WHERE email = '$email'"." AND code = 0 AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($data) != 0) {
      $err_msg = $err_msg."e-postadressen finns redan registrerad. ";
      $ok = 0;
    }

    // Om e-postadressen fanns, men koden inte var nollad så markeras den som raderad.
    // Det här kan inträffa om användaren tidigare försökt registrera sig, men
    // sedan inte lyckats aktivera kontot.
    $query = "UPDATE User_data SET deleted = 1 ".
             "WHERE email = '$email'"." AND code != 0 AND deleted = 0";
    if (!mysqli_query($dbc, $query)) {
      die("signup.php: ".mysqli_error($dbc).$query);
    }

    // Kontrollera att användarnamnet inte är upptaget. shbf och dk är reserverade.
    $query = "SELECT * FROM Users WHERE user_name = '$user_name'"." AND deleted = 0";
    $data = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if ( (mysqli_num_rows($data) != 0) || ($user_name == "shbf") || ($user_name == "dk") ) {
      $err_msg = $err_msg."Användarnamnet finns redan registrerat. ";
      $ok = 0;
    }
    // Kontrollera att e-postformatet är giltigt.
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $err_msg = $err_msg."Ogiltigt e-postformat. ";
      $ok = 0;
    }

    // Kolla att lösenordet är tillräckligt långt.
    if ( strlen($passwd) < MIN_CHARS_PASSWD ) {
      $err_msg = $err_msg."Lösenordet måste ha minst ".MIN_CHARS_PASSWD." tecken. ";
      $ok = 0;
    }

    // Kolla att det upprepade lösenordet är rätt.
    if ( $passwd != $passwd2 ) {
      $err_msg = $err_msg."Det upprepade lösenordet är fel. ";
      $ok = 0;
    }

    // Kolla captcha.
    if ( strtolower($captcha_usr_ans) != strtolower($captcha_answer[$_SESSION['$captcha_no']]) ) {
      $err_msg = $err_msg."Fel captchasvar. Försök igen. ";
      $_SESSION['$captcha_no'] = mt_rand(1,$no_captchas);
      $ok = 0;
    }


    if ($ok) {
      // Lägg in användaren i Users. Görs för att skapa ett user_id kopplat till user_name.
      $query = "INSERT INTO Users (user_name) ".
               "VALUES ('$user_name')";
      if (mysqli_query($dbc, $query)) {
        $user_id = mysqli_insert_id($dbc);
      } else { die("signup.php: ".mysqli_error($dbc).$query); }

      // Lägg in användaren i User_data. 
      $code = 0; // Användes förut för att aktivera kontot.
      $query = "INSERT INTO User_data (user_id, name, member_no, email, care_of, street, post_no, city, passwd, code) ".
               "VALUES ('$user_id', '$name', '$member_no', '$email', '$care_of', '$street', '$post_no', '$city', SHA('$passwd'), '$code')";
      if (!mysqli_query($dbc, $query)) {
        die("signup.php: ".mysqli_error($dbc).$query);
      }

      // Visa meddelande.
      $get = '?message=' . 'Ditt konto är skapat. Du kan nu logga in.';
      ReDirect ('message.php' . $get);
    }
  }

  mysqli_close($dbc);
?>




<?php
  // Sidhuvud.
  $page_title = 'Skapa användarkonto';
  require_once('header_nav.php');
?>

  <p class="error"> <?php echo $err_msg ?> </p>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <fieldset>
      <legend>Registrering</legend>
      <label for="user_name">Användarnamn:</label>
      <input type="text" id="user_name" name="user_name" value="<?php if (!empty($user_name)) echo $user_name; ?>" maxlength="35"><br>
      <label for="name">Namn:</label>
      <input type="text" id="name" name="name" value="<?php if (!empty($name)) echo $name; ?>" maxlength="35"><br>
      <label for="member_no">Medlemsnummer (Lämnas tomt om du inte är medlem eller inte vet ditt medlemsnummer.):</label>
      <input type="number" name="member_no" id="member_no" min=0 value="<?php if (!empty($member_no)) echo $member_no; ?>" /><br>
      <label for="email">e-post:</label>
      <input type="text" id="email" name="email" value="<?php if (!empty($email)) echo $email; ?>" maxlength="50"><br>
      <label for="care_of">Ev. C/O:</label>
      <input type="text" id="care_of" name="care_of" value="<?php if (!empty($care_of)) echo $care_of; ?>" maxlength="35"><br>
      <label for="street">Gata:</label>
      <input type="text" id="street" name="street" value="<?php if (!empty($street)) echo $street; ?>" maxlength="35"><br>
      <label for="post_no">Postnummer:</label>
      <input type="text" id="post_no" name="post_no" value="<?php if (!empty($post_no)) echo $post_no; ?>" maxlength="10"><br>
      <label for="city">Ort:</label>
      <input type="text" id="city" name="city" value="<?php if (!empty($city)) echo $city; ?>" maxlength="35"><br>
      <label for="passwd">Lösenord:</label>
      <input type="password" id="passwd" name="passwd" value="<?php if (!empty($passwd)) echo $passwd; ?>" maxlength="1000"><br>
      <label for="passwd">Upprepa lösenordet:</label>
      <input type="password" id="passwd2" name="passwd2" value="<?php if (!empty($passwd2)) echo $passwd2; ?>" maxlength="1000"><br>
      <label for="captcha_usr_ans"><?php echo $captcha_question[$_SESSION['$captcha_no']] ?>:</label>
      <input type="text" id="captcha_usr_ans" name="captcha_usr_ans" value="<?php if (!empty($captcha_usr_ans)) echo $captcha_usr_ans; ?>" maxlength="80"><br>
    </fieldset>
    <input type="submit" value="Registrera dig" name="submit" />
  </form>

<?php
  // Sidfot.
  require_once('footer.php');
?>

<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sätt startvärden på alla fält i formuläret.
  $_SESSION['dtreg_user_id'] = 0;
  $_SESSION['no_lines'] = 0;

  // Gå till dt_reg.
  ReDirect ('dt_reg.php');

?>

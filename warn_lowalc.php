<?php
  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Kolla att beer_id är satt.
  if (!empty($_SESSION['beer_id'])) {
    $beer_id = $_SESSION['beer_id'];
  } else {
    die ("warn_lowalc.php: Invalid beer_id.");
  }
  
  // Tillbaka till receptet.
  if (isset($_POST['back'])) {
    ReDirect ('beer_reg_pre.php?beer_id='.$beer_id);
  }

  // Gå vidare till event_reg.
  if (isset($_POST['cont'])) {
    ReDirect ('event_reg.php');
  }

?>


<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  // Skriv ut meddelande.
  echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'">';
  echo '<table>';
  echo '<tr>';
  echo '<p colspan=2 class="message">Vill du anmäla ditt öl till Folkölsklassen?</p>';
  echo '</tr>';
  echo '<tr>';
  echo '<td> <input type="submit" name="back" value="Tillbaka" /> </td> '; 
  echo '<td> <input type="submit" name="cont" value="Nej" /> </td> '; 
  echo '</tr>';
  echo '</table>';
  echo '</form>';


  // Sidfot
  require_once('footer.php');
?>


<?php

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  
  // Hämta ölets namn.
  $query = "SELECT beer_name FROM Beer_data ".
           "WHERE beer_id = ".$_SESSION['beer_id'].
           " AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) != 1) {
    die("beer_reg_del.php: No or too many beer_name found in Beer_data.");
  }
  $row = mysqli_fetch_array($result);
  $beer_name = $row['beer_name'];
   
  if (isset($_POST['delete'])) {
    // Markera ölet beer_id som raderad i Beers.
    $query = "UPDATE Beers SET deleted = 1 ".
             "WHERE beer_id = ".$_SESSION['beer_id'];
    if (!mysqli_query($dbc, $query)) {
      die("beer_reg_del.php: ".mysqli_error($dbc).$query);
    }
    // Markera ölet beer_id som raderad i Beers_in_event.
    $query = "UPDATE Beers_in_event SET deleted = 1 ".
             "WHERE beer_id = ".$_SESSION['beer_id'];
    if (!mysqli_query($dbc, $query)) {
      die("beer_reg_del.php: ".mysqli_error($dbc).$query);
    }
    // Markera ölet beer_id som raderad i Beer_data.
    $query = "UPDATE Beer_data SET deleted = 1 ".
             "WHERE beer_id = ".$_SESSION['beer_id'];
    if (!mysqli_query($dbc, $query)) {
      die("beer_reg_del.php: ".mysqli_error($dbc).$query);
    }
    // Gå till event_reg.
    ReDirect ('event_reg.php');
  }

  if (isset($_POST['cancel'])) {
    // Gå till event_reg.
    ReDirect ('event_reg.php');
  }
  
  mysqli_close($dbc);
?>




<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

?>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <p> Vill du radera <?php echo $beer_name;?>? </p>
    <input type="submit" value="Radera" name="delete" />
    <input type="submit" value="Ångra" name="cancel" />
   </form>

<?php
  // Sidfot.
  require_once('footer.php');
?>

<?php

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

?>




<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  // Skriv ut meddelande.
  $strings = explode ("\n", $_SESSION['msg']);
  foreach ($strings as $string) {
    echo '<p>' . $string . '</p>';
  }

  // Sidfot.
  require_once('footer.php');
?>

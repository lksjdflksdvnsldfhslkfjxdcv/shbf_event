<?php

// Uppdaterar tabellen Beers_in_event.
function ModBeersEvent ($dbc, $event_id, $comp) {
  $query = "SELECT beers_in_event_id FROM Beers_in_event ".
           "WHERE beer_id = ".$_SESSION['beer_id']." AND ".
           "event_id = ".$event_id." AND ".
           "deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) == 0) {
    if ($comp) {
      // Hämta latest_label_no för flasknummer.
      $query = "LOCK TABLE Events WRITE";
      if (!mysqli_query($dbc, $query)) {
        die("funct.php ".mysqli_error($dbc).$query);
      }
      $query = "SELECT latest_label_no FROM Events ".
               "WHERE event_id = ".$event_id." AND ".
               "deleted = 0 FOR UPDATE";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      $row = mysqli_fetch_array($result);
      if (mysqli_num_rows($result) != 1) {
        mysqli_commit($dbc);
        die("funct.php "."Incorrect number of rows in Events.");
      }
      $label_no = $row['latest_label_no'] + 1;
      $query = "UPDATE Events SET latest_label_no = ".$label_no." ".
               "WHERE event_id = ".$event_id." AND ".
               "deleted = 0";
      mysqli_commit($dbc);
      if (!mysqli_query($dbc, $query)) {
        die("funct.php ".mysqli_error($dbc).$query);
      }
      $query = "UNLOCK TABLES";
      if (!mysqli_query($dbc, $query)) {
        die("funct.php ".mysqli_error($dbc).$query);
      }
      // Lägg till ölet till evenemanget i Beers_in_event.
      $query = "INSERT INTO Beers_in_event (event_id, beer_id, label_no) ".
               "VALUES ('".$event_id.
               "', '".$_SESSION['beer_id']."', '".$label_no."')";
      if (!mysqli_query($dbc, $query)) {
        die("funct.php 2 ".mysqli_error($dbc).$query);
      }
    }
  } else {
    if (!$comp) {
      // Ta bort ölet från evenemanget i Beers_in_event.
      $query = "UPDATE Beers_in_event SET deleted = 1 ".
           "WHERE beer_id = ".$_SESSION['beer_id']." AND ".
           "event_id = ".$event_id." AND ".
           "deleted = 0";
      if (!mysqli_query($dbc, $query)) {
        die("funct.php 3 ".mysqli_error($dbc).$query);
      }
    }          
  }
}


function IsBeerInEvent ($dbc, $beer_id, $event_id) {
  if (!$event_id) {
    return 0;
  }
  $query = "SELECT * FROM Beers_in_event WHERE beer_id = ".$beer_id. 
           " AND event_id = ".$event_id." AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    return 1;
  } else {
    return 0;
  }
}


// Skriver ut alla öl som användaren har registrerat till detta evenemang.
function ListBeers ($user_id) {
  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  // Hämta ölerna.
  $beers_event_id = "";
  if (!empty($_SESSION['dt_event_id'])) {
    $beers_event_id = "Beers_in_event.event_id = ".$_SESSION['dt_event_id'];
  } 
  if (empty($beers_event_id)) {
    $or = "";
  } else {$or = "OR";}
  if (!empty($_SESSION['fv_event_id'])) {
    $beers_event_id = $beers_event_id." ".$or." Beers_in_event.event_id = ".$_SESSION['fv_event_id'];
  } 
  if (empty($beers_event_id)) {
    $or = "";
  } else {$or = "OR";}
  if (!empty($_SESSION['et_event_id'])) {
    $beers_event_id = $beers_event_id." ".$or." Beers_in_event.event_id = ".$_SESSION['et_event_id'];
  }
  if (empty($beers_event_id)) {
    die("funct.php: No event id defined.");
  }
  $beers_event_id = "AND (".$beers_event_id.") ";
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $query = "SELECT Beers.beer_id, Beers_in_event.event_id, Beers_in_event.fv_competition_no, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class, ".
           "Beer_data.og, Beer_data.fg, Beer_data.bu, Beer_data.alc FROM Beers ".
           "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
           "WHERE Beers.user_id = ".$_SESSION['user_id']." ".$beers_event_id.
           "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
           "AND Beers.deleted = 0 ".
           //sortera på dem med tävlignsnummer först, för att inte missa dem vid dublettfilteringen nedan
           "ORDER BY Beers.time ASC, Beers_in_event.fv_competition_no DESC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  
  echo '<table> ';
  echo '<tr> ';
  echo '<td class=header> Ölets namn </td> ';
  echo '<td class=header> Öltyp </td> ';
  echo '<td class=header> Bryggare </td> ';
  echo '<td class=header> OG [g/l] </td> ';
  echo '<td class=header> FG [g/l] </td> ';
  echo '<td class=header> Beska [BU] </td> ';
  echo '<td class=header> Alk [vol %] </td> ';
  echo '</tr>';

  $beer_id_old =  0;
  while ($row = mysqli_fetch_array($result)) {
	  $beer_id = $row['beer_id'];
    $fv_competition_no = null;
    $et_competition_no = null;

    
    if ($row['event_id'] == $_SESSION['fv_event_id']) {
      $fv_competition_no = $row['fv_competition_no'];
      //hämta etikettävlingens tävlingsnummer
      $query2 = "SELECT fv_competition_no FROM Beers_in_event WHERE beer_id = ".$beer_id." AND event_id = ".$_SESSION['et_event_id']." AND deleted = 0";
      $result2 = mysqli_query($dbc, $query2);
      if (mysqli_num_rows($result2) == 1) {
        $row2 = mysqli_fetch_array($result2);
        $et_competition_no = $row2['fv_competition_no'];
      }
    }
    else if ($row['event_id'] == $_SESSION['et_event_id']) {
      $et_competition_no = $row['fv_competition_no'];
      //hämta folkets val tävlingsnummer
      $query2 = "SELECT fv_competition_no FROM Beers_in_event WHERE beer_id = ".$beer_id." AND event_id = ".$_SESSION['fv_event_id']." AND deleted = 0";
      $result2 = mysqli_query($dbc, $query2); 
      if (mysqli_num_rows($result2) == 1) {
        $row2 = mysqli_fetch_array($result2);
        $fv_competition_no = $row2['fv_competition_no'];
      }
    }
    
    // Ta bort dubletter.(Samma öl kan vara anmält till fler tävlingar.)
    if ($beer_id != $beer_id_old) {
	    $beer_name = $row['beer_name'];
	    $type_id = $row['main_class'].":".$row['sub_class'];
	    // Hämta bryggarnas namn.
      $query2 = "SELECT Brewers_of_beer.brewer_id, Brewers.brewer_name FROM Brewers_of_beer ".
                "INNER JOIN Brewers USING (brewer_id) ".
                "WHERE Brewers_of_beer.beer_id = '$beer_id' ".
                "AND Brewers_of_beer.deleted = 0";
      $result2 = mysqli_query($dbc, $query2);
      $i=0;
      $brewer_names = "";
      while ($row2 = mysqli_fetch_array($result2)) {
        if ($i > 0) {
          $brewer_names = $brewer_names . ", " . $row2['brewer_name'];
        } else {
          $brewer_names = $row2['brewer_name'];
        }
        $i++;
      }
      // Översätt type_id till text.
	    $type_name = "";
	    for ($i=0; $i < count($_SESSION['type_values']); $i++) {
        if ($_SESSION['type_values'][$i] == $type_id) {
          $type_name = $_SESSION['type_names'][$i];
        }
      }
	    $og = $row['og'];
	    $fg = $row['fg'];
	    $bu = $row['bu'];
	    $alc = $row['alc'];
      $dt_comment = "";
      
      if (IsBeerInEvent ($dbc, $beer_id, $_SESSION['dt_event_id'])) {
        $dt="DT";
        // Hämta received, disq och comment från Dt_event_reg.
        $query2 = "SELECT received, disq, comment FROM Dt_event_reg ".
                  "WHERE beer_id = ".$beer_id." ".
                  "AND event_id = ".$_SESSION['dt_event_id']." ".
                  "AND deleted = 0";
        $result2 = mysqli_query($dbc, $query2);
        if (mysqli_num_rows($result2) == 1) {
          $row2 = mysqli_fetch_array($result2);
          if ($row2['received']) {
            $dt_comment = $dt_comment."Mottaget. ";
          }
          if ($row2['disq']) {
            $dt_comment = $dt_comment."Diskvalificerat. ";
          }
          $dt_comment = $dt_comment.$row2['comment'];
        }
        if (mysqli_num_rows($result2) > 1) {
          die ("funct.php: No data or too many data found in Dt_event_reg.");
        }
      } else {
        $dt="";
      }
      if (IsBeerInEvent ($dbc, $beer_id, $_SESSION['fv_event_id'])) {
        $fv="FV";
      } else {
        $fv="";
      }
      if (IsBeerInEvent ($dbc, $beer_id, $_SESSION['et_event_id'])) {
        $et="ET";
      } else {
        $et="";
      }
	    // Skapa HTML-kod för listan.
  	  echo '<tr> ';
  	  echo '<td> '.$beer_name. ' </td> ';
  	  echo '<td> '.$type_name. ' </td> ';
  	  echo '<td> '.$brewer_names. ' </td> ';
  	  echo '<td> '.($og+1000). ' </td> ';
  	  echo '<td> '.($fg+1000). ' </td> ';
  	  echo '<td> '.$bu. ' </td> ';
  	  echo '<td> '.$alc. ' </td> ';
  	  echo '<td> '.$dt. ' </td> ';
  	  echo '<td> '.$fv. ' </td> ';
  	  echo '<td> '.$et. ' </td> ';
      echo '<td> <a href="beer_reg_pre.php?beer_id='.$beer_id.'">Ändra</a> </td> ';
      echo '<td> <a href="beer_reg_del_pre.php?beer_id='.$beer_id.'">Ta bort</a> </td> ';
      //om ölet är med i FV, och om registring är stängd visa funktioner för tävlingsnummer
      if ( $fv=="FV" && $_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE && $fv_competition_no != null ) {
        
        echo '<td> <a href="fv_qrgen.php?votesys_competition_id='.$_SESSION['fv_votesys_competition_id'].'&fv_beer_id='.$fv_competition_no.'">FV QR-Kod</a> </td> ';
        
      }
      //om etiketten är med i ET, och om registring är stängd visa funktioner för tävlingsnummer
      if ( $et=="ET" && ($_SESSION['et_event_closed_brewerinfo_ready'] === TRUE || $_SESSION['fv_event_closed_brewerinfo_ready'] === TRUE  )&& $et_competition_no != null ) {
        
        echo '<td> <a href="fv_qrgen.php?votesys_competition_id='.$_SESSION['et_votesys_competition_id'].'&fv_beer_id='.$et_competition_no.'">ET QR-Kod</a> </td> ';
        
      }

  	  echo '<td> '.$dt_comment. ' </td> ';
  	  echo '</tr>';
      $beer_id_old =  $beer_id;
    }
  }
  echo '</table>';
  echo '<p> DT=Domartävlan, FV=Folkets val, ET=Etikettävlingen </p> ';
  mysqli_close($dbc);
}
//mj, flyttad från fv_list_unsorted.php, lagt till as_numeric
function FvClass ($low_alc, $main_class, $sub_class,$as_numeric=false) {
  if (!empty($low_alc)) {
    $fv_class = !$as_numeric ? 'Folköl' : '8';
  } elseif ( ($main_class==1) || ($main_class==2) ) {
    $fv_class = !$as_numeric ? 'Lager och underjäst öl' : '1';
  } elseif ( (($main_class==3) && ($sub_class!='F')) ||
             (($main_class==4) && ($sub_class=='A')) ||
             (($main_class==4) && ($sub_class=='B')) ||
             (($main_class==4) && ($sub_class=='F')) ||
             (($main_class==4) && ($sub_class=='G')) ||
             (($main_class==5) && ($sub_class=='A')) ||
             (($main_class==5) && ($sub_class=='G')) ||
             (($main_class==5) && ($sub_class=='H')) ||
              ($main_class==7) ||
              ($main_class==8) ||
             (($main_class==9) && ($sub_class=='B')) ||
             (($main_class==9) && ($sub_class=='E')) ||
             (($main_class==11) && ($sub_class=='E')) ||
             (($main_class==11) && ($sub_class=='K')) ) {
    $fv_class = !$as_numeric ? 'Maltdominerad öl' : '2';
  } elseif ( (($main_class==3) && ($sub_class=='F')) ||
             (($main_class==4) && ($sub_class=='C')) ||
             (($main_class==4) && ($sub_class=='D')) ||
             (($main_class==4) && ($sub_class=='E')) ||
             (($main_class==5) && ($sub_class=='B')) ||
             (($main_class==5) && ($sub_class=='C')) ||
             (($main_class==5) && ($sub_class=='D')) ||
             (($main_class==5) && ($sub_class=='E')) ||
             (($main_class==5) && ($sub_class=='F')) ||
             (($main_class==6) && ($sub_class=='F')) ) {
    $fv_class = !$as_numeric ? 'Humledominerad öl' : '3';
  } elseif ( (($main_class==6) && ($sub_class!='F')) ||
             (($main_class==9) && ($sub_class=='A')) ||
             (($main_class==9) && ($sub_class=='C')) ||
             (($main_class==9) && ($sub_class=='D')) ||
             (($main_class==9) && ($sub_class=='F')) ||
             (($main_class==9) && ($sub_class=='G')) ||
             (($main_class==9) && ($sub_class=='H')) ||
             (($main_class==9) && ($sub_class=='I')) ||
             (($main_class==9) && ($sub_class=='J')) ||
             (($main_class==9) && ($sub_class=='K')) ||
             (($main_class==9) && ($sub_class=='L')) ) {
    $fv_class = !$as_numeric ? 'Jästdominerad öl' : '4';
  } elseif (  ($main_class==10) ||
             (($main_class==11) && ($sub_class=='C')) ) {
    $fv_class = !$as_numeric ? 'Syrligt och spontanjäst öl' : '5';
  } elseif ( (($main_class==11) && ($sub_class=='A')) ||
             (($main_class==11) && ($sub_class=='B')) ||
             (($main_class==11) && ($sub_class=='D')) ||
             (($main_class==11) && ($sub_class=='F')) ||
             (($main_class==11) && ($sub_class=='G')) ||
             (($main_class==11) && ($sub_class=='H')) ||
             (($main_class==11) && ($sub_class=='I')) ||
             (($main_class==11) && ($sub_class=='J')) ||
             (($main_class==11) && ($sub_class=='L')) ) {
    $fv_class = !$as_numeric ?  'Övriga öl' : '6';
  } elseif (  ($main_class==12) ) {
    $fv_class = !$as_numeric ? 'Cider och mjöd' : '7';
  } else {
    $fv_class = 'Okänd';
  }
  return $fv_class;
}

function FvClassNumber ($low_alc, $main_class, $sub_class) {
  if (!empty($low_alc)) {
    $fv_class = '8';
  } elseif ( ($main_class==1) || ($main_class==2) ) {
    $fv_class = 'Lager och underjäst öl';
  } elseif ( (($main_class==3) && ($sub_class!='F')) ||
             (($main_class==4) && ($sub_class=='A')) ||
             (($main_class==4) && ($sub_class=='B')) ||
             (($main_class==4) && ($sub_class=='F')) ||
             (($main_class==4) && ($sub_class=='G')) ||
             (($main_class==5) && ($sub_class=='A')) ||
             (($main_class==5) && ($sub_class=='G')) ||
             (($main_class==5) && ($sub_class=='H')) ||
              ($main_class==7) ||
              ($main_class==8) ||
             (($main_class==9) && ($sub_class=='B')) ||
             (($main_class==9) && ($sub_class=='E')) ||
             (($main_class==11) && ($sub_class=='E')) ||
             (($main_class==11) && ($sub_class=='K')) ) {
    $fv_class = 'Maltdominerad öl';
  } elseif ( (($main_class==3) && ($sub_class=='F')) ||
             (($main_class==4) && ($sub_class=='C')) ||
             (($main_class==4) && ($sub_class=='D')) ||
             (($main_class==4) && ($sub_class=='E')) ||
             (($main_class==5) && ($sub_class=='B')) ||
             (($main_class==5) && ($sub_class=='C')) ||
             (($main_class==5) && ($sub_class=='D')) ||
             (($main_class==5) && ($sub_class=='E')) ||
             (($main_class==5) && ($sub_class=='F')) ||
             (($main_class==6) && ($sub_class=='F')) ) {
    $fv_class = 'Humledominerad öl';
  } elseif ( (($main_class==6) && ($sub_class!='F')) ||
             (($main_class==9) && ($sub_class=='A')) ||
             (($main_class==9) && ($sub_class=='C')) ||
             (($main_class==9) && ($sub_class=='D')) ||
             (($main_class==9) && ($sub_class=='F')) ||
             (($main_class==9) && ($sub_class=='G')) ||
             (($main_class==9) && ($sub_class=='H')) ||
             (($main_class==9) && ($sub_class=='I')) ||
             (($main_class==9) && ($sub_class=='J')) ||
             (($main_class==9) && ($sub_class=='K')) ||
             (($main_class==9) && ($sub_class=='L'))) { //mj, lade till L
    $fv_class = 'Jästdominerad öl';
  } elseif (  ($main_class==10) ||
             (($main_class==11) && ($sub_class=='C')) ) {
    $fv_class = 'Syrligt och spontanjäst öl';
  } elseif ( (($main_class==11) && ($sub_class=='A')) ||
             (($main_class==11) && ($sub_class=='B')) ||
             (($main_class==11) && ($sub_class=='D')) ||
             (($main_class==11) && ($sub_class=='F')) ||
             (($main_class==11) && ($sub_class=='G')) ||
             (($main_class==11) && ($sub_class=='H')) ||
             (($main_class==11) && ($sub_class=='I')) ||
             (($main_class==11) && ($sub_class=='J')) ||
             (($main_class==11) && ($sub_class=='L')) ) {
    $fv_class = 'Övriga öl';
  } elseif (  ($main_class==12) ) {
    $fv_class = 'Cider och mjöd';
  } else {
    $fv_class = 'Okänd';
  }
  return $fv_class;
}


// Skicka e-post.
// Följande måste vara installerat:
// sudo apt-get install php-pear
// sudo pear install mail
// sudo pear install Net_SMTP
// sudo pear install Auth_SASL
// sudo pear install mail_mime
function SendMail ($recipient, $subject, $message, $attachment = '') {
  
  if (USE_BUILTIN_SENDMAIL == true){
    SendMail2 ($recipient, $subject, $message, $attachment);
    return;
  }
  $from = USER;
  $host = SERVER;
//  $port = '465';
//  $port = '587';
  $port = PORT;
  $username = USER;
  $password = PASSWORD;

  $headers = array ('From' => $from, 'To' => $recipient, 'Subject' => $subject);
  
  if ($attachment != '') {
    $crlf = "\n";
    $mime = new Mail_mime($crlf);
    $mime->setTXTBody($message);
    $mime->addAttachment($attachment, 'application/pdf');
    $body = $mime->get();
    $headers = $mime->headers($headers);
  } else {
    $body = $message;
  }

  $smtp = Mail::factory('smtp',
    array ('host' => $host,
           'socket_options' => array('ssl' =>
             array('verify_peer_name' => false)),
      'port' => $port,
      'auth' => true,
      'username' => $username,
      'password' => $password));

  $mail = $smtp->send($recipient, $headers, $body);

  if (PEAR::isError($mail)) {
    die("SendMail: " . $mail->getMessage() . "");  
  }

}
//använder standard php core mail function (kan supporta $attachment om behov finns)
function SendMail2 ($recipient, $subject, $message, $attachment = '') {
  $from = USER;
  
  $headers = array ('From' => $from, 'Reply-To' => $from, 'X-Mailer' => 'PHP/' . phpversion());
  
  //print_r($headers);
  mail($recipient, $subject, $message, $headers);
 

}








// Flytta rader i matris.
// $array är en vektor med radnummer för ölerna.
function MoveRows ($from, $to, $noRows, $array) {
  $size = count ($array);
  // Felkontroll.
  if ( ($from > $to) && ($to + $noRows > $from) ) {
    return $array;
  }
  // Flytta raderna.
  for ($i=0; $i<$size; $i++) {
    if ( ($array[$i] >= $from) && ($array[$i] < $from + $noRows) ) {
      $array[$i] = $to + $array[$i] - $from - $noRows;
    } else {
      if ($from > $to) {
        if ( ($array[$i] >= $to) && ($array[$i] < $from) ) {
          $array[$i] = $array[$i] + $noRows;
        }
      } else {
        if ( ($array[$i] >= $from) && ($array[$i] < $to + $noRows) ) {
          $array[$i] = $array[$i] - $noRows;
        }
      }
    }
  }
  return $array;
}

// Hämta datum och tid.
function GetCurrDate() {
  $date = date_create('now', timezone_open('Europe/Stockholm')); //NULL deprecated
  return date_format($date, 'Y-m-d H:i:s');
}

// Kontrollera att anmälan till evenemanget är öppen.
function DateChk () {
  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("funct.php: "."Connection failed: " . mysqli_connect_error());
  }
  // Sätt startvärden.
  $open = 1;
  $msg = "";
  $_SESSION['dt_event_open'] = FALSE;
  $_SESSION['fv_event_open'] = FALSE;
  $_SESSION['et_event_open'] = FALSE;
  // Kolla domartävlan.
  if (!empty($_SESSION['dt_event_id'])) {
    $query = "SELECT time_open, time_close FROM Events ".
             "WHERE event_id = ".$_SESSION['dt_event_id']." ".
             "AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    while ($row = mysqli_fetch_array($result)) {
      if ( (GetCurrDate('Y-m-d H:i:s') > $row['time_open']) &&
           (GetCurrDate('Y-m-d H:i:s') < $row['time_close']) ) {
        $_SESSION['dt_event_open'] = TRUE;
        $_SESSION['dt_event_msg'] = "Domartävlingen";
      } elseif ( (GetCurrDate('Y-m-d H:i:s') < $row['time_open']) ) {
        $_SESSION['dt_event_open'] = FALSE;
        $_SESSION['dt_event_msg'] = "Anmälan till domartävlingen öppnar ".mb_strimwidth ($row['time_open'],0,10).".";
      } elseif ( (GetCurrDate('Y-m-d H:i:s') > $row['time_close']) ) {
        $_SESSION['dt_event_open'] = FALSE;
        $_SESSION['dt_event_msg'] = "Anmälan till domartävlingen är stängd.";
      }
    }
  } 
  // Kolla folkets val.
  if (!empty($_SESSION['fv_event_id'])) {
    $fv_nr_visible_to_brewers = TRUE;

    $query = "SELECT time_open, time_close, fv_nr_edit_locked, fv_nr_time_visible_to_brewers FROM Events ".
             "WHERE event_id = ".$_SESSION['fv_event_id']." ".
             "AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    while ($row = mysqli_fetch_array($result)) {
        if ( (GetCurrDate('Y-m-d H:i:s') > $row['time_open']) &&
            (GetCurrDate('Y-m-d H:i:s') < $row['time_close']) ) {
          $_SESSION['fv_event_open'] = TRUE;
          $_SESSION['fv_event_msg'] = "Folkets val";
        } elseif ( (GetCurrDate('Y-m-d H:i:s') < $row['time_open']) ) {
          $_SESSION['fv_event_open'] = FALSE;
          $_SESSION['fv_event_msg'] = "Anmälan till folkets val öppnar ".mb_strimwidth ($row['time_open'],0,10).".";
        } elseif ( (GetCurrDate('Y-m-d H:i:s') > $row['time_close']) ) {
          $_SESSION['fv_event_open'] = FALSE;
          $_SESSION['fv_event_msg'] = "Anmälan till folkets val är stängd.";
        }
        $_SESSION['fv_nr_edit_locked'] = FALSE;
        if (!empty($row['fv_nr_edit_locked']) && $row['fv_nr_edit_locked'] == 1) {
            $_SESSION['fv_nr_edit_locked'] = TRUE;
        } else{
            $_SESSION['fv_nr_edit_locked'] = FALSE;
        }
        $_SESSION['fv_nr_time_visible_to_brewers'] = "";
        if (!empty($row['fv_nr_time_visible_to_brewers'])) {
            if ( (GetCurrDate('Y-m-d H:i:s') > $row['fv_nr_time_visible_to_brewers'])) {
              $fv_nr_visible_to_brewers = TRUE;
            }
            else{
              $fv_nr_visible_to_brewers = FALSE;
              
            }
            $_SESSION['fv_nr_time_visible_to_brewers'] =  $row['fv_nr_time_visible_to_brewers'];
        }
      }

    
    //om folkets val registrering är stängd, 
    //kolla om det finns fv_competition_no (tävingsnummer) registrerade på några öl
    //om det finns, sätt $_SESSION['fv_event_closed_brewerinfo_ready'] = TRUE

    $_SESSION['fv_event_closed_brewerinfo_ready'] = FALSE;
    if (!$_SESSION['fv_event_open'] && $fv_nr_visible_to_brewers && $_SESSION['fv_votesys_competition_id'] != 0) {
      $query = "SELECT fv_competition_no FROM Beers_in_event ".
               "WHERE event_id = ".$_SESSION['fv_event_id']." ".
               "AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        if (!empty($row['fv_competition_no'])) {
          $_SESSION['fv_event_closed_brewerinfo_ready'] = TRUE;
          break;
        }
      }
    }
  } 
  // Kolla etikettävlingen.
  if (!empty($_SESSION['et_event_id'])) {
    $et_nr_visible_to_brewers = TRUE;
    $query = "SELECT time_open, time_close, fv_nr_edit_locked, fv_nr_time_visible_to_brewers  FROM Events ".
             "WHERE event_id = ".$_SESSION['et_event_id']." ".
             "AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    while ($row = mysqli_fetch_array($result)) {
      if ( (GetCurrDate('Y-m-d H:i:s') > $row['time_open']) &&
           (GetCurrDate('Y-m-d H:i:s') < $row['time_close']) ) {
        $_SESSION['et_event_open'] = TRUE;
        $_SESSION['et_event_msg'] = "Etikettävlingen";
      } elseif ( (GetCurrDate('Y-m-d H:i:s') < $row['time_open']) ) {
        $_SESSION['et_event_open'] = FALSE;
        $_SESSION['et_event_msg'] = "Anmälan till etikettävlingen öppnar ".mb_strimwidth ($row['time_open'],0,10).".";
      } elseif ( (GetCurrDate('Y-m-d H:i:s') > $row['time_close']) ) {
        $_SESSION['et_event_open'] = FALSE;
        $_SESSION['et_event_msg'] = "Anmälan till etikettävlingen är stängd.";
      }
      $_SESSION['et_nr_edit_locked'] = FALSE;
      if (!empty($row['fv_nr_edit_locked']) && $row['fv_nr_edit_locked'] == 1) {
          $_SESSION['et_nr_edit_locked'] = TRUE;
      } else{
          $_SESSION['et_nr_edit_locked'] = FALSE;
      }
      }
      $_SESSION['et_nr_time_visible_to_brewers'] = "";
      if (!empty($row['fv_nr_time_visible_to_brewers'])) {
          if ( (GetCurrDate('Y-m-d H:i:s') > $row['fv_nr_time_visible_to_brewers'])) {
            $et_nr_visible_to_brewers = TRUE;
           
          }
          else{
            $et_nr_visible_to_brewers = FALSE;
          }
          $_SESSION['et_nr_time_visible_to_brewers'] =  $row['fv_nr_time_visible_to_brewers'];
      }
    

  
    //om etiketttävling registrering är stängd, 
    //kolla om det finns fv_competition_no (tävingsnummer) registrerade på några öl
    //om det finns, sätt $_SESSION['fv_event_closed_brewerinfo_ready'] = TRUE

    $_SESSION['et_event_closed_brewerinfo_ready'] = FALSE;
    if (!$_SESSION['et_event_open'] && $et_nr_visible_to_brewers && $_SESSION['et_votesys_competition_id'] != 0) {
      $query = "SELECT fv_competition_no FROM Beers_in_event ".
              "WHERE event_id = ".$_SESSION['et_event_id']." ".
              "AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      while ($row = mysqli_fetch_array($result)) {
        if (!empty($row['fv_competition_no'])) {
          $_SESSION['et_event_closed_brewerinfo_ready'] = TRUE;
          break;
        }
      }
    }
  }
  
}

function AccessChk ($file) {
  DateChk ();

  switch ($file) {
    case 'activate':
    case 'login':
    case 'signup':
      // Kontrollera att användaren inte redan är inloggad.
      if ( isset($_SESSION['user_id']) ) {
        // Visa meddelande.
        $message = "Du är redan inloggad. ";
        ReDirect ('message.php?message=' . $message);
      }
      break;
    case 'beer_reg':
    case 'beer_reg_pre':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att något event är valt.
      if ( empty($_SESSION['dt_event_id']) && 
           empty($_SESSION['fv_event_id']) && 
           empty($_SESSION['et_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att något evenemang är öppet.
      if (!($_SESSION['dt_event_open'] == TRUE) && 
          !($_SESSION['fv_event_open'] == TRUE) && 
          !($_SESSION['et_event_open'] == TRUE) ) {
        // Visa meddelande.
        $message = "";
        if (!empty($_SESSION['dt_event_msg'])) {
          $message = $message.$_SESSION['dt_event_msg']." ";
          }
        if (!empty($_SESSION['fv_event_msg'])) {
          $message = $message.$_SESSION['fv_event_msg']." ";
          }
        if (!empty($_SESSION['et_event_msg'])) {
          $message = $message.$_SESSION['et_event_msg']." ";
          }
        ReDirect ('message.php?message=' . $message);
      }
      break;
    case 'beer_reg_del_pre':
    case 'beer_reg_del':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att något event är valt.
      if ( empty($_SESSION['dt_event_id']) && 
           empty($_SESSION['fv_event_id']) && 
           empty($_SESSION['et_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att något evenemang är öppet.
      if (!($_SESSION['dt_event_open'] == TRUE) && 
          !($_SESSION['fv_event_open'] == TRUE) && 
          !($_SESSION['et_event_open'] == TRUE) ) {
        // Visa meddelande.
        $message = "";
        if (!empty($_SESSION['dt_event_msg'])) {
          $message = $message.$_SESSION['dt_event_msg']." ";
          }
        if (!empty($_SESSION['fv_event_msg'])) {
          $message = $message.$_SESSION['fv_event_msg']." ";
          }
        if (!empty($_SESSION['et_event_msg'])) {
          $message = $message.$_SESSION['et_event_msg']." ";
          }
        ReDirect ('message.php?message=' . $message);
      }
			// Kontrollera att anmälningarna är öppna.
      $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
      if (!$dbc) {
        die("Connection failed: " . mysqli_connect_error());
      }
      $_SESSION['comp_dt'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['dt_event_id']);
      $_SESSION['comp_fv'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['fv_event_id']);
      $_SESSION['comp_et'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['et_event_id']);
			if ( ($_SESSION['comp_dt'] && !$_SESSION['dt_event_open']) || 
			     ($_SESSION['comp_fv'] && !$_SESSION['fv_event_open']) || 
			     ($_SESSION['comp_et'] && !$_SESSION['et_event_open']) ) {
			  // Visa meddelande.
			  $message = "Ölet kan inte längre raderas.";
			  ReDirect ('message.php?message=' . $message);
			} 
      break;
    case 'event_reg':
    case 'event_reg_msg':
    case 'warn_lowalc':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att något event är valt.
      if ( empty($_SESSION['dt_event_id']) && 
           empty($_SESSION['fv_event_id']) && 
           empty($_SESSION['et_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      break;

    case 'labels':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera att en domartävlan är vald.
      if ( empty($_SESSION['dt_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollerar inte att domartävlan är öppen ifall bryggaren vill
      // skriva ut etiketterna efter stängning.
      break;

    case 'fv_list':
    case 'fv_list_unsorted':
    case 'fv_comp_posters':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // Kontrollera att en folkets val tävlan är vald.
      if ( empty($_SESSION['fv_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera användarens behörighet.
      if ( !(($_SESSION['adm_lev'] == 'LIM') || ($_SESSION['adm_lev'] == 'FULL')) ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollerar inte att folkets val är öppet eftersom
      // listan tas ut efter stängning.
      break;

    case 'dt_list':
    case 'dt_list_rec':
    case 'dt_reg':
    case 'dt_reg_pre':
    case 'address':
    case 'id':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // Kontrollera att en domartävlan är vald.
      if ( empty($_SESSION['dt_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera användarens behörighet.
      if ( !(($_SESSION['adm_lev'] == 'LIM') || ($_SESSION['adm_lev'] == 'FULL')) ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollerar inte att domartävlan är öppen eftersom
      // listan tas ut efter stängning.
      break;

    case 'et_list':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // Kontrollera att en etikettävling är vald.
      if ( empty($_SESSION['et_event_id']) ) {
        // Visa meddelande.
        $message = "Inget evenemang valt. Klicka på länken för anmälan till evenemanget.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera användarens behörighet.
      if ( !(($_SESSION['adm_lev'] == 'LIM') || ($_SESSION['adm_lev'] == 'FULL')) ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollerar inte att etikettävlingen är öppen eftersom
      // listan tas ut efter stängning.
      break;

    case 'user_addr':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // Kontrollera användarens behörighet.
      if ( !(($_SESSION['adm_lev'] == 'LIM') || ($_SESSION['adm_lev'] == 'FULL')) ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      break;

    case 'mail_brewers':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollera användarens behörighet.
      if ( !($_SESSION['adm_lev'] == 'FULL') ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      // Kontrollerar inte att evenemanget är öppet eftersom
      // utskick ska kunna göras även efter stängning.
      break;

    case 'logout':
    case 'recipe':
    case 'recipes_all':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      break;

    case 'update':
    case 'update_pre':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // shbf har inte behörighet till denna sida.
      if ( $_SESSION['user_name'] == "shbf" ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      break;

    case 'test':
      // Gå till login-sidan om användaren inte är inloggad.
      if ( !isset($_SESSION['user_id']) ) {
        ReDirect ('login.php');
      }
      // Kontrollera användarens behörighet.
      if ( !($_SESSION['adm_lev'] == 'FULL') ) {
        // Visa meddelande.
        $message = "Du har inte behörighet till denna sida.";
        ReDirect ('message.php?message=' . $message);
      }
      break;

    default:
      die ("Access rights not defined for $file.php");
  }
}

function FilterPost ($dbc, $string, $maxLen) {
  return mb_strimwidth (mysqli_real_escape_string($dbc, filter_var(trim($string),FILTER_SANITIZE_SPECIAL_CHARS)),0,$maxLen);
}

// Totalt antal bokade bord i lokalen.
function TotTables () {
  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (empty($_SESSION['fv_event_id'])) {
    die("funct.php: No fv_event id defined.");
  }
  // Hämta user_id för alla som registrerat öl till detta evenemang. Även dom som glömt att anmäla sig.
  // Öl anmälda av raderade användare tas inte med.
  $query = "SELECT DISTINCT Users.user_id FROM Users ".
           "INNER JOIN Beers USING (user_id) INNER JOIN Beers_in_event USING (beer_id) ".
           "INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$_SESSION['fv_event_id']." ".
           "AND Beers.deleted = 0 AND Beers_in_event.deleted = 0 ".
           "AND Users.deleted = 0 AND User_data.deleted = 0 ";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $tot_no_beer_slots = 0;
  $tot_bar_length = 0;
  while ($row = mysqli_fetch_array($result)) {
    // Hämta barlängder och ölplatser.
    $query3 = "SELECT beer_slots, bar_length FROM Fv_event_reg ".
              "WHERE Fv_event_reg.fv_event_id = ".$_SESSION['fv_event_id']." ".
              "AND Fv_event_reg.user_id = ".$row['user_id']." AND Fv_event_reg.deleted = 0 ";
    $result3 = mysqli_query($dbc, $query3) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result3) == 0) {
      $beer_slots = 0;
      $bar_length = 0;
    } else if (mysqli_num_rows($result3) == 1) {
      $row3 = mysqli_fetch_array($result3);
      $beer_slots = $row3['beer_slots'];
      $bar_length = $row3['bar_length'];
    } else {
      die ("funct.php: Too many data found in Fv_event_reg.");
    }
    // Kontrollera att antal bord eller barlängd är ifyllt.
    if ( empty($beer_slots) &&
         empty($bar_length) ) {
      // Om inte, antag att bryggaren vill ha så många bord som möjligt.
      mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
      $query2 = "SELECT Beers.beer_id FROM Beers ".
                "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
                "WHERE Beers.user_id = ".$row['user_id']." AND "."Beers_in_event.event_id = ".$_SESSION['fv_event_id']." ".
                "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
                "AND Beers.deleted = 0 ";
      $result2 = mysqli_query($dbc, $query2);
      $tot_no_beer_slots = $tot_no_beer_slots + mysqli_num_rows($result2);
    } else {
	    $tot_no_beer_slots = $tot_no_beer_slots + $beer_slots;
	    $tot_bar_length = $tot_bar_length + $bar_length;
    }
  }
  // Räkna om till hur många bord som är bokade i lokalen totalt.
  return round(($tot_no_beer_slots / $_SESSION['beers_per_table']) + ($tot_bar_length / $_SESSION['table_length']), 0);
}

function FreeTables ($dbc) {
  $noFreeTables = $_SESSION['max_no_tables'] - TotTables ();
  if ($noFreeTables > 0) {
    return $noFreeTables;
  } else {
    return 0;
  }
}

// Antal anmälda öl till evenemanget.
function NoRegBeers ($event_id) {
  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  
   $query = "SELECT * FROM Beers ".
           "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
           "INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$event_id." ".
           "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
           "AND Users.deleted = 0 AND User_data.deleted = 0 ".
           "AND Beers.deleted = 0 ";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  return mysqli_num_rows($result);
}

function ReDirect ($to) {
// ReDirect ('xxx.php');
// Om relativa sökvägar inte funkar så se raden nedan.
  //$url = 'https://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/' . $to;
  //echo $url;
  //$url = $to;
  //mj, detta fungerar bättre?
  echo "<script type='text/javascript'>location.href='" . $to . "';</script>"; 
  //header('Location: ' . $url, true, 303); // Statuskod 303: Ingen cache och POST-värden skickas inte med. 
  //die ("Page ".$to." not found.");
}

function CurrUrl() {
  $pageURL = 'http';
  if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
  $pageURL .= "://";
  if ($_SERVER["SERVER_PORT"] != "80") {
    $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
  } else {
    $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
  }
  return $pageURL;
}

?>

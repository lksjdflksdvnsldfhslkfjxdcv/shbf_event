<?php // -*- coding: utf-8 -*-
//skapa qr-kod med url, för fv_beer_id
///kräver extension=gd i php.ini

require_once('const.php');
$QRhostData = VOTE_HOST  . "?cid=" . $_GET['votesys_competition_id'] . '&bid=' . $_GET['fv_beer_id'];


require_once('./php-qrcode/qrcode.php');
//qr-kod options set to scalefactor 10 and high error correction
$options = array('s' => 'qr-h','sf' => 10);

$generator = new QRCode($QRhostData,$options );
$generator->output_image();
//vikigt att denna fil inte har avslutande php-tag, gäller även const.php
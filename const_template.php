<?php
  // Döp om till const.php.

  // Konstanter för databasen.
  define('DB_HOST', 'host');
  define('DB_DOMAIN', 'domain');
  define('DB_USER', 'db_user');
  define('DB_PASSWORD', 'user_pwd');
  define('DB_NAME', 'db_name');
  //Konstanter för databasen för ratingsystemet
  define('DB_RATE_HOST', 'localhost');
  define('DB_RATE_USER', 'db_user');
  define('DB_RATE_PASSWORD', 'user_pwd');
  define('DB_RATE_NAME', 'db_name');
  

  // Konstanter för e-post.
  define('USE_BUILTIN_SENDMAIL', false);    
  define('SERVER', 'smtp_server');
  define('USER', 'noreply@domain');
  define('PASSWORD', 'noreply_pwd');  
  define('PORT', '587');  

  // Övriga konstanter.
  define('MIN_CHARS_PASSWD', '8');

  //url till röstsystem (som genererade qr-koder skall peka på)
  define('VOTE_HOST', 'http://localhost:65194/rate/index.html');
  define('VOTE_HOST_STAT', 'http://localhost:65194/rate/user/index.html'); 

  //vikigt att denna fil inte har avslutande php-tag

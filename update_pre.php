<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Hämta användaruppgifter.
  $query = "SELECT * FROM User_data WHERE user_id = ".$_SESSION['user_id']." AND code = 0 AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) == 0) {
    die("update.php: No data found in database.");
  }
  $row = mysqli_fetch_array($result);
  $_SESSION['member_no'] = $row['member_no'];
  $_SESSION['email'] = $row['email'];
  $_SESSION['care_of'] = $row['care_of'];
  $_SESSION['street'] = $row['street'];
  $_SESSION['post_no'] = $row['post_no'];
  $_SESSION['city'] = $row['city'];
  $_SESSION['passwd'] = ""; // Hämtar inte lösenordet.
  $_SESSION['name'] = $row['name'];

  // Gå till update.
  ReDirect ('update.php');

?>

<?php


// Skapar pdf med ID-nummer till flasknumren.
function id($event_id, $filter_main) {
  require_once('fpdf.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  // Skapa pdf-sida.
  $pdf = new FPDF('P','mm','A4');
  $pdf->SetLeftMargin(28);
  $pdf->SetRightMargin(28);
  $pdf->AddPage();
  $pdf->SetFont('Courier','B',12);

  $query = "SELECT Beers.user_id, Beers_in_event.label_no, Beer_data.main_class, Beer_data.sub_class ".
           "FROM Beers_in_event ".
           "INNER JOIN Beers USING (beer_id) INNER JOIN Beer_data USING (beer_id) ".
           "INNER JOIN Users USING (user_id) INNER JOIN User_data USING (user_id) ".
           "WHERE Beers_in_event.event_id = ".$event_id." AND Beers_in_event.deleted = 0 ".
           "AND Users.deleted = 0 AND User_data.deleted = 0 ".
           "AND Beer_data.deleted = 0 AND Beers.deleted = 0 ".
           $filter_main." ".
           "ORDER BY Beers.user_id, Beer_data.main_class, Beer_data.sub_class, Beers_in_event.label_no ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $prev_user_id = "";
  $id = "";
  $text = "";
  $labelLength = 12;
  $lineHeight = 5;
  while ($row = mysqli_fetch_array($result)) {
    $main_class = $row['main_class'];
    $sub_class = $row['sub_class'];
    $label_no = $row['label_no'];
    $user_id = $row['user_id'];

    if ($user_id != $prev_user_id) {
      $ypos = $pdf->GetY();
      if ( ($ypos + (ceil(strlen($text)/($labelLength*5)))*$lineHeight ) > 250 ) {
        $pdf->AddPage();
      }
      $pdf->SetFont('Courier','B',14);
      $pdf->write(7, $id);
      $pdf->SetFont('Courier','',$labelLength);
      $pdf->write($lineHeight, $text);
      $text = "";
      $prev_user_id = $user_id;
      $id = "\n\n".$user_id."\n";
    }
    $label = $row['main_class'].":".$row['sub_class']."-".$label_no;
    while (strlen($label) < 12) {
      $label = $label." ";
    }
    $text = $text.$label;
  }
  $ypos = $pdf->GetY();
  if ( ($ypos + (ceil(strlen($text)/($labelLength*5)))*$lineHeight ) > 250 ) {
    $pdf->AddPage();
  }
  $pdf->SetFont('Courier','B',14);
  $pdf->write(7, $id);
  $pdf->SetFont('Courier','',$labelLength);
  $pdf->write($lineHeight, $text);
  $text = "";

  ob_start();
  $pdf->Output();
  ob_end_flush();
}

  // Skapar pdf med ID-etiketter för deltagare i domartävlingen.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sidhuvud.
  $page_title = 'Adressetiketter';

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');
  
  // Hämta filtrering på huvudklass från Get.
  if (isset($_GET['filter_main'])) {
    $text = $_GET['filter_main'];
    $filter_main = mb_strimwidth (filter_var(trim($_GET['filter_main']),FILTER_SANITIZE_SPECIAL_CHARS),0,100);
    if ($text != "Alla") {
      if ( preg_match_all('/(\d+)/', $filter_main, $matches) ) {
        $filter_main = "AND (Beer_data.main_class=".$matches[1][0];
        $classes = $matches[1][0];
        for ($i=1; $i<sizeof($matches[1]); $i++) {
          $filter_main = $filter_main." OR Beer_data.main_class=".$matches[1][$i];
          $classes = $classes.", ".$matches[1][$i];
        };
        $filter_main = $filter_main.") ";
      } else {
        $filter_main = "";
      }
    } else {
      $filter_main = "";
    }
  }

  // Skapa pdf med ID-etiketter för deltagare i domartävlingen.
  id($_SESSION['dt_event_id'], $filter_main);

?>


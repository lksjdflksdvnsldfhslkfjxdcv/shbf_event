<?php
// Skriver ut adressen till användaren.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("user_addr.php: "."Connection failed: " . mysqli_connect_error());
  }
  
  // Läs user_id från get.
  if (isset($_GET['user_id']) && is_numeric($_GET['user_id'])) {
    $user_id = FilterPost ($dbc, $_GET['user_id'], 100);
  } else {
    die ("user_addr.php: Missing user_id.");
  }

  // Sidhuvud.
  $page_title = 'Adress';
  require_once('header_nav.php');

  // Hämta namn till user_id.
  $query = "SELECT name, care_of, street, post_no, city FROM User_data ".
         "WHERE user_id = ".$user_id.
         " AND deleted = 0";
  $result = mysqli_query($dbc, $query);
  if (mysqli_num_rows($result) == 1) {
  $row = mysqli_fetch_array($result);
    $name = $row['name'];
    $care_of = $row['care_of'];
    $street = $row['street'];
    $post_no = $row['post_no'];
    $city = $row['city'];
  } else {
  die ("user_addr.php: No data or too many data found in User_data.");
  }



  // Skapa HTML-kod för listan.
  echo '<p> '.$name. ' </p> ';
  if (!empty($care_of)) {echo '<p> '.$care_of. ' </p> ';};
  echo '<p> '.$street. ' </p> ';
  echo '<p> '.$post_no.' '.$city. ' </p> ';
  mysqli_close($dbc);
?>

<?php
  // Sidfot.
  require_once('footer.php');
?>

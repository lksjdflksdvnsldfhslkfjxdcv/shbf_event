<?php
// Skickar ett meddelande till alla bryggarna.

 // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("mail_brewers.php: "."Connection failed: " . mysqli_connect_error());
  }
  
  // Nollställ variabler.
  $dbg_msg = "";

  // Hämta id för alla öl anmälda till detta evenemang.
  $beers_event_id = "";
  if (!empty($_SESSION['dt_event_id'])) {
    $beers_event_id = "Beers_in_event.event_id = ".$_SESSION['dt_event_id'];
  } 
  if (empty($beers_event_id)) {
    $or = "";
  } else {$or = "OR";}
  if (!empty($_SESSION['fv_event_id'])) {
    $beers_event_id = $beers_event_id." ".$or." Beers_in_event.event_id = ".$_SESSION['fv_event_id'];
  } 
  if (empty($beers_event_id)) {
    $or = "";
  } else {$or = "OR";}
  if (!empty($_SESSION['et_event_id'])) {
    $beers_event_id = $beers_event_id." ".$or." Beers_in_event.event_id = ".$_SESSION['et_event_id'];
  }
  if (empty($beers_event_id)) {
    die("mail_brewers.php: No event id defined.");
  }
  // Hämta alla beer_id för det här evenemanget.
  $query = "SELECT Beers.beer_id FROM Beers ".
           "INNER JOIN Beers_in_event USING (beer_id) ".
           "WHERE (".$beers_event_id.") ".
           "AND Beers_in_event.deleted = 0 AND Beers.deleted = 0 ".
           "ORDER BY Beers.beer_id ASC";
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $old_beer_id = 0;
  $i = 0;
  unset ($email);
  while ($row = mysqli_fetch_array($result)) {
    $beer_id = $row['beer_id'];
    if ($old_beer_id != $beer_id) {
      $old_beer_id = $beer_id;
      // Hämta alla e-postadresser för det här ölet.
      $query2 = "SELECT Brewers_of_beer.brewer_id, Brewers.brewer_email FROM Brewers_of_beer ".
                "INNER JOIN Brewers USING (brewer_id) ".
                "WHERE Brewers_of_beer.beer_id = $beer_id ".
                "AND Brewers_of_beer.deleted = 0 AND Brewers.deleted = 0";
      $result2 = mysqli_query($dbc, $query2) or die (mysqli_error($dbc));
      while ($row2 = mysqli_fetch_array($result2)) {
        $email[$i] = $row2['brewer_email'];
        $i++;
      }
    }
  }
  sort ($email);
  $old_to = "";
  foreach ($email as $to) {
    if ( ($old_to != $to) AND !empty($to) AND filter_var($to, FILTER_VALIDATE_EMAIL) ) {
      // Skicka e-post till användaren.
      $old_to = $to;
      $msg = "";
      $subject = "Registrering ".$_SESSION['event_name'];
      $msg = wordwrap($msg, 70, "\n");
//       SendMail($to, $subject, $msg);
$dbg_msg = $dbg_msg.$to."<br>";
    }
  }
?>

<?php
  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

?>
  <p> <?php echo $dbg_msg;?> </p>
  <p class="error"> <?php echo $err_msg;?> </p>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
  <table>
      <td class=header colspan="2">E-post till alla bryggare</td>
    <tr>
      <td>Ämne</td>
      <td> <input type="text" name="subject" id="subject" value="<?php echo $_SESSION['subject'] ?>" maxlength="150" /> </td>
    </tr>
    <tr>
      <td colspan="2"> <textarea name="message" rows="15" cols="70" maxlength="1000"><?php echo $_SESSION['message'];?></textarea> </td>
    </tr>
    <tr>
      <td> <input type="submit" value="Skicka" name="send" /> </td>
    </tr>
  </table>
  </form>


<?php
  // Sidfot.
  require_once('footer.php');
?>

<?php
// Registrera flaskinlämnade öl till domartävlingen.

 // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("dt_list.php: "."Connection failed: " . mysqli_connect_error());
  }
  
  // Spara data till Dt_event_reg.
  if (isset($_POST['save'])) {
    // Hämta värden från POST.
    for ($i=0; $i<$_SESSION['no_lines']; $i++) {
      if ($_POST['dt_received'][$i] == '1') {
        $_SESSION['dt_received'][$i] = 1;
      } else {
        $_SESSION['dt_received'][$i] = 0;
      }
      if ($_POST['dt_disq'][$i] == '1') {
        $_SESSION['dt_disq'][$i] = 1;
      } else {
        $_SESSION['dt_disq'][$i] = 0;
      }
      $_SESSION['dt_comment'][$i] = FilterPost ($dbc, $_POST['dt_comment'][$i], 200);
    }
    for ($i=0; $i<$_SESSION['no_lines']; $i++) {
      // Kontrollera om något data ändrats
      // Hämta received, disq och comment från Dt_event_reg.
      $query = "SELECT received, disq, comment FROM Dt_event_reg ".
                "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
                "AND event_id = ".$_SESSION['dt_event_id']." ".
                "AND deleted = 0";
      $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
      $add = 0;
      $del = 0;
      if (mysqli_num_rows($result) == 0) {
        $add = 1;
      }
      if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        if ($_SESSION['dt_received'][$i] != $row['received'] ||
            $_SESSION['dt_disq'][$i] != $row['disq'] ||
            $_SESSION['dt_comment'][$i] != $row['comment']) {
          $add = 1;
          $del = 1;
        }
      }
      if (mysqli_num_rows($result) > 1) {
        die ("dt_list.php: No data or too many data found in Dt_event_reg.");
      }
      if ($del) {
        // Markera nuvarande rad i Dt_event_reg som raderad.
        $query = "UPDATE Dt_event_reg SET deleted = 1 ".
                 "WHERE beer_id = ".$_SESSION['dt_beer_id'][$i]." ".
                 "AND event_id = ".$_SESSION['dt_event_id']." ".
                 "AND deleted = 0";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
      if ($add) {
        // Lägg till ny rad i Dt_event_reg.
        $query = "INSERT INTO Dt_event_reg (beer_id, event_id, received, disq, comment) ".
                 "VALUES ('".$_SESSION['dt_beer_id'][$i]."', '".$_SESSION['dt_event_id']."', '".$_SESSION['dt_received'][$i].
                 "', '".$_SESSION['dt_disq'][$i]."', '".$_SESSION['dt_comment'][$i]."')";
        if (!mysqli_query($dbc, $query)) {
          die("dt_list.php.Dt_event_reg ".mysqli_error($dbc).$query);
        }
      }
    }
  ReDirect ('dt_reg_pre.php');
  }

  // Hämta bryggarens user_id med hjälp av label_no.
  if (isset($_POST['fetch']) && !empty($_POST['bottle_id'])) {
    if (empty($_SESSION['dt_event_id'])) {
      die("dt_list.php: No dt_event id defined.");
    }
    $bottle_id = FilterPost ($dbc, $_POST['bottle_id'], 10);
    $bottle_id = mb_strtoupper($bottle_id);
    if ( preg_match('/([0-9]+)/', $bottle_id, $matches) ) {
      $label_no = $matches[1];
    }
    if ( preg_match('/([0-9]+)[^A-Z]*([A-Z]+)[^0-9]*([0-9]+)/', $bottle_id, $matches) ) {
      $label_no = $matches[3];
    }
    $query = "SELECT Beers.user_id FROM Beers_in_event ".
             "INNER JOIN Beers USING (beer_id) ".
             "WHERE Beers_in_event.event_id = ".$_SESSION['dt_event_id']." ".
             "AND Beers_in_event.label_no = ".$label_no." ".
             "AND Beers_in_event.deleted = 0 ";
             "AND Beers.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $_SESSION['no_lines'] = mysqli_num_rows($result);
    if ($_SESSION['no_lines']== 0) {
      $_SESSION['dtreg_user_id'] = 0;
    }
    if (mysqli_num_rows($result) == 1) {
      $row = mysqli_fetch_array($result);
      $_SESSION['dtreg_user_id'] = $row['user_id'];
    }
    if (mysqli_num_rows($result) > 1) {
      die ("dt_reg.php: Too many data found in User_data.");
    }
  }

  // Lista bryggarens anmälda brygder.
  if ($_SESSION['no_lines'] > 0) {
    mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
    $query = "SELECT Beers.beer_id, Beers.user_id, Beer_data.beer_name, Beer_data.main_class, Beer_data.sub_class, ".
             "Beers_in_event.label_no FROM Beers ".
             "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
             "WHERE Beers_in_event.event_id = ".$_SESSION['dt_event_id']." ".
             "AND Beers_in_event.deleted = 0 AND Beer_data.deleted = 0 ".
             "AND Beers.user_id = ". $_SESSION['dtreg_user_id'] . " AND Beers.deleted = 0 ".
             "ORDER BY Beer_data.main_class, Beer_data.sub_class, Beers_in_event.label_no ASC";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $line = 0;
    while ($row = mysqli_fetch_array($result)) {
	    $_SESSION['dt_beer_id'][$line] = $row['beer_id'];
      $beer_names[$line] = $row['beer_name'];
      $user_ids[$line] = $row['user_id'];
      $type_ids[$line] = $row['main_class'].":".$row['sub_class'];
      $label_nos[$line] = $row['label_no'];
      // Hämta namn till user_id.
      $query2 = "SELECT name FROM User_data ".
                "WHERE user_id = ".$user_ids[$line]." ".
                "AND deleted = 0";
      $result2 = mysqli_query($dbc, $query2);
      if (mysqli_num_rows($result2) == 1) {
        $row2 = mysqli_fetch_array($result2);
        $user_names[$line] = $row2['name'];
      } else {
        die ("dt_reg.php: No data or too many data found in User_data.");
      }
      // Översätt type_id till text.
      $type_names[$line] = "";
      for ($i=0; $i < count($_SESSION['type_values']); $i++) {
        if ($_SESSION['type_values'][$i] == $type_ids[$line]) {
          $type_names[$line] = $_SESSION['type_names'][$i];
        }
      }
      // Hämta received, disq och comment från Dt_event_reg.
      $query2 = "SELECT received, disq, comment FROM Dt_event_reg ".
                "WHERE beer_id = ".$_SESSION['dt_beer_id'][$line]." ".
                "AND event_id = ".$_SESSION['dt_event_id']." ".
                "AND deleted = 0";
      $result2 = mysqli_query($dbc, $query2);
      if (mysqli_num_rows($result2) == 0) {
        $dt_received[$line] = 0;
        $dt_disq[$line] = 0;
        $dt_comment[$line] = "";
      }
      if (mysqli_num_rows($result2) == 1) {
        $row2 = mysqli_fetch_array($result2);
        $dt_received[$line] = $row2['received'];
        $dt_disq[$line] = $row2['disq'];
        $dt_comment[$line] = $row2['comment'];
      }
      if (mysqli_num_rows($result2) > 1) {
        die ("dt_list.php: No data or too many data found in Dt_event_reg.");
      }
      $line++;
    }
    $_SESSION['no_lines'] = $line;
    mysqli_close($dbc);
  }




  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  echo '<form method="post" action="'.$_SERVER['PHP_SELF'].'"> ';

  echo '<table> ';
  echo '<tr> ';
  echo '<td class=col_1> Flasknr: </td> ';
  echo '<td> <input type="text" name="bottle_id" id="bottle_id" value="" maxlength="10" />';
  echo '<input type="submit" value="hämta" name="fetch" /> </td>'; 
  echo '</tr>';
  echo '</table>';


  echo '<input type="submit" value="Spara" name="save" /> '; 
  echo '<table> ';
  echo '<tr> ';
  echo '<td class=header> Öltyp </td> ';
  echo '<td class=header> Flasknr </td> ';
  echo '<td class=header> Ölets namn </td> ';
  echo '<td class=header> Anmält av </td> ';
  echo '<td class=header> ID </td> ';
  echo '<td class=header> Mottaget </td> ';
  echo '<td class=header> Diskad </td> ';
  echo '<td class=header> Kommentar </td> ';
  echo '</tr>';

  for ($i=0; $i<$_SESSION['no_lines']; $i++) {
    // Skapa HTML-kod för listan.
	  echo '<tr> ';
	  echo '<td> '.$type_names[$i]. ' </td> ';
	  echo '<td> '.$type_ids[$i].'-'.$label_nos[$i]. ' </td> ';
	  echo '<td> '.$beer_names[$i]. ' </td> ';
	  echo '<td> '.$user_names[$i]. ' </td> ';
	  echo '<td> '.$user_ids[$i]. ' </td> ';
    echo '<td> <input type="checkbox" name="dt_received['.$i.']" value="1" '; 
    if($dt_received[$i]){echo("checked");} 
    echo '> </td>';
    echo '<td> <input type="checkbox" name="dt_disq['.$i.']" value="1" '; 
    if($dt_disq[$i]){echo("checked");} 
    echo '> </td>';
    echo '<td> <input type="text" name="dt_comment['.$i.']" id="dt_comment['.$i.'] " value="' . $dt_comment[$i] . '" maxlength="200" /> </td>';
    echo '<td> <a href="recipe.php?beer_id='.$_SESSION['dt_beer_id'][$i].'">Recept</a> </td> ';
    echo '</tr>';
    $line++;
  }
  echo '</table>';
  echo '</form> ';
?>

<?php
  // Sidfot.
  require_once('footer.php');
?>

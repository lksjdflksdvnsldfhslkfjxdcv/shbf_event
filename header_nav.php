
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<!--
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <form accept-charset="utf-8">
-->

<?php

  $page_title = "Eventregistrering";

  if (isset($_SESSION['event_name'])) {
    $page_title .= ' - ' .  $_SESSION['event_name'];
  }
  if (isset($_SESSION['name'])) {
    $page_title .= ' - (<small><i>' .  $_SESSION['name'] . ')</i></small>';
  }
  echo '<title>' . $page_title . '</title>';
  // header('Content-type: text/html; charset=utf-8');
?>

  <link rel="stylesheet" href="style.css" />
</head>


<body>

<header>
<?php
  echo '<h3>' . $page_title . '</h3>';
?>


<?php

  // Skapa navigeringslisten.
  echo '<hr />';
  echo '<table>';
  echo '<tr>';
  echo '<td class=nav><a class=nav href="index.php">Evenemang</a></td>';
  if (!isset($_SESSION['user_id'])) {
    echo '<td class=nav><a class=nav href="login.php">Logga in</a></td>';
    echo '<td class=nav><a class=nav href="signup.php">Registrera dig</a></td>';
  }
  if (isset($_SESSION['user_id'])) {
    echo '<td class=nav><a class=nav href="event_reg.php">Evenemangsregistrering</a></td>';
    echo '<td class=nav><a class=nav href="beer_reg_pre.php">Ölregistrering</a></td>';
    if (!empty($_SESSION['dt_event_id'])) {
      echo '<td class=nav><a class=nav href="labels.php">Flasketiketter</a></td>';
    }
  }
  if ( !empty($_SESSION['adm_lev']) ) {
    if (($_SESSION['adm_lev'] == 'LIM') || ($_SESSION['adm_lev'] == 'FULL')) {
      if (!empty($_SESSION['fv_event_id'])) {
        echo '<td class=nav><a class=nav href="fv_list_unsorted.php">Anmälda till folkets val</a></td>';
        echo '<td class=nav><a class=nav href="fv_comp_posters.php">Nummerskyltar folkets val</a></td>';
      }
      if (!empty($_SESSION['et_event_id'])) {
        echo '<td class=nav><a class=nav href="et_list.php">Anmälda till etikettävlingen</a></td>';
        echo '<td class=nav><a class=nav href="fv_comp_posters.php?event_id=' . $_SESSION['et_event_id'] . '">Nummerskyltar Etiketttävling</a></td>';
      }
      if (!empty($_SESSION['dt_event_id'])) {
        echo '<td class=nav><a class=nav href="dt_list.php">Anmälda till domartävlingen</a></td>';
      }
      if (!empty($_SESSION['dt_event_id'])) {
        echo '<td class=nav><a class=nav href="dt_reg_pre.php">Flaskinlämningen till domartävlingen</a></td>';
      }
    }
  }
//  if ($_SESSION['adm_lev'] == 'FULL') {
//    echo '<td class=nav><a class=nav href="test.php">Test</a></td>';
//  }
  if (isset($_SESSION['user_id'])) {
    echo '<td class=nav><a class=nav href="update_pre.php">Uppdatera användaruppgifter</a></td>';
    echo '<td class=nav><a class=nav href="logout.php">Logga ut</a></td>';
  }
  echo '</tr>';
  echo '</table>';
  echo '<hr />';

echo '</header>';
?>

<?php


// Skapar pdf med öletiketer med fpdf.
function labels($event_id) {
  require_once('fpdf.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  // Skapa pdf-sida.
  $pdf = new FPDF('P','mm','A4');
  $pdf->AddPage();
  $pdf->SetFont('Courier','B',24);

  $border = 1;
  $cell_width = 60;
  $cell_hight = 30;
  $start_xpos = 15;
  $start_ypos = 10;
  $xpos = $start_xpos;
  $ypos = $start_ypos;
  $first_page = 1;

  $query = "SELECT Beers.beer_id, Beers_in_event.label_no, Beer_data.main_class, Beer_data.sub_class, ".
           "Beer_data.beer_name FROM Beers ".
           "INNER JOIN Beer_data USING (beer_id) INNER JOIN Beers_in_event USING (beer_id) ".
           "WHERE Beers.user_id = ".$_SESSION['user_id']." "."AND Beers.deleted = 0 ".
           "AND Beers_in_event.event_id = ".$event_id." AND Beers_in_event.deleted = 0 ".
           "AND Beer_data.deleted = 0 ";
//die ($query);
  mysqli_query($dbc, "SET SESSION SQL_BIG_SELECTS=1") or die("no big select support in database.");
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $beer_no = 0;
  while ($row = mysqli_fetch_array($result)) {
    $beer_name = $row['beer_name'];
    $beer_type = $row['main_class'].":".$row['sub_class'];
    $label_no = $row['label_no'];
    if (($beer_no == 0) && !$first_page) {
      $pdf->AddPage();
    }
    $first_page = 0;
    $xpos = $start_xpos;
    $ypos = $start_ypos + ($beer_no * 90);
    $pdf->SetXY($xpos,$ypos);
    $pdf->Cell($cell_width*3,$cell_hight,mb_strimwidth($beer_name,0,30),0,0,'C');
    $ypos = $ypos + 20;
    $pdf->SetXY($xpos,$ypos);
    for ($i=0; $i<5; $i++) {
      if ($xpos > $cell_width * 3) {
        $xpos = $start_xpos;
        $ypos = $ypos + $cell_hight;
      }
      $pdf->SetXY($xpos, $ypos);
      $pdf->Cell($cell_width,$cell_hight,$beer_type."-".$label_no,$border,0,'C');
      $xpos = $xpos + $cell_width;
    }
    $beer_no = $beer_no + 1;
    if ($beer_no == 3) {
      $beer_no = 0;
    }
  }
  ob_start();
  $pdf->Output();
  ob_end_flush();
}

  // Skapar pdf med flaskettiketter till domartävlingen.

  // Starta session.
  require_once('startsession.php');

  // Inkludera konstanter och funktioner.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sidhuvud.
  $page_title = 'Ölregistrering';

  // Skapa flasketiketer för domartävlingen.
  labels($_SESSION['dt_event_id']);

?>


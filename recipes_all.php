<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Sidhuvud.
  $page_title = 'Anmälan till '.$_SESSION['event_name'];
  require_once('header_nav.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (isset($_GET['event_id']) && is_numeric($_GET['event_id'])) {
    $event_id = FilterPost ($dbc, $_GET['event_id'], 100);
  } else {
    die ("recipes_all.php: Missing event_id.");
  }

  // Hämta beer_id.
  $query = "SELECT Beers_in_event.beer_id FROM Beers_in_event ".
           "INNER JOIN Beer_data USING (beer_id) ".
           "WHERE event_id = ".$event_id.
           " AND Beers_in_event.deleted = 0".
           " AND Beer_data.deleted = 0".
           " ORDER BY Beer_data.main_class, Beer_data.sub_class ASC";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  if (mysqli_num_rows($result) > 0) {
    $i=0;
    while ($row = mysqli_fetch_array($result)) {
      $beer_id[$i] = $row['beer_id'];
      $i++;
    }
    $no_beers = mysqli_num_rows($result);
  } else {
    $no_beers = 0;
  }


  for ($j=0; $j<$no_beers; $j++) {
    // Hämta öldata.
    $query = "SELECT * FROM Beer_data WHERE beer_id = ".$beer_id[$j]." AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($result);
    $beer_name = $row['beer_name'];
    $type_name = $row['type_name'];
    $volume = $row['volume'];
    $og = $row['og'];
    $fg = $row['fg'];
    $bu = $row['bu'];
    $alc = $row['alc'];
    $mashing = $row['mashing'];
    $ferment = $row['ferment'];
    $water = $row['water'];
    $comment = $row['comment'];

    // Hämta maltinfo.
    $query = "SELECT Malts_in_beer.malts_in_beer_id, Malts_in_beer.malt_id, ".
             "Malts_in_beer.malt_weight, Malts_in_beer.malt_comment, Malts.malt_name FROM Malts_in_beer ".
             "INNER JOIN Malts USING (malt_id) ".
             "WHERE Malts_in_beer.beer_id = ".$beer_id[$j].
             " AND Malts_in_beer.deleted = 0".
             " ORDER BY Malts_in_beer.malt_weight DESC";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $i=0;
      while ($row = mysqli_fetch_array($result)) {
        $malts_name[$i] = $row['malt_name'];
        $malts_weight[$i] = $row['malt_weight'];
        $malts_comment[$i] = $row['malt_comment'];
        $i++;
      }
      $no_malts = mysqli_num_rows($result);
    } else {
      $no_malts = 0;
    }

    // Hämta humleinfo.
    $query = "SELECT Hops_in_beer.hops_weight, Hops.hops_name, ".
              "Hops.hops_form_id, Hops.hops_alpha, Hops_in_beer.hops_boil_time, ".
              "Hops_in_beer.hops_comment, Hops_forms.hops_form_name ".
              "FROM Hops_in_beer ".
              "INNER JOIN Hops USING (hops_id) ".
              "INNER JOIN Hops_forms USING (hops_form_id) ".
              "WHERE Hops_in_beer.beer_id = ".$beer_id[$j].
              "  AND Hops_in_beer.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $i=0;
      while ($row = mysqli_fetch_array($result)) {
        $hops_name[$i] = $row['hops_name'];
        $hops_form_name[$i] = $row['hops_form_name'];
        $hops_weight[$i] = $row['hops_weight'];
        $hops_alpha[$i] = $row['hops_alpha'];
        $hops_boil_time[$i] = $row['hops_boil_time'];
        $hops_comment[$i] = $row['hops_comment'];
        $i++;
      }
      $no_hops = mysqli_num_rows($result);
    } else {
      $no_hops = 0;
    }

    // Hämta övrigtinfo.
    $query = "SELECT Others_in_beer.others_weight, Others.others_name, ".
             "Others.others_stage_id, Others_in_beer.others_comment, ".
             "Others_stages.others_stage_name ".
             "FROM Others_in_beer ".
             "INNER JOIN Others USING (others_id) ".
             "INNER JOIN Others_stages USING (others_stage_id) ".
             "WHERE Others_in_beer.beer_id = ".$beer_id[$j].
             "  AND Others_in_beer.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $i=0;
      while ($row = mysqli_fetch_array($result)) {
        $others_name[$i] = $row['others_name'];
        $others_stage_name[$i] = $row['others_stage_name'];
        $others_weight[$i] = $row['others_weight'];
        $others_comment[$i] = $row['others_comment'];
        $i++;
      }
      $no_others = mysqli_num_rows($result);
    } else {
      $no_others = 0;
    }
	
    echo '<table>';
    echo '<tr>';
    echo '<td class=head_1 colspan="5">'.$type_name.'</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">'.$beer_name.'</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td> Volym: '.$volume.' l </td>';
    echo '<td> OG: '.$og.' &#176Ö </td>';
    echo '<td> FG: '.$fg.' &#176Ö </td>';
    echo '<td> Beska: '.$bu.' BU </td>';
    echo '<td> Alkoholhalt: '.$alc.' % </td>';
    echo '</tr>';
    echo '</table>';
    echo '<table>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">Kokning</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_3>Humle / krydda</td>';
    echo '<td class=head_3>Form</td>';
    echo '<td class=head_3_r>Vikt [g]</td>';
    echo '<td class=head_3_r>Alfasyra [%]</td>';
    echo '<td class=head_3_r>Koktid [min]</td>';
    echo '<td class=head_3>Kommentar</td>';
    echo '</tr>';
    for ($i=0; $i<$no_hops; $i++) {
      echo '<tr>';
      echo '<td>'.$hops_name[$i].'</td>';
      echo '<td>'.$hops_form_name[$i].'</td>';
      echo '<td class=norm_r>'.$hops_weight[$i].'</td>';
      echo '<td class=norm_r>'.$hops_alpha[$i].'</td>';
      echo '<td class=norm_r>'.$hops_boil_time[$i].'</td>';
      echo '<td>'.$hops_comment[$i].'</td>';
      echo '</tr>';
    }
    echo '</table>';
    echo '<table>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">Mäskning</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_3>Malt / extraktgivare</td>';
    echo '<td class=head_3_r>Vikt [g]</td>';
    echo '<td class=head_3>Kommentar</td>';
    echo '</tr>';
    for ($i=0; $i<$no_malts; $i++) {
      echo '<tr>';
      echo '<td>'.$malts_name[$i].'</td>';
      echo '<td class=norm_r>'.$malts_weight[$i].'</td>';
      echo '<td>'.$malts_comment[$i].'</td>';
      echo '</tr>';
    }
    echo '<tr>';
    echo '<td colspan="3"> <textarea name="mashing" rows="5" cols="40" readonly>'.$mashing.'</textarea> </td>';
    echo '</tr>';
    echo '</table>';
    echo '<table>';
    echo '<tr>';
    echo '<td class=head_2 colspan="4">Övriga ingredienser</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_3>Ingrediens</td>';
    echo '<td class=head_3>Tillsatt vid</td>';
    echo '<td class=head_3_r>Vikt [g]</td>';
    echo '<td class=head_3>Kommentar</td>';
    echo '</tr>';
    for ($i=0; $i<$no_others; $i++) {
      echo '<tr>';
      echo '<td>'.$others_name[$i].'</td>';
      echo '<td>'.$others_stage_name[$i].'</td>';
      echo '<td class=norm_r>'.$others_weight[$i].'</td>';
      echo '<td>'.$others_comment[$i].'</td>';
      echo '</tr>';
    }
    echo '</table>';
    echo '<table>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">Jäsning</td>';
    echo '</tr>';
    echo '<td colspan="5"> <textarea name="ferment" rows="5" cols="40" readonly>'.$ferment.'</textarea> </td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">Vattenbehandling</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td colspan="5"> <textarea name="water" rows="5" cols="40" readonly>'.$water.'</textarea> </td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td class=head_2 colspan="5">Kommentar</td>';
    echo '</tr>';
    echo '<tr>';
    echo '<td colspan="5"> <textarea name="comment" rows="5" cols="40" readonly>'.$comment.'</textarea> </td>';
    echo '</tr>';
    echo '</table>';
  }

  // Sidfot.
  require_once('footer.php');
?>


<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if (isset($_GET['beer_id']) && is_numeric($_GET['beer_id'])) {
    $_SESSION['beer_id'] = FilterPost ($dbc, $_GET['beer_id'], 100);
    $_SESSION['update'] = 1;
  } else {
    $_SESSION['beer_id'] = NULL;
    $_SESSION['update'] = 0;
  }

  // Om beer_id är satt, kontrollera att det är användarens öl.
    if ($_SESSION['update']) {
    $query = "SELECT * FROM Beers WHERE beer_id = ".$_SESSION['beer_id']." AND deleted = 0 ".
                     "AND user_id = ".$_SESSION['user_id'];
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) == 0) {
      die ("beer_reg_pre.php: Wrong user_id.");
    }
  }

  // Sätt startvärden på alla fält i formuläret.
  // alla fält sparas i en $_SESSION-variabel.
  $_SESSION['autofocus_sel'] = "";
  $_SESSION['comp_dt'] = 0;
  $_SESSION['comp_fv'] = 0;
  $_SESSION['comp_et'] = 0;
  $_SESSION['beer_name'] = "";
  $_SESSION['main_class'] = "";
  $_SESSION['sub_class'] = "";
  $_SESSION['beer_type'] = "";
  $_SESSION['low_alc_sel'] = 0;
  $_SESSION['volume'] = NULL;
  $_SESSION['og'] = NULL;
  $_SESSION['fg'] = NULL;
  $_SESSION['bu'] = NULL;
  $_SESSION['alc'] = NULL;
  $query = "SELECT name, email FROM User_data WHERE user_id = ".$_SESSION['user_id']." AND deleted = 0";
  $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
  $row = mysqli_fetch_array($result);
  $_SESSION['brewers_name'][0] = $row['name'];
  $_SESSION['brewers_email'][0] = $row['email'];
  $_SESSION['no_brewers'] = 1;

  $_SESSION['hops_name'][0] = "";
  $_SESSION['hops_form_id_sel'][0] = 0;
  $_SESSION['hops_weight'][0] = NULL;
  $_SESSION['hops_alpha'][0] = NULL;
  $_SESSION['hops_boil_time'][0] = NULL;
  $_SESSION['hops_comment'][0] = "";
  $_SESSION['hops_name'][1] = "";
  $_SESSION['hops_form_id_sel'][1] = 0;
  $_SESSION['hops_weight'][1] = NULL;
  $_SESSION['hops_alpha'][1] = NULL;
  $_SESSION['hops_boil_time'][1] = NULL;
  $_SESSION['hops_comment'][1] = "";
  $_SESSION['hops_name'][2] = "";
  $_SESSION['hops_form_id_sel'][2] = 0;
  $_SESSION['hops_weight'][2] = NULL;
  $_SESSION['hops_alpha'][2] = NULL;
  $_SESSION['hops_boil_time'][2] = NULL;
  $_SESSION['hops_comment'][2] = "";
  $_SESSION['hops_name'][3] = "";
  $_SESSION['hops_form_id_sel'][3] = 0;
  $_SESSION['hops_weight'][3] = NULL;
  $_SESSION['hops_alpha'][3] = NULL;
  $_SESSION['hops_boil_time'][3] = NULL;
  $_SESSION['hops_comment'][3] = "";
  $_SESSION['no_hops'] = 4;

  $_SESSION['malts_name'][0] = "";
  $_SESSION['malts_weight'][0] = NULL;
  $_SESSION['malts_comment'][0] = NULL;
  $_SESSION['malts_name'][1] = "";
  $_SESSION['malts_weight'][1] = NULL;
  $_SESSION['malts_comment'][1] = NULL;
  $_SESSION['malts_name'][2] = "";
  $_SESSION['malts_weight'][2] = NULL;
  $_SESSION['malts_comment'][2] = NULL;
  $_SESSION['malts_name'][3] = "";
  $_SESSION['malts_weight'][3] = NULL;
  $_SESSION['malts_comment'][3] = NULL;
  $_SESSION['no_malts'] = 4;

  $_SESSION['others_name'][0] = "";
  $_SESSION['others_stage_id_sel'][0] = 0;
  $_SESSION['others_weight'][0] = NULL;
  $_SESSION['others_comment'][0] = "";
  $_SESSION['others_name'][1] = "";
  $_SESSION['others_stage_id_sel'][1] = 0;
  $_SESSION['others_weight'][1] = NULL;
  $_SESSION['others_comment'][1] = "";
  $_SESSION['others_name'][2] = "";
  $_SESSION['others_stage_id_sel'][2] = 0;
  $_SESSION['others_weight'][2] = NULL;
  $_SESSION['others_comment'][2] = "";
  $_SESSION['others_name'][3] = "";
  $_SESSION['others_stage_id_sel'][3] = 0;
  $_SESSION['others_weight'][3] = NULL;
  $_SESSION['others_comment'][3] = "";
  $_SESSION['no_others'] = 4;

  $_SESSION['mashing'] = "Proteinrast xx &#176C, xx min&#13;&#10;Försockringsrast xx &#176C, xx min";
  $_SESSION['ferment'] = "Jästsort: xxx&#13;&#10;Jäsning xx &#176C, xx dagar";
  $_SESSION['water'] = "Vatten från xx-stad&#13;&#10;Tillsatser:&#13;&#10;xx g någonting";
  $_SESSION['comment'] = "Här ska t.ex. öltyp anges för underklasserna Övriga klassiska.";

  // Om vi ska uppdatera ett öl så behöver vi hämta
  // dom gamla uppgifterna till formuläret.
  if ($_SESSION['update']) {
    // Hämta tävlingsinfo.
    $_SESSION['comp_dt'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['dt_event_id']);
    $_SESSION['comp_fv'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['fv_event_id']);
    $_SESSION['comp_et'] = IsBeerInEvent ($dbc, $_SESSION['beer_id'], $_SESSION['et_event_id']);

    // Hämta öldata.
    $query = "SELECT * FROM Beer_data WHERE beer_id = ".$_SESSION['beer_id']." AND deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    $row = mysqli_fetch_array($result);
    $_SESSION['beer_name'] = $row['beer_name'];
    $_SESSION['main_class'] = $row['main_class'];
    $_SESSION['sub_class'] = $row['sub_class'];
    $_SESSION['beer_type'] = $_SESSION['main_class'].":".$_SESSION['sub_class'];
    $_SESSION['low_alc_sel'] = $row['low_alc'];
    $_SESSION['volume'] = $row['volume'];
    $_SESSION['og'] = $row['og'];
    $_SESSION['fg'] = $row['fg'];
    $_SESSION['bu'] = $row['bu'];
    $_SESSION['alc'] = $row['alc'];
    $_SESSION['mashing'] = $row['mashing'];
    $_SESSION['ferment'] = $row['ferment'];
    $_SESSION['water'] = $row['water'];
    $_SESSION['comment'] = $row['comment'];

    // Hämta bryggarnas namn och e-post.
    $query2 = "SELECT Brewers.brewer_name, Brewers.brewer_email FROM Brewers_of_beer ".
              "INNER JOIN Brewers USING (brewer_id) ".
              "WHERE Brewers_of_beer.beer_id = ".$_SESSION['beer_id'].
              "  AND Brewers.user_id = ".$_SESSION['user_id'].
              "  AND Brewers_of_beer.deleted = 0";
    $result2 = mysqli_query($dbc, $query2);
    if (mysqli_num_rows($result2) > 0) {
      $i=0;
      while ($row2 = mysqli_fetch_array($result2)) {
        $_SESSION['brewers_name'][$i] = $row2['brewer_name'];
        $_SESSION['brewers_email'][$i] = $row2['brewer_email'];
        $i++;
      }
      $_SESSION['no_brewers'] = mysqli_num_rows($result2);
    }

    // Hämta maltinfo.
    $query2 = "SELECT Malts_in_beer.malts_in_beer_id, Malts_in_beer.malt_id, ".
              "Malts_in_beer.malt_weight, Malts_in_beer.malt_comment, Malts.malt_name FROM Malts_in_beer ".
              "INNER JOIN Malts USING (malt_id) ".
              "WHERE Malts_in_beer.beer_id = ".$_SESSION['beer_id'].
              "  AND Malts.user_id = ".$_SESSION['user_id'].
              "  AND Malts_in_beer.deleted = 0".
              " ORDER BY Malts_in_beer.malt_weight DESC";
    $result2 = mysqli_query($dbc, $query2);
    if (mysqli_num_rows($result2) > 0) {
      $i=0;
      while ($row2 = mysqli_fetch_array($result2)) {
//        $_SESSION['malts_list_id'][$i] = $row2['malts_in_beer_id'];
//        $_SESSION['malts_id'][$i] = $row2['malt_id'];
        $_SESSION['malts_name'][$i] = $row2['malt_name'];
        $_SESSION['malts_weight'][$i] = $row2['malt_weight'];
        $_SESSION['malts_comment'][$i] = $row2['malt_comment'];
        $i++;
      }
      $_SESSION['no_malts'] = mysqli_num_rows($result2);
    }

    // Hämta humleinfo.
    $query = "SELECT Hops_in_beer.hops_weight, Hops.hops_name, ".
              "Hops.hops_form_id, Hops.hops_alpha, Hops_in_beer.hops_boil_time, ".
              "Hops_in_beer.hops_comment ".
              "FROM Hops_in_beer ".
              "INNER JOIN Hops USING (hops_id) ".
              "INNER JOIN Hops_forms USING (hops_form_id) ".
              "WHERE Hops_in_beer.beer_id = ".$_SESSION['beer_id'].
              "  AND Hops.user_id = ".$_SESSION['user_id'].
              "  AND Hops_in_beer.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $i=0;
      while ($row = mysqli_fetch_array($result)) {
        $_SESSION['hops_name'][$i] = $row['hops_name'];
        $_SESSION['hops_form_id_sel'][$i] = $row['hops_form_id'];
        $_SESSION['hops_weight'][$i] = $row['hops_weight'];
        $_SESSION['hops_alpha'][$i] = $row['hops_alpha'];
        $_SESSION['hops_boil_time'][$i] = $row['hops_boil_time'];
        $_SESSION['hops_comment'][$i] = $row['hops_comment'];
        $i++;
      }
      $_SESSION['no_hops'] = mysqli_num_rows($result);
    }

    // Hämta övrigtinfo.
    $query = "SELECT Others_in_beer.others_weight, Others.others_name, ".
             "Others.others_stage_id, Others_in_beer.others_comment ".
             "FROM Others_in_beer ".
             "INNER JOIN Others USING (others_id) ".
             "INNER JOIN Others_stages USING (others_stage_id) ".
             "WHERE Others_in_beer.beer_id = ".$_SESSION['beer_id'].
             "  AND Others.user_id = ".$_SESSION['user_id'].
             "  AND Others_in_beer.deleted = 0";
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) > 0) {
      $i=0;
      while ($row = mysqli_fetch_array($result)) {
        $_SESSION['others_name'][$i] = $row['others_name'];
        $_SESSION['others_stage_id_sel'][$i] = $row['others_stage_id'];
        $_SESSION['others_weight'][$i] = $row['others_weight'];
        $_SESSION['others_comment'][$i] = $row['others_comment'];
        $i++;
      }
      $_SESSION['no_others'] = mysqli_num_rows($result);
    }
	}

  // Gå till beer_reg.
  ReDirect ('beer_reg.php');

?>

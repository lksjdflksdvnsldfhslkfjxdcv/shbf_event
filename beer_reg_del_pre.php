<?php

  // Starta session.
  require_once('startsession.php');
  
  // Inkludera konstanter.
  require_once('const.php');
  require_once('funct.php');

  // Anslut till databasen.
  $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if (!$dbc) {
    die("Connection failed: " . mysqli_connect_error());
  }

  // Hämta beer_id.
  if (isset($_GET['beer_id']) && is_numeric($_GET['beer_id'])) {
    $_SESSION['beer_id'] = FilterPost ($dbc, $_GET['beer_id'], 100);
    $delete = 1;
  } else {
    $_SESSION['beer_id'] = NULL;
    $delete = 0;
  }

  // Om beer_id är satt, kontrollera att det är användarens öl.
  if ($delete) {
    $query = "SELECT * FROM Beers WHERE beer_id = ".$_SESSION['beer_id']." AND deleted = 0 ".
                     "AND user_id = ".$_SESSION['user_id'];
    $result = mysqli_query($dbc, $query) or die (mysqli_error($dbc));
    if (mysqli_num_rows($result) == 0) {
      die ("beer_reg_del.php: Wrong user_id.");
    }
  }

  // Kontrollera behörighet.
  AccessChk (basename(__FILE__, ".php"));

  if ($delete) {
    // Gå till beer_reg_del om vi ska radera.
    ReDirect ('beer_reg_del.php');
  } else {
    // Annars gå till beer_reg.
    ReDirect ('event_reg.php');
  }

?>

-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2018 at 09:30 AM
-- Server version: 10.1.35-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shbfhems_eventreg`
--
CREATE DATABASE IF NOT EXISTS `shbfhems_eventreg` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `shbfhems_eventreg`;

-- --------------------------------------------------------

--
-- Table structure for table `Beers`
--

DROP TABLE IF EXISTS `Beers`;
CREATE TABLE `Beers` (
  `beer_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci COMMENT='Varje inmatat recept får här ett eget id.';

--
-- Indexes for table `Beers`
--
ALTER TABLE `Beers`
  ADD PRIMARY KEY (`beer_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for table `Beers`
--
ALTER TABLE `Beers`
  MODIFY `beer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Beers_in_event`
--
DROP TABLE IF EXISTS `Beers_in_event`;
CREATE TABLE `Beers_in_event` (
  `beers_in_event_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `label_no` int(11) NOT NULL DEFAULT 0,
  `fv_competition_no` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;



--
-- Indexes for table `Beers_in_event`
--
ALTER TABLE `Beers_in_event`
  ADD PRIMARY KEY (`beers_in_event_id`);

--
-- AUTO_INCREMENT for table `Beers_in_event`
--
ALTER TABLE `Beers_in_event`
  MODIFY `beers_in_event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Beer_data`
--

DROP TABLE IF EXISTS `Beer_data`;
CREATE TABLE `Beer_data` (
  `beer_data_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `main_class` int(10) UNSIGNED NOT NULL,
  `sub_class` text COLLATE utf8_swedish_ci NOT NULL,
  `type_name` text COLLATE utf8_swedish_ci NOT NULL,
  `votesys_category` int(11) DEFAULT NULL COMMENT 'Id tävlingsklass FV',
  `low_alc` tinyint(1) DEFAULT '0',
  `type_def` text COLLATE utf8_swedish_ci NOT NULL,
  `beer_name` text COLLATE utf8_swedish_ci NOT NULL,
  `volume` int(11) NOT NULL,
  `og` int(11) NOT NULL,
  `fg` int(11) NOT NULL,
  `bu` int(11) NOT NULL,
  `alc` decimal(10,1) NOT NULL,
  `mashing` text COLLATE utf8_swedish_ci NOT NULL,
  `ferment` text COLLATE utf8_swedish_ci NOT NULL,
  `water` text COLLATE utf8_swedish_ci NOT NULL,
  `comment` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Beer_data`
--
ALTER TABLE `Beer_data`
  ADD PRIMARY KEY (`beer_data_id`);

--
-- AUTO_INCREMENT for table `Beer_data`
--
ALTER TABLE `Beer_data`
  MODIFY `beer_data_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Brewers`
--

DROP TABLE IF EXISTS `Brewers`;
CREATE TABLE `Brewers` (
  `brewer_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `brewer_name` text COLLATE utf8_swedish_ci NOT NULL,
  `brewer_email` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Brewers`
--
ALTER TABLE `Brewers`
  ADD PRIMARY KEY (`brewer_id`);

--
-- AUTO_INCREMENT for table `Brewers`
--
ALTER TABLE `Brewers`
  MODIFY `brewer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Brewers_of_beer`
--

DROP TABLE IF EXISTS `Brewers_of_beer`;
CREATE TABLE `Brewers_of_beer` (
  `brewers_of_beer_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `brewer_id` int(10) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Brewers_of_beer`
--
ALTER TABLE `Brewers_of_beer`
  ADD PRIMARY KEY (`brewers_of_beer_id`);

--
-- AUTO_INCREMENT for table `Brewers_of_beer`
--
ALTER TABLE `Brewers_of_beer`
  MODIFY `brewers_of_beer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Captcha`
--

DROP TABLE IF EXISTS `Captcha`;
CREATE TABLE `Captcha` (
  `captcha_id` int(10) UNSIGNED NOT NULL,
  `captcha_question` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `captcha_answer` text COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `Captcha`
--

INSERT INTO `Captcha` (`captcha_id`, `captcha_question`, `captcha_answer`) VALUES(1, 'Öl bryggs vanligtvis med fyra huvudingredienser: Malt, humle, vatten och ?', 'Jäst');
INSERT INTO `Captcha` (`captcha_id`, `captcha_question`, `captcha_answer`) VALUES(2, 'Öl bryggs vanligtvis med fyra huvudingredienser: Jäst, humle, vatten och ?', 'Malt');
INSERT INTO `Captcha` (`captcha_id`, `captcha_question`, `captcha_answer`) VALUES(3, 'Öl bryggs vanligtvis med fyra huvudingredienser: Malt, jäst, vatten och ?', 'Humle');
INSERT INTO `Captcha` (`captcha_id`, `captcha_question`, `captcha_answer`) VALUES(4, 'Öl bryggs vanligtvis med fyra huvudingredienser: Malt, humle, jäst och ?', 'Vatten');

--
-- Indexes for table `Captcha`
--
ALTER TABLE `Captcha`
  ADD PRIMARY KEY (`captcha_id`);

-- --------------------------------------------------------

--
-- Table structure for table `Dt_event_reg`
--

DROP TABLE IF EXISTS `Dt_event_reg`;
CREATE TABLE `Dt_event_reg` (
  `dt_event_reg_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `received` tinyint(1) DEFAULT NULL,
  `disq` tinyint(1) DEFAULT NULL,
  `comment` text COLLATE utf8_swedish_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Dt_event_reg`
--
ALTER TABLE `Dt_event_reg`
  ADD PRIMARY KEY (`dt_event_reg_id`);

--
-- AUTO_INCREMENT for table `Dt_event_reg`
--
ALTER TABLE `Dt_event_reg`
  MODIFY `dt_event_reg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Events`
--

DROP TABLE IF EXISTS `Events`;
CREATE TABLE `Events` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_name` text COLLATE utf8_swedish_ci NOT NULL,
  `comp` enum('','DT','FV','ET') COLLATE utf8_swedish_ci NOT NULL,
  `type_def` text COLLATE utf8_swedish_ci NOT NULL,
  `extra_fv` text COLLATE utf8_swedish_ci,
  `low_alc` tinyint(1) DEFAULT '0',
  `votesys_competition_id` int(11) DEFAULT NULL COMMENT 'id för eventet i röstsystemet',
  `fv_allow_user_votesys_category_selection` int(11) NOT NULL COMMENT 'om 0 används FVClass för automatiskt klassning',
  `latest_label_no` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `beers_per_table` float NOT NULL DEFAULT '0',
  `table_length` int(11) NOT NULL DEFAULT '0',
  `max_no_tables` int(11) NOT NULL DEFAULT '0',
  `max_no_beers` int(11) NOT NULL DEFAULT '0',
  `dt_cost_memb` int(10) UNSIGNED NOT NULL,
  `dt_cost_nonmemb` int(10) UNSIGNED NOT NULL,
  `dt_pay_before` datetime DEFAULT NULL,
  `time_open` datetime NOT NULL,
  `time_close` datetime NOT NULL,
  `fv_nr_edit_locked` int(11) DEFAULT NULL COMMENT '1=Inga förändringar av FV-nr tillåts i webbgränssnittet',
  `fv_nr_time_visible_to_brewers` datetime DEFAULT NULL COMMENT 'Innan denna tidpunkt är FV-Nr (QR-koder) ej synliga för bryggare (om ej null)',  
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Events`
--
ALTER TABLE `Events`
  ADD PRIMARY KEY (`event_id`);

--
-- AUTO_INCREMENT for table `Events`
--
ALTER TABLE `Events`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Fv_event_reg`
--

DROP TABLE IF EXISTS `Fv_event_reg`;
CREATE TABLE `Fv_event_reg` (
  `fv_event_reg_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `fv_event_id` int(10) UNSIGNED NOT NULL,
  `next_to_id` int(10) UNSIGNED NOT NULL,
  `beer_slots` int(10) UNSIGNED NOT NULL,
  `bar_length` int(11) UNSIGNED NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Fv_event_reg`
--
ALTER TABLE `Fv_event_reg`
  ADD PRIMARY KEY (`fv_event_reg_id`);

--
-- AUTO_INCREMENT for table `Fv_event_reg`
--
ALTER TABLE `Fv_event_reg`
  MODIFY `fv_event_reg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Hops`
--

DROP TABLE IF EXISTS `Hops`;
CREATE TABLE `Hops` (
  `hops_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `hops_name` text COLLATE utf8_swedish_ci NOT NULL,
  `hops_form_id` int(10) UNSIGNED DEFAULT '0',
  `hops_alpha` decimal(10,1) DEFAULT '0.0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Hops`
--
ALTER TABLE `Hops`
  ADD PRIMARY KEY (`hops_id`);

--
-- AUTO_INCREMENT for table `Hops`
--
ALTER TABLE `Hops`
  MODIFY `hops_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Hops_forms`
--

DROP TABLE IF EXISTS `Hops_forms`;
CREATE TABLE `Hops_forms` (
  `hops_form_id` int(10) UNSIGNED NOT NULL,
  `hops_form_name` text COLLATE utf8_swedish_ci NOT NULL,
  `hops_const_alpha` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `Hops_forms`
--

INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(0, '', 0, 0, '2018-01-09 16:42:03');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(1, 'Kottar', 3, 0, '2018-01-09 16:42:03');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(2, 'Pellets', 4, 0, '2018-01-09 16:42:03');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(3, 'Färsk', 1, 0, '2018-08-09 21:40:55');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(4, 'Hel', 3, 0, '2018-01-09 16:42:03');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(5, 'Krossad', 3, 0, '2018-01-09 16:42:03');
INSERT INTO `Hops_forms` (`hops_form_id`, `hops_form_name`, `hops_const_alpha`, `deleted`, `time`) VALUES(6, 'Mald', 3, 0, '2018-01-09 16:42:03');

--
-- Indexes for table `Hops_forms`
--
ALTER TABLE `Hops_forms`
  ADD PRIMARY KEY (`hops_form_id`);

-- --------------------------------------------------------

--
-- Table structure for table `Hops_in_beer`
--

DROP TABLE IF EXISTS `Hops_in_beer`;
CREATE TABLE `Hops_in_beer` (
  `hops_in_beer_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `hops_id` int(10) UNSIGNED NOT NULL,
  `hops_weight` int(11) NOT NULL,
  `hops_boil_time` int(11) DEFAULT NULL,
  `hops_comment` text COLLATE utf8_swedish_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Hops_in_beer`
--
ALTER TABLE `Hops_in_beer`
  ADD PRIMARY KEY (`hops_in_beer_id`);

--
-- AUTO_INCREMENT for table `Hops_in_beer`
--
ALTER TABLE `Hops_in_beer`
  MODIFY `hops_in_beer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Malts`
--

DROP TABLE IF EXISTS `Malts`;
CREATE TABLE `Malts` (
  `malt_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `malt_name` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Malts`
--
ALTER TABLE `Malts`
  ADD PRIMARY KEY (`malt_id`);

--
-- AUTO_INCREMENT for table `Malts`
--
ALTER TABLE `Malts`
  MODIFY `malt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Malts_in_beer`
--

DROP TABLE IF EXISTS `Malts_in_beer`;
CREATE TABLE `Malts_in_beer` (
  `malts_in_beer_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `malt_id` int(10) UNSIGNED NOT NULL,
  `malt_weight` int(11) NOT NULL,
  `malt_comment` text COLLATE utf8_swedish_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Malts_in_beer`
--
ALTER TABLE `Malts_in_beer`
  ADD PRIMARY KEY (`malts_in_beer_id`);

--
-- AUTO_INCREMENT for table `Malts_in_beer`
--
ALTER TABLE `Malts_in_beer`
  MODIFY `malts_in_beer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Others`
--

DROP TABLE IF EXISTS `Others`;
CREATE TABLE `Others` (
  `others_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `others_name` text COLLATE utf8_swedish_ci NOT NULL,
  `others_stage_id` int(10) UNSIGNED DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Others`
--
ALTER TABLE `Others`
  ADD PRIMARY KEY (`others_id`);

--
-- AUTO_INCREMENT for table `Others`
--
ALTER TABLE `Others`
  MODIFY `others_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Others_in_beer`
--

DROP TABLE IF EXISTS `Others_in_beer`;
CREATE TABLE `Others_in_beer` (
  `others_in_beer_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `others_id` int(10) UNSIGNED NOT NULL,
  `others_weight` int(11) NOT NULL,
  `others_comment` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Others_in_beer`
--
ALTER TABLE `Others_in_beer`
  ADD PRIMARY KEY (`others_in_beer_id`);

--
-- AUTO_INCREMENT for table `Others_in_beer`
--
ALTER TABLE `Others_in_beer`
  MODIFY `others_in_beer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Others_stages`
--

DROP TABLE IF EXISTS `Others_stages`;
CREATE TABLE `Others_stages` (
  `others_stage_id` int(10) UNSIGNED NOT NULL,
  `others_stage_name` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumping data for table `Others_stages`
--

INSERT INTO `Others_stages` (`others_stage_id`, `others_stage_name`, `deleted`, `time`) VALUES(0, '', 0, '2018-02-10 15:31:16');
INSERT INTO `Others_stages` (`others_stage_id`, `others_stage_name`, `deleted`, `time`) VALUES(1, 'Mäskning', 0, '2018-09-02 18:35:21');
INSERT INTO `Others_stages` (`others_stage_id`, `others_stage_name`, `deleted`, `time`) VALUES(2, 'Kokning', 0, '2018-02-10 15:26:44');
INSERT INTO `Others_stages` (`others_stage_id`, `others_stage_name`, `deleted`, `time`) VALUES(3, 'Jäsning', 0, '2018-09-02 18:35:36');
INSERT INTO `Others_stages` (`others_stage_id`, `others_stage_name`, `deleted`, `time`) VALUES(4, 'Lagring', 0, '2018-02-10 15:25:17');

--
-- Indexes for table `Others_stages`
--
ALTER TABLE `Others_stages`
  ADD PRIMARY KEY (`others_stage_id`);

-- --------------------------------------------------------

--
-- Table structure for table `Prices`
--

DROP TABLE IF EXISTS `Prices`;
CREATE TABLE `Prices` (
  `prices_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `beer_id` int(10) UNSIGNED NOT NULL,
  `price` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Prices`
--
ALTER TABLE `Prices`
  ADD PRIMARY KEY (`prices_id`);

--
-- AUTO_INCREMENT for table `Prices`
--
ALTER TABLE `Prices`
  MODIFY `prices_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` text COLLATE utf8_swedish_ci NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

-- --------------------------------------------------------

--
-- Table structure for table `User_data`
--

DROP TABLE IF EXISTS `User_data`;
CREATE TABLE `User_data` (
  `user_data_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` text COLLATE utf8_swedish_ci NOT NULL,
  `member_no` int(10) UNSIGNED DEFAULT '0',
  `email` text COLLATE utf8_swedish_ci NOT NULL,
  `care_of` text COLLATE utf8_swedish_ci,
  `street` text COLLATE utf8_swedish_ci,
  `post_no` text COLLATE utf8_swedish_ci,
  `city` text COLLATE utf8_swedish_ci,
  `adm_lev` enum('NONE','FULL','LIM','') COLLATE utf8_swedish_ci DEFAULT 'NONE',
  `passwd` text COLLATE utf8_swedish_ci NOT NULL,
  `code` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Indexes for table `User_data`
--
ALTER TABLE `User_data`
  ADD PRIMARY KEY (`user_data_id`);

--
-- AUTO_INCREMENT for table `User_data`
--
ALTER TABLE `User_data`
  MODIFY `user_data_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

--
-- Indexes for dumped tables
--




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

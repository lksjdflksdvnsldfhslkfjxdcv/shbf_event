-- Endast rad för FV. På höstölen behöver man inte anmäla sig till DT. Man är med där automatiskt. (Utom för mjöd och cider.)
INSERT INTO Events (event_name, comp, type_def, extra_fv, low_alc, latest_label_no, beers_per_table, table_length, max_no_tables, dt_cost_memb, dt_cost_nonmemb, dt_pay_before, time_open, time_close, deleted) VALUES ('Höstöl Stockholm-2019', 'FV', 'http://styles.shbf.se/json/2018/styles', 'json/extra_fv_classes.json', '', '0', '2', '120', '14', '0', '0', '2019-10-26 00:00:00', '2019-09-01 00:00:00', '2019-10-21 05:00:00', '0');
SELECT * FROM Events;


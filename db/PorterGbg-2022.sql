INSERT INTO Events (
 event_name,                      comp, type_def,                                 extra_fv,   low_alc, latest_label_no, beers_per_table, table_length, max_no_tables, max_no_beers, dt_cost_memb, dt_cost_nonmemb, dt_pay_before,         time_open,             time_close,            deleted) VALUES (
'Porter och stout Göteborg 2022', 'DT', 'json/porter_dt_classes.json',            '',         '0',     '0',             '2',             '180',        '40',          '80',         '0',          '0',             '2022-02-04 00:00:00', '2021-12-14 00:00:00', '2022-01-31 05:00:00', '0');
INSERT INTO Events (
 event_name,                      comp, type_def,                                 extra_fv,   low_alc, latest_label_no, beers_per_table, table_length, max_no_tables, max_no_beers, dt_cost_memb, dt_cost_nonmemb, dt_pay_before,         time_open,             time_close,            deleted) VALUES (
'Porter och stout Göteborg 2022', 'FV', 'http://styles.shbf.se/json/2020/styles', '',         '0',     '0',             '2',             '180',        '40',          '80',         '0',          '0',             '2022-02-04 00:00:00', '2021-12-14 00:00:00', '2022-01-31 05:00:00', '0');
SELECT * FROM Events;
  

INSERT INTO Events (
 event_name,              comp, type_def,                                 extra_fv,                    low_alc, votesys_competition_id, latest_label_no, beers_per_table, table_length, max_no_tables, max_no_beers, dt_cost_memb, dt_cost_nonmemb, dt_pay_before,         time_open,             time_close,            fv_nr_edit_locked, fv_nr_time_visible_to_brewers, deleted) VALUES (
'Höstöl Stockholm-2024', 'FV', 'http://styles.shbf.se/json/2020/styles', 'json/extra_fv_classes.json', '0',     'NULL',                 '0',             '2',             '120',        '20',           '30',          '0',          '0',          '2024-10-19 00:00:00', '2024-09-20 00:00:00', '2024-10-15 05:00:00',    '0',               '2024-10-17 00:00:00',         '0');
INSERT INTO Events (
 event_name,              comp, type_def,                                 extra_fv,                    low_alc, votesys_competition_id, latest_label_no, beers_per_table, table_length, max_no_tables, max_no_beers, dt_cost_memb, dt_cost_nonmemb, dt_pay_before,         time_open,             time_close,            fv_nr_edit_locked, fv_nr_time_visible_to_brewers, deleted) VALUES (
'Höstöl Stockholm-2024', 'DT', 'http://styles.shbf.se/json/2020/styles', 'json/extra_fv_classes.json', '0',     'NULL',                 '0',             '2',             '120',        '20',           '30',          '0',          '0',          '2024-10-19 00:00:00', '2024-09-20 00:00:00', '2024-10-15 05:00:00',    '0',               '2024-10-17 00:00:00',         '0');
SELECT * FROM Events;


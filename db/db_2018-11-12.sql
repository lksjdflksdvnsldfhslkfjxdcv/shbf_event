ALTER TABLE Beer_data
ADD COLUMN `low_alc` tinyint(1) DEFAULT '0' AFTER type_name;

ALTER TABLE User_data
ADD COLUMN `member_no` int(10) UNSIGNED DEFAULT '0' AFTER name;

ALTER TABLE Events
ADD COLUMN `low_alc` tinyint(1) DEFAULT '0' AFTER extra_fv;


